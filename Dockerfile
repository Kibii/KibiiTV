FROM node:alpine as node-env
WORKDIR /app
COPY Source/ ./Source

# Before publishing the frontend binaries (or any of them), we'll build our NPM stuff first
RUN cd ./Source/KibiiTV.Frontend/ && npm install && npm run install && npm run build
FROM microsoft/dotnet:2.1-sdk AS build-env

WORKDIR /app
COPY --from=node-env /app/Source/ ./Source

RUN dotnet publish ./Source/KibiiTV.Api/KibiiTV.csproj -c Release -o Build
RUN dotnet publish ./Source/KibiiTV.Frontend/KibiiTV.Frontend.csproj -c Release -o Build
RUN dotnet publish ./Source/KibiiTV.Data.Generator/KibiiTV.Data.Generator.csproj -c Release -o Build

EXPOSE 80/tcp
EXPOSE 443/tcp

# We'll be using the runtime in production.
FROM microsoft/dotnet:2.1-aspnetcore-runtime
WORKDIR /app

COPY --from=build-env /app/Source/KibiiTV.Api/Build ./Api
COPY --from=build-env /app/Source/KibiiTV.Frontend/Build ./Frontend
COPY --from=build-env /app/Source/KibiiTV.Data.Generator/Build ./DataGenerator
