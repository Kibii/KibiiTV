ROOT_PATH="Source/KibiiTV.Api"
BUILD_PATH="Build"
EXE_PATH="Build/Kibii.dll"

cd $ROOT_PATH
dotnet build -c Release -o $BUILD_PATH
dotnet $EXE_PATH
sleep 10s