ROOT_PATH="Source/KibiiTV.Data.Generator"
BUILD_PATH="Build"
EXE_PATH="Build/Kibii.Data.Generator.dll"

cd $ROOT_PATH
dotnet build -c Release -o $BUILD_PATH
dotnet $EXE_PATH