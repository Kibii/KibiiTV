#!/usr/bin/env bash
set -o errexit # Abort if any command fails

export DOTNET_CLI_TELEMETRY_OPTOUT=1
export ASPNETCORE_ENVIRONMENT="Production"

GIT_URL="git@gitlab.com:kibii/kibii.tv.git"
TOKEN="NcxVWTH1MKjFTxvLCQVp"

# Update and upgrade packages to keep up-to-date
update_packages() {
    sudo apt-get update -y
    sudo apt-get upgrade -y
    sudo apt-get autoremove -y
}

# Install git
install_git() {
    sudo apt-get install git -y
}

# Packages required by .NET Core
install_netcore_packages() {
    sudo apt-get install libunwind8 liblttng-ust0 libcurl3 libssl1.0.0 libuuid1 libkrb5-3 zlib1g libicu55 -y
}

# Register Microsoft key and feed
register_ms() {
    if [ ! -d /kibii/.netcore_msfeed ]; then
        curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
        sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
        # Add the package into the apt sources
        sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-ubuntu-xenial-prod xenial main" > /etc/apt/sources.list.d/dotnetdev.list'
        mkdir /kibii/.netcore_msfeed
    fi
}

# Install .NET Core runtime and SDK
install_netcore() {
    sudo apt-get install apt-transport-https -y 
    sudo apt-get update
    sudo apt-get install dotnet-sdk-2.1.103 -y
}

verify_kibii_folder() {
    if [ ! -d /kibii ]; then
        mkdir /kibii
    fi
    
    if [ ! -d /kibii/repo ]; then
        mkdir /kibii/repo
    fi

    if [ ! -d /kibii/prod_build ]; then
        mkdir /kibii/prod_build
    fi
}

verify_keys() {
    rm -f ~/.ssh/id_rsa
    ssh-keygen -t rsa -N "KIBII_CI_AUTODEPLOY_04231820" -f ~/.ssh/id_rsa
    SSH_KEY="$(cat ~/.ssh/id_rsa.pub)"
    curl -X POST -F "key=$SSH_KEY" -F "title=$(hostname)" -F "private_token=$TOKEN" https://gitlab.com/api/v3/user/keys
}

verify_install() {
    if [ ! -d /kibii/netcore_gitinstall ]; then
        install_git
        mkdir /kibii/netcore_gitinstall
    fi

    if [ ! -d /kibii/netcore_pkginstall ]; then
        install_netcore_packages
        install_netcore
        mkdir /kibii/netcore_pkginstall
    fi

    update_packages
}

run_build() {
    dotnet build /kibii/repo/src/api/kibii.csproj --configuration Release -o /kibii/prod_build

    cp /kibii/repo/src/api/app.db /kibii/prod_build
    cp /kibii/repo/src/api/appsettings.json /kibii/prod_build
    cp /kibii/repo/src/api/appsettings.Development.json /kibii/prod_build
    cp /kibii/repo/src/api/logs.db /kibii/prod_build

    dotnet /kibii/prod_build/kibii.dll --urls "http://*:80"
}

begin() {
    verify_kibii_folder
    register_ms
    update_packages
    verify_install

    if [ ! -d /kibii/netcore_gitsuccess ]; then
        git clone $GIT_URL /kibii/repo
        
        git config --global user.name "Kibii CI AutoDeploy"
        git config --global user.email "ci@kibii.org"
        
        mkdir /kibii/netcore_gitsuccess
    fi
    
    cd /kibii/repo
    git pull
    run_build
}

verify_keys
begin