﻿using Newtonsoft.Json;

namespace KibiiTV.Scraper.Deprecated.Models
{
    public partial class Synonym
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("type")]
        public int Type { get; set; }
    }
}
