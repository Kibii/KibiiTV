﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace KibiiTV.Scraper.Deprecated.Models
{
    public class MirrorInfo
    {
        [JsonProperty("mirrors")]
        public List<Mirror> Mirrors { get; set; }
    }
}