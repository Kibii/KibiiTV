﻿using Newtonsoft.Json;

namespace KibiiTV.Scraper.Deprecated.Models
{
    public partial class Wallpaper
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("file")]
        public string File { get; set; }
    }
}
