﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace KibiiTV.Scraper.Deprecated.Models
{
    public partial class MasterResponse
    {
        [JsonProperty("total")]
        public int? Total { get; set; }

        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

        [JsonProperty("current_page")]
        public int? CurrentPage { get; set; }

        [JsonProperty("last_page")]
        public int? LastPage { get; set; }

        [JsonProperty("next_page_url")]
        public string NextPageUrl { get; set; }

        [JsonProperty("prev_page_url")]
        public string PrevPageUrl { get; set; }

        [JsonProperty("from")]
        public int? From { get; set; }

        [JsonProperty("to")]
        public int? To { get; set; }

        [JsonProperty("data")]
        public List<MAnime> Anime { get; set; }
    }
}
