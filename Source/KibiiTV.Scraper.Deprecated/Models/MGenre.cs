﻿using Newtonsoft.Json;

namespace KibiiTV.Scraper.Deprecated.Models
{
    public partial class MGenre
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
