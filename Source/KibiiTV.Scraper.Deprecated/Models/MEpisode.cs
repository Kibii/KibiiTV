﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace KibiiTV.Scraper.Deprecated.Models
{
    public partial class MEpisode
    {
        [JsonProperty("info")]
        public EpisodeInfo Info { get; set; }

        [JsonProperty("thumbnail")]
        public string Thumbnail { get; set; }

        [JsonIgnore]
        public List<Mirror> Mirrors { get; set; }
    }
}
