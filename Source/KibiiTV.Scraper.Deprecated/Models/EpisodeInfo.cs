﻿using Newtonsoft.Json;
using System;

namespace KibiiTV.Scraper.Deprecated.Models
{
    public partial class EpisodeInfo
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("anime_id")]
        public int? AnimeId { get; set; }

        [JsonProperty("episode")]
        public string Episode { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("tvdb_id")]
        public int? TvdbId { get; set; }

        [JsonProperty("aired")]
        public DateTime? Aired { get; set; }

        [JsonProperty("type")]
        public int? Type { get; set; }

        [JsonProperty("duration")]
        public string Duration { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
