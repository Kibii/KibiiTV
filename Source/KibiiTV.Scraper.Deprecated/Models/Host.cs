﻿using Newtonsoft.Json;

namespace KibiiTV.Scraper.Deprecated.Models
{
    public partial class Host
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("embed_prefix")]
        public string EmbedPrefix { get; set; }

        [JsonProperty("embed_suffix")]
        public string EmbedSuffix { get; set; }
    }
}
