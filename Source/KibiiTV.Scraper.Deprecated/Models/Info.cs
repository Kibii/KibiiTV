﻿using Newtonsoft.Json;
using System;

namespace KibiiTV.Scraper.Deprecated.Models
{
    public partial class Info
    {
        [JsonProperty("id")]
        public int? Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }

        [JsonProperty("synopsis")]
        public string Synopsis { get; set; }

        [JsonProperty("status")]
        public int? Status { get; set; }

        [JsonProperty("type")]
        public int? Type { get; set; }

        [JsonProperty("score")]
        public double Score { get; set; }

        [JsonProperty("users_watching")]
        public int? UsersWatching { get; set; }

        [JsonProperty("users_completed")]
        public int? UsersCompleted { get; set; }

        [JsonProperty("users_on_hold")]
        public int? UsersOnHold { get; set; }

        [JsonProperty("users_planned")]
        public int? UsersPlanned { get; set; }

        [JsonProperty("users_dropped")]
        public int? UsersDropped { get; set; }

        [JsonProperty("episode_count")]
        public int? EpisodeCount { get; set; }

        [JsonProperty("started_airing_date")]
        public DateTime? StartedAiringDate { get; set; }

        [JsonProperty("finished_airing_date")]
        public DateTime? FinishedAiringDate { get; set; }

        [JsonProperty("youtube_trailer_id")]
        public string YoutubeTrailerId { get; set; }

        [JsonProperty("age_rating")]
        public string AgeRating { get; set; }

        [JsonProperty("episode_length")]
        public int? EpisodeLength { get; set; }

        [JsonProperty("tvdb_id")]
        public int? TvdbId { get; set; }

        [JsonProperty("tvdb_season_id")]
        public string TvdbSeasonId { get; set; }

        [JsonProperty("tvdb_episode")]
        public int? TvdbEpisode { get; set; }

        [JsonProperty("wallpaper_id")]
        public string WallpaperId { get; set; }

        [JsonProperty("wallpaper_offset")]
        public int? WallpaperOffset { get; set; }

        [JsonProperty("franchise_count")]
        public int? FranchiseCount { get; set; }
    }
}
