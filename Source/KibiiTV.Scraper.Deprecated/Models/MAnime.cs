﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace KibiiTV.Scraper.Deprecated.Models
{
    public partial class MAnime
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("type")]
        public int? Type { get; set; }

        [JsonProperty("score")]
        public double Score { get; set; }

        [JsonProperty("episode_count")]
        public long? EpisodeCount { get; set; }

        [JsonProperty("started_airing_date")]
        public DateTime? StartedAiringDate { get; set; }

        [JsonProperty("finished_airing_date")]
        public DateTime? FinishedAiringDate { get; set; }

        [JsonProperty("genres")]
        public List<MGenre> Genres { get; set; }

        [JsonProperty("poster")]
        public Poster Poster { get; set; }

        [JsonProperty("anime_info")]
        public AnimeInfo Info { get; set; }
    }
}
