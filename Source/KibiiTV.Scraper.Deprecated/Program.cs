﻿using System;

namespace KibiiTV.Scraper.Deprecated
{
    public class Program
    {
        public static void Main(string[] args) => DisplayDeprecationMessage();

        public static void DisplayDeprecationMessage()
        {
            Console.WriteLine("Kibii TV SeriesDeploy Kibii.Scraper.Deprecated v0.1");
            Console.WriteLine("Please refrain from running this application.");
            Console.WriteLine("This scraper was originally used to fill up the intial version of the Kibii TV database.");
            Console.WriteLine("This code is extremely outdated and has little value, it is no longer/barely functional.");
            Console.WriteLine("This code will not be updated alongside any future updates as of 05/20/2018");
            Console.WriteLine("Please refrain from running this code or using any of its snippets.");

            Console.WriteLine("Please hit the ENTER key to exit the application...");
            Console.ReadLine();
        }
    }
}
