﻿using KibiiTV.Scraper.Deprecated.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;

namespace KibiiTV.Scraper.Deprecated.Services
{
    public class KibiiApiService
    {
        private HttpClient Client { get; set; }
        private static string ApiUrl = "https://localhost:5000/api/administration/media";
        private static string AddAnimeUrl = ApiUrl + "/anime";
        private static string AddEpisodeUrl = ApiUrl + "/episodes";
        private static string AddEmbedUrl = ApiUrl + "/blobs";
        private static string AddGenreUrl = ApiUrl + "/genres";

        public KibiiApiService()
        {
            Client = new HttpClient();
            Client.DefaultRequestHeaders.Add("Accept", "application/json");
            Client.DefaultRequestHeaders.Add("Kibii-API-Token", "REMOVED_FOR_SECURITY_PURPOSES");
        }

        public void AddEpisode(int AnimeId, MEpisode episode)
        {

        }

        public void AddEmbed(int EpisodeId, Mirror mirror)
        {

        }

        public void AddAnime(MAnime anime)
        {

        }

        public void RegisterGenres()
        {
            string p = "[\"Action\",\"Adventure\",\"Cars\",\"Comedy\",\"Dementia\",\"Demons\",\"Drama\",\"Ecchi\",\"Fantasy\",\"Game\",\"Harem\",\"Historical\",\"Horror\",\"Josei\",\"Kids\",\"Magic\",\"Martial Arts\",\"Mecha\",\"Military\",\"Music\",\"Mystery\",\"Parody\",\"Police\",\"Psychological\",\"Romance\",\"Samurai\",\"School\",\"Sci-Fi\",\"Seinen\",\"Shoujo\",\"Shoujo Ai\",\"Shounen\",\"Shounen Ai\",\"Slice of Life\",\"Space\",\"Sports\",\"Super Power\",\"Supernatural\",\"Thriller\",\"Vampire\",\"Yaoi\",\"Yuri\"]";
            var genres = JsonConvert.DeserializeObject<List<string>>(p);
            var gs = new Dictionary<int, string>();

            foreach(string g in genres)
            {
                using (var r = new HttpRequestMessage(HttpMethod.Post, AddGenreUrl))
                {
                    r.Content = Json(new { name = g });
                    var resp = Client.SendAsync(r).Result;
                    var res = resp.Content.ReadAsStringAsync().Result;

                    var oo = new { Id = 0, Name = g };
                    var o = JsonConvert.DeserializeAnonymousType(res, oo);
                    Console.WriteLine("Created genre '" + o.Name + "' with ID " + o.Id);
                    gs.Add(o.Id, o.Name);
                }
            }

            File.WriteAllText("Genres.json", JsonConvert.SerializeObject(gs, Formatting.Indented));
        }

        private StringContent Json(object obj)
        {
            return new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");
        }
    }
}

