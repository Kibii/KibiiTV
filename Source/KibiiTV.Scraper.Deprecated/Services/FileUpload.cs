﻿namespace KibiiTV.Scraper.Deprecated.Services
{
    public class FileUpload
    {
        public string Id { get; internal set; }
        public string OldId { get; internal set; }
        public FileType Type { get; internal set; }
        public string NewId { get; internal set; }
    }
}