﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KibiiTV.Scraper.Deprecated.Services
{
    public static class Extentions
    {
        public static T Random<T>(this IEnumerable<T> list)
        {
            return list.ElementAt(new Random().Next(0, list.Count()));
        }

        public static T CloneJson<T>(this T source)
        {
            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            // initialize inner objects individually
            // for example in default constructor some list property initialized with some values,
            // but in 'source' these items are cleaned -
            // without ObjectCreationHandling.Replace default constructor values will be added to result
            var deserializeSettings = new JsonSerializerSettings { ObjectCreationHandling = ObjectCreationHandling.Replace };

            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(source), deserializeSettings);
        }
    }
}
