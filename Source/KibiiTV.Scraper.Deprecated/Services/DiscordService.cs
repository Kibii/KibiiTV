﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace KibiiTV.Scraper.Deprecated.Services
{
    public class DiscordService
    {
        private static string WebhookURL = "REMOVED_FOR_SECURITY_PURPOSES";
        private HttpClient Client;
        private Dictionary<string, string> anime = new Dictionary<string, string>();
        private Dictionary<string, string> embeds = new Dictionary<string, string>();
        public DiscordService()
        {
            Client = new HttpClient();
        }

        public void AddAnime(string id, string name)
        {
            anime.Add(id, name);
        } 

        public void AddEmbed(string id, string name)
        {
            embeds.Add(id, name);
        }

        public async Task BeginSearch()
        {
            while (true)
            {
                if (anime.Count > new Random().Next(10, 25))
                {
                    await DeployHookAsync(anime);
                    anime.Clear();
                }

                if (embeds.Count > new Random().Next(450, 510))
                {
                    await DeployEmbedHookAsync(embeds.Count);
                    embeds.Clear();
                }

                await Task.Delay(5000);
            }
        }

        private async Task DeployEmbedHookAsync(int count)
        {
            using (var r = new HttpRequestMessage(HttpMethod.Post, WebhookURL))
            {
                r.Content = new StringContent(JsonConvert.SerializeObject(new
                {
                    content = $"Kibii SeriesDeploy has deployed **{count} new episodes to Kibii TV!**"
                }), Encoding.UTF8, "application/json");

                await Client.SendAsync(r);
            }
        }

        private async Task DeployHookAsync(Dictionary<string, string> aa)
        {
            string content = $"Kibii SeriesDeploy has deployed **{aa.Count} series of anime to Kibii TV!**";
            content += "\nHere's a list:";

            foreach(var anime in aa)
            {
                content += $"\n- **{anime.Value}** - 「{anime.Key}」";
            }

            using (var r = new HttpRequestMessage(HttpMethod.Post, WebhookURL))
            {
                r.Content = new StringContent(JsonConvert.SerializeObject(new
                {
                    content
                }), Encoding.UTF8, "application/json");

                await Client.SendAsync(r);
            }
        }
    }
}
