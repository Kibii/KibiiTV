﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using KibiiTV.Data.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace KibiiTV.Scraper.Deprecated.Services
{
    public class S3Service
    {
        private static string S3Secret = "REMOVED_FOR_SECURITY_PURPOSES";
        private static string S3Key = "REMOVED_FOR_SECURITY_PURPOSES";
        private static string BucketName = "REMOVED_FOR_SECURITY_PURPOSES";
        private static string MasterEndpoint = "https://cdn.masterani.me";
        private static readonly RegionEndpoint BucketRegion = RegionEndpoint.USEast1;
        private HttpClient Client;
        private List<FileUpload> Uploads;
        private List<string> Finished;
        private IAmazonS3 S3Client;
        private TransferUtility TransferClient;

        public S3Service()
        {
            Client = new HttpClient();
            Uploads = new List<FileUpload>();
            Finished = new List<string>();
            S3Client = new AmazonS3Client(S3Key, S3Secret, BucketRegion);
            TransferClient = new TransferUtility(S3Client);
        }

        public string GenerateId(string oldId)
        {
            string guid = Guid.NewGuid().ToString();
            string id = IdGenerator.NewId();
            string[] splits = oldId.Split('.');
            string ext;

            if (splits.Length <= 1)
                ext = "jpg";
            else
                ext = splits[splits.Length - 1];

            return guid + "/" + id + "." + ext;
        }

        public void UploadFile(FileUpload Upload)
        {
            Upload.Id = Guid.NewGuid().ToString();
            Uploads.Add(Upload);
        }

        public async Task SearchAndUpload()
        {
            while (true)
            {
                foreach(var upload in Uploads.ToList())
                {
                    if (Finished.Contains(upload.Id))
                    {
                        Uploads.Remove(upload);
                        continue;
                    }
                    else
                    {
                        await CompleteUpload(upload.OldId, upload.Type, upload.NewId);
                        Finished.Add(upload.Id);
                    }
                }

                await Task.Delay(5000);
                Console.WriteLine("\n\nUPLOADS_AVAILABLE: " + Uploads.Count + "\nUPLOADS_FINISHED: " + Finished.Count + "\n\n");
            }
        }

        public async Task CompleteUpload(string Id, FileType Type, string NewId)
        {
            if (string.IsNullOrWhiteSpace(Id))
                return;

            string SubEndpoint = "/episode/";

            if (Type == FileType.Wallpaper)
                SubEndpoint = "/wallpaper/0/";
            else if (Type == FileType.Cover)
                SubEndpoint = "/poster/";

            string URL = MasterEndpoint + SubEndpoint + Id;

            using (var req = new HttpRequestMessage(HttpMethod.Get, URL))
            {
                req.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36");

                var res = await Client.SendAsync(req);
                int statusCode = (int)res.StatusCode;

                if (statusCode == 404 || statusCode == 400 || statusCode == 500)
                    return;

                var s3Req = new TransferUtilityUploadRequest()
                {
                    Key = NewId,
                    AutoCloseStream = true,
                    BucketName = BucketName,
                    CannedACL = S3CannedACL.PublicRead,
                    ContentType = res.Content.Headers.ContentType.ToString(),
                    InputStream = await res.Content.ReadAsStreamAsync()
                };

                await TransferClient.UploadAsync(s3Req);
            }
        }
    }

    public enum FileType
    {
        Wallpaper = 0,
        Episode = 1,
        Cover = 2
    }
}
