﻿using HtmlAgilityPack;
using KibiiTV.Data.Media;
using KibiiTV.Data.Misc;
using KibiiTV.Scraper.Deprecated.Models;
using KibiiTV.Scraper.Deprecated.Services;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KibiiTV.Scraper.Deprecated
{
    /// <summary>
    /// Please do not mind the extremely bad code that you see here. We know it was a mistake.
    /// This scraper was originally used to fill up the intial version of the Kibii TV database.
    /// This code is extremely outdated and has little value, it is no longer/barely functional.
    /// This code will not be updated alongside any future updates as of 05/20/2018
    /// Please refrain from running this code or using any of its snippets.
    /// </summary>
    public class Scraper
    {
        static HttpClient client = new HttpClient();

        public static void EntryPoint(string[] args)
        {
            Console.WriteLine("Running :^)");
            int meme = RunScraper().GetAwaiter().GetResult();
        }

        public static bool Debug = true;

        public static async Task<int> RunScraper()
        {
            var db = new KibiiTV.Data.KibiiContext("REMOVED_FOR_SECURITY_PURPOSES");
            var sv = new S3Service(); // These never worked
            var wb = new DiscordService();

            string p = "[\"Action\",\"Adventure\",\"Cars\",\"Comedy\",\"Dementia\",\"Demons\",\"Drama\",\"Ecchi\",\"Fantasy\",\"Game\",\"Harem\",\"Historical\",\"Horror\",\"Josei\",\"Kids\",\"Magic\",\"Martial Arts\",\"Mecha\",\"Military\",\"Music\",\"Mystery\",\"Parody\",\"Police\",\"Psychological\",\"Romance\",\"Samurai\",\"School\",\"Sci-Fi\",\"Seinen\",\"Shoujo\",\"Shoujo Ai\",\"Shounen\",\"Shounen Ai\",\"Slice of Life\",\"Space\",\"Sports\",\"Super Power\",\"Supernatural\",\"Thriller\",\"Vampire\",\"Yaoi\",\"Yuri\"]";
            Dictionary<string, string> Genres = new Dictionary<string, string>();

            if (!File.Exists("Genres.json"))
            {
                foreach (string gg in JsonConvert.DeserializeObject<List<string>>(p))
                {
                    var g = new Genre() { Name = gg };

                    await db.Genres.AddAsync(g);
                    await db.SaveChangesAsync();

                    Genres.Add(g.Id, g.Name);
                }

                File.WriteAllText("Genres.json", JsonConvert.SerializeObject(Genres, Formatting.Indented));
            }

            Genres = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText("Genres.json"));

            foreach (var g in Genres)
            {
                Console.WriteLine($"Genre '{g.Value}' with ID {g.Key} loaded.");
            }

            // File.WriteAllText("Genres.json", JsonConvert.SerializeObject(gs, Formatting.Indented));

            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            var settings2 = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                Formatting = Formatting.Indented
            };

            //await Task.Delay(5000);
            int currentPage = 1;
            bool AnimeListSearchCompleted = false;

            List<MAnime> Anime = new List<MAnime>();
            List<int> Skipped = new List<int>();
            List<int> Finished = GetFinishedAnime() ?? new List<int>();
            List<int> FinishedEpisodes = GetFinishedEpisodes() ?? new List<int>();

            new Thread(async () => await sv.SearchAndUpload()).Start();
            new Thread(async () => await wb.BeginSearch()).Start(); // These services never worked.

            while (!AnimeListSearchCompleted)
            {
                using (var r = new HttpRequestMessage(HttpMethod.Get, $"https://www.masterani.me/api/anime/filter?order=score_desc&page={currentPage}"))
                {

                    bool requestNotRatelimited = false;
                    HttpResponseMessage rest = null;

                    do
                    {
                        rest = await client.SendAsync(Clone(r));
                        if ((int)rest.StatusCode == 429)
                        {
                            Console.WriteLine("Ratelimited! Retrying after " + rest.Headers.GetValues("Retry-After").FirstOrDefault() + " seconds.");
                            await Task.Delay(Convert.ToInt32(rest.Headers.GetValues("Retry-After").FirstOrDefault()) * 1000);
                        }
                        else
                        {
                            requestNotRatelimited = true;
                        }
                    }
                    while (!requestNotRatelimited);

                    string response = await rest.Content.ReadAsStringAsync();

                    var resp = JsonConvert.DeserializeObject<MasterResponse>(response, settings);

                    Anime.AddRange(resp.Anime);

                    File.WriteAllText("AnimeInProgress.json", JsonConvert.SerializeObject(Anime, settings));

                    if (resp.CurrentPage >= resp.LastPage)
                    {
                        AnimeListSearchCompleted = true;
                    }

                }

                Console.WriteLine($"Page {currentPage++} finished");

            }
            int epcount = 0;
            int cc = 0;
            foreach (var a in Anime)
            {
                cc++;
                using (var r = new HttpRequestMessage(HttpMethod.Get, $"https://www.masterani.me/api/anime/{a.Id}/detailed"))
                {

                    bool requestNotRatelimited = false;
                    HttpResponseMessage rest = null;

                    do
                    {
                        rest = await client.SendAsync(Clone(r));
                        if ((int)rest.StatusCode == 429)
                        {
                            Console.WriteLine("Ratelimited! Retrying after " + rest.Headers.GetValues("Retry-After").FirstOrDefault() + " seconds.");
                            await Task.Delay(Convert.ToInt32(rest.Headers.GetValues("Retry-After").FirstOrDefault()) * 1000);
                        }
                        else
                        {
                            requestNotRatelimited = true;
                        }
                    }
                    while (!requestNotRatelimited);

                    string response = await rest.Content.ReadAsStringAsync();

                    try
                    {
                        bool animeAlreadyAdded = false;
                        var resp = JsonConvert.DeserializeObject<AnimeInfo>(response, settings);
                        epcount += resp.Episodes?.Count() ?? 0;
                        a.Info = resp;
                        //     Console.WriteLine(epcount);

                            string w = a.Info.Wallpapers?.Random()?.File;

                            var anime = new Anime()
                            {
                                AgeRating = a.Info.Info.AgeRating,
                                CoverId = sv.GenerateId(a.Info.Poster ?? "notfound.jpg"),
                                Description = a.Info.Info.Synopsis,
                                StartedAiringDate = a.StartedAiringDate,
                                FinishedAiringDate = a.FinishedAiringDate,
                                Status = (AnimeStatus)a.Status,
                                Title = a.Title,
                                EnglishTitle = a.Info.Synonyms?.FirstOrDefault(x => x.Type == 1)?.Title,
                                JapaneseTitle = a.Info.Synonyms?.FirstOrDefault(x => x.Type == 2)?.Title,
                                Score = a.Score,
                                Type = (AnimeType)a.Type,
                                YoutubeTrailerId = a.Info.Info.YoutubeTrailerId,
                                WallpaperId = sv.GenerateId(w ?? "notfound.jpg"),
                                MasterId = a.Id,
                            };

                            Console.WriteLine("Uploading Cover: " + anime.CoverId);
                            sv.UploadFile(new FileUpload()
                            {
                                OldId = a.Info.Poster ?? "notfound.jpg",
                                Type = FileType.Cover,
                                NewId = anime.CoverId
                            });

                            Console.WriteLine("Uploading Wallpaper: " + anime.WallpaperId);
                            sv.UploadFile(new FileUpload()
                            {
                                OldId = w ?? "notfound.jpg",
                                Type = FileType.Wallpaper,
                                NewId = anime.WallpaperId
                            });

                            List<string> names = a.Genres.Select(x => x.Name).ToList();

                            List<Genre> ges = new List<Genre>();
                            foreach (string n in names)
                                ges.Add(await db.Genres.FirstOrDefaultAsync(x => x.Name == n));

                            foreach (var g in ges)
                                anime.AnimeGenres.Add(new AnimeGenre() { Anime = anime, Genre = g });

                            anime.AddViewCount(a.Info.Info.UsersCompleted ?? 0);

                            wb.AddAnime(anime.Id, anime.Title);
                        /*else
                        {
                            animeAlreadyAdded = true;

                            if (anime.MasterId == -3 || anime.MasterId == -2 || anime.MasterId == -1)
                            {
                                Console.WriteLine("Skipping anime " + a.Id + " because it already was added by Kibii.");
                                continue;
                            }
                            else
                            {
                                Console.WriteLine("Skipping anime " + a.Id + " because it already was added by the Main Upload.");

                                if (anime.StartedAiringDate != a.StartedAiringDate)
                                    anime.StartedAiringDate = a.StartedAiringDate;

                                if (anime.Status != (AnimeStatus)a.Status)
                                    anime.Status = (AnimeStatus)a.Status;

                                if (anime.Type != (AnimeType)a.Type)
                                    anime.Type = (AnimeType)a.Type;
                            }

                        } */

                        int? episodeID = 1;

                        foreach (MEpisode e in resp.Episodes)
                        {
                            bool episodeAlreadyAdded = false;

                            try
                            {
                                    var episode = new Episode()
                                    {
                                        Title = e.Info.Title,
                                        Duration = Convert.ToInt32(e.Info.Duration ?? a.Info.Info.EpisodeLength?.ToString() ?? "0"),
                                        Description = e.Info.Description,
                                        Aired = (e.Info.Aired ?? anime.StartedAiringDate ?? new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)),
                                        EpisodeNumber = Convert.ToInt32(e.Info.Episode ?? "0"),
                                        ThumbnailId = sv.GenerateId(e.Thumbnail ?? "notfound.jpg"),
                                        Type = (AnimeType)e.Info.Type,
                                        MasterId = e.Info.Id ?? -1,
                                    };

                                    Console.WriteLine("Uploading Thumbnail: " + episode.ThumbnailId);

                                    sv.UploadFile(new FileUpload()
                                    {
                                        OldId = e.Thumbnail ?? "notfound.jpg",
                                        Type = FileType.Episode,
                                        NewId = episode.ThumbnailId
                                    });
                                
                                /* else
                                {
                                    episodeAlreadyAdded = true;
                                    if (episode.MasterId == -3 || episode.MasterId == -1)
                                    {
                                        if (Debug)
                                            Console.WriteLine("Skipping episode " + e.Info.Id + " because it already was added by Kibii.");
                                        continue;
                                    }
                                    else
                                    {
                                        if (Debug)
                                            Console.WriteLine("Skipping episode " + e.Info.Id + " because it already was added by the Main Upload.");
                                    }
                                } */

                                //episodeID = e.Info.Id;
                                using (var re = new HttpRequestMessage(HttpMethod.Get, $"https://www.masterani.me/anime/watch/{a.Slug}/{episodeID}"))
                                {
                                    requestNotRatelimited = false;
                                    HttpResponseMessage resta = null;

                                    do
                                    {
                                        resta = await client.SendAsync(Clone(re));
                                        if ((int)resta.StatusCode == 429)
                                        {
                                            System.Console.WriteLine("Ratelimited! Retrying after " + resta.Headers.GetValues("Retry-After").FirstOrDefault() + " seconds.");
                                            await Task.Delay(Convert.ToInt32(rest.Headers.GetValues("Retry-After").FirstOrDefault()) * 1000);
                                        }
                                        else
                                        {
                                            requestNotRatelimited = true;
                                        }
                                    }
                                    while (!requestNotRatelimited);
                                    string htmldocresponse = await resta.Content.ReadAsStringAsync();
                                    var document = new HtmlDocument();
                                    document.LoadHtml(htmldocresponse);

                                    string title = document.DocumentNode.SelectSingleNode("//head/title").InnerText;

                                    Console.WriteLine(a.Id + " - " + e.Info.Id + " - " + title.Replace("- Masterani", ""));

                                    if (resta.StatusCode == System.Net.HttpStatusCode.NotFound)
                                    {
                                        System.Console.WriteLine("===========\nEpisode does not exist! Going to next series.\n===========");
                                        System.Console.WriteLine($"Total episodes in {a.Title}: {--episodeID}");
                                        break;
                                    }
                                    else
                                    {
                                        episodeID++;
                                        var data = document.DocumentNode.SelectSingleNode("//body/script").Descendants()
                                            .FirstOrDefault();
                                        string jsresult = data.InnerText.Trim();

                                        jsresult = jsresult.Replace("var args = ", "").Replace("anime: {", "\"anime\": {").Replace("mirrors: [", "\"mirrors\": [").Replace("auto_update: [", "\"auto_update\": [");
                                        //    Console.WriteLine(jsresult);

                                        MirrorInfo d = JsonConvert.DeserializeObject<MirrorInfo>(jsresult, settings);

                                        if (e.Mirrors == null)
                                            e.Mirrors = new List<Mirror>();

                                        e.Mirrors = d.Mirrors ?? new List<Mirror>();

                                        foreach (var m in e.Mirrors)
                                        {
                                            try
                                            {
                                                var ee = await db.Blobs.FirstOrDefaultAsync(x => x.MasterId == m.Id);
                                                var embed = new Blob()
                                                {
                                                    Quality = m.Quality ?? 360,
                                                    BlobId = m.EmbedId,
                                                    BlobPrefix = m.Host.EmbedPrefix,
                                                    BlobSuffix = m.Host.EmbedSuffix,
                                                    Dubbed = ((m.Type ?? 0) == 1 ? false : true),
                                                    Name = m.Host.Name,
                                                    MasterId = m.Id
                                                };

                                                episode.Blobs.Add(embed);
                                                wb.AddEmbed(embed.Id, embed.Name);

                                            }
                                            catch (Exception ex)
                                            {
                                                Blacklist b = new Blacklist()
                                                {
                                                    Model = BlacklistModel.Embed,
                                                    AffectedId = (m.Id).ToString(),
                                                    Reason = "Exception while adding, " + ex.ToString(),
                                                    Type = BlacklistType.BrokenEmbed,
                                                    Author = "MSI_SCRPR"
                                                };

                                                await db.Blacklists.AddAsync(b);
                                            }
                                        }

                                        if (!episodeAlreadyAdded)
                                            anime.Episodes.Add(episode);

                                        FinishedEpisodes.Add(e.Info.Id ?? 0);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                Blacklist b = new Blacklist()
                                {
                                    Model = BlacklistModel.Episode,
                                    AffectedId = (e.Info.Id ?? -1).ToString(),
                                    Reason = "Exception while adding, " + ex.ToString(),
                                    Type = BlacklistType.BrokenEpisode,
                                    Author = "MSI_SCRPR"
                                };

                                await db.Blacklists.AddAsync(b);
                            }
                        }

                        if (!animeAlreadyAdded)
                            await db.Anime.AddAsync(anime);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Anime ID " + a.Id + " skipped!");
                        Skipped.Add(a.Id);

                        Blacklist b = new Blacklist()
                        {
                            Model = BlacklistModel.Anime,
                            AffectedId = (a.Id).ToString(),
                            Reason = "Exception while adding, " + ex.ToString(),
                            Type = BlacklistType.BrokenAnime,
                            Author = "MSI_SCRPR"
                        };

                        await db.Blacklists.AddAsync(b);

                        File.WriteAllText("Skipped.json", JsonConvert.SerializeObject(Skipped, settings2));
                        File.WriteAllText("Finished.json", JsonConvert.SerializeObject(Finished, settings2));
                        File.WriteAllText("FinishedEpisodes.json", JsonConvert.SerializeObject(FinishedEpisodes, settings2));
                    }
                }

                Console.WriteLine($"Anime '{a.Title}' {cc} out of {Anime.Count} finished");
                Finished.Add(a.Id);
                File.WriteAllText("Skipped.json", JsonConvert.SerializeObject(Skipped, settings2));
                File.WriteAllText("Finished.json", JsonConvert.SerializeObject(Finished, settings2));
                File.WriteAllText("FinishedEpisodes.json", JsonConvert.SerializeObject(FinishedEpisodes, settings2));
                await db.SaveChangesAsync();

            }

            Console.WriteLine($"Total Anime List: {Anime.Count}");
            File.WriteAllText("Anime.json", JsonConvert.SerializeObject(Anime, settings2));
            File.WriteAllText("Skipped.json", JsonConvert.SerializeObject(Skipped, settings2));
            File.WriteAllText("Finished.json", JsonConvert.SerializeObject(Finished, settings2));
            File.WriteAllText("FinishedEpisodes.json", JsonConvert.SerializeObject(FinishedEpisodes, settings2));

            await Task.Delay(-1);
            return 0;

        }

        private static List<int> GetFinishedEpisodes()
        {
            if (!File.Exists("Finished.json"))
                return null;
            else return JsonConvert.DeserializeObject<List<int>>(File.ReadAllText("Finished.json"));
        }

        private static List<int> GetFinishedAnime()
        {
            if (!File.Exists("FinishedEpisodes.json"))
                return null;
            else return JsonConvert.DeserializeObject<List<int>>(File.ReadAllText("FinishedEpisodes.json"));
        }

        public static HttpRequestMessage Clone(HttpRequestMessage req)
        {
            HttpRequestMessage clone = new HttpRequestMessage(req.Method, req.RequestUri)
            {
                Content = req.Content,
                Version = req.Version
            };

            foreach (KeyValuePair<string, object> prop in req.Properties)
            {
                clone.Properties.Add(prop);
            }

            foreach (KeyValuePair<string, IEnumerable<string>> header in req.Headers)
            {
                clone.Headers.TryAddWithoutValidation(header.Key, header.Value);
            }

            return clone;
        }

        static string JsonPrettyPrint(string json)
        {
            if (string.IsNullOrEmpty(json))
                return string.Empty;

            json = json.Replace(Environment.NewLine, "").Replace("\t", "");

            StringBuilder sb = new StringBuilder();
            bool quote = false;
            bool ignore = false;
            char last = ' ';
            int offset = 0;
            int indentLength = 2;

            foreach (char ch in json)
            {
                switch (ch)
                {
                    case '"':
                        if (!ignore) quote = !quote;
                        break;
                    case '\\':
                        if (quote && last != '\\') ignore = true;
                        break;
                }

                if (quote)
                {
                    sb.Append(ch);
                    if (last == '\\' && ignore) ignore = false;
                }
                else
                {
                    switch (ch)
                    {
                        case '{':
                        case '[':
                            sb.Append(ch);
                            sb.Append(Environment.NewLine);
                            sb.Append(new string(' ', ++offset * indentLength));
                            break;
                        case '}':
                        case ']':
                            sb.Append(Environment.NewLine);
                            sb.Append(new string(' ', --offset * indentLength));
                            sb.Append(ch);
                            break;
                        case ',':
                            sb.Append(ch);
                            sb.Append(Environment.NewLine);
                            sb.Append(new string(' ', offset * indentLength));
                            break;
                        case ':':
                            sb.Append(ch);
                            sb.Append(' ');
                            break;
                        default:
                            if (quote || ch != ' ') sb.Append(ch);
                            break;
                    }
                }
                last = ch;
            }

            return sb.ToString().Trim();
        }



    }
}
