﻿using System.Collections.Generic;

namespace KibiiTV.RateLimit
{
    public class IpRateLimitPolicies
    {
        public List<IpRateLimitPolicy> IpRules { get; set; }
    }
}
