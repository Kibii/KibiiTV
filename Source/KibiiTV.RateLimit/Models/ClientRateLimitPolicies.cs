﻿using System.Collections.Generic;

namespace KibiiTV.RateLimit
{
    public class ClientRateLimitPolicies
    {
        public List<ClientRateLimitPolicy> ClientRules { get; set; }
    }
}
