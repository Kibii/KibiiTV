﻿using KibiiTV.Data.Affiliates;
using KibiiTV.Data.Misc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KibiiTV.Models.Api
{
    public class CommunityApplicationModel
    {
        [Required(AllowEmptyStrings = false)]
        [MyStringLength(2, 32, "Username")]
        public string Username { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MyStringLength(64, 3, "Full name")]
        public string FullName { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MyStringLength(64, 3, "Country")]
        public string Country { get; set; }

        [MyStringLength(256, 3, "Email")]
        [Required(AllowEmptyStrings = false)]
        public string Email { get; set; }

        [MyStringLength(5000, "Your text data")]
        [Required(AllowEmptyStrings = false)]
        public string UserReason { get; set; } // Why do you want to be a part of Kibii?

        [MyStringLength(5000, "Your text data")]
        [Required(AllowEmptyStrings = false)]
        public string UserDetails { get; set; } // Tell us a little bit about yourself

        [MyStringLength(5000, "Your text data")]
        [Required(AllowEmptyStrings = false)]
        public string UserReferredFrom { get; set; } // How did you discover Kibii?

        [MyStringLength(5000, "Your text data")]
        public string FavoriteAnime { get; set; }

        [Required]
        public GenderType Gender { get; set; }

        [Required]
        public ApplicationType Type { get; set; }

        public List<ApiSocialModel> SocialMedia { get; set; }
    }
}
