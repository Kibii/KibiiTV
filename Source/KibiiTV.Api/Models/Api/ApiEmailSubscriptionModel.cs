﻿using KibiiTV.Data.Broadcast;
using System.ComponentModel.DataAnnotations;

namespace KibiiTV.Models.Api
{
    public class ApiEmailSubscriptionModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Invalid email")]
        public string Email { get; set; }

        public string AnimeId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Subscription type is required")]
        public EmailSubscriptionType Type { get; set; }
    }
}
