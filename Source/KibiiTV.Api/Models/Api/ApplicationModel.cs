﻿using KibiiTV.Data.Misc;
using System;
using System.ComponentModel.DataAnnotations;

namespace KibiiTV.Models.Api
{
    public class ApplicationModel
    {
        //[RegularExpression(@"^\S*$", ErrorMessage = "Username cannot have spaces")]

        [Required(AllowEmptyStrings = false)]
        [MyStringLength(2, 32, "Username")]
        public string Username { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MyStringLength(64, 3, "Full name")]
        public string FullName { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MyStringLength(64, 3, "Country")]
        public string Country { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MyStringLength(64, 3, "City")]
        public string City { get; set; }

        [MyStringLength(256, 3, "Email")]
        [Required(AllowEmptyStrings = false)]
        public string Email { get; set; }

        [MyStringLength(24, "Phone number")]
        public string PhoneNumber { get; set; }

        [MyStringLength(32, "Discord username")]
        public string DiscordName { get; set; }

        [MyStringLength(16, 3, "Twitter")]
        public string Twitter { get; set; }

        [MyStringLength(256, "LinkedIn")]
        public string LinkedIn { get; set; }

        [MyStringLength(256, "Website")]
        public string Website { get; set; }

        [MyStringLength(5000, "Your text data")]
        [Required(AllowEmptyStrings = false)]
        public string UserReason { get; set; } // Why do you want to be a part of Kibii?

        [MyStringLength(5000, "Your text data")]
        [Required(AllowEmptyStrings = false)]
        public string UserAcceptanceReason { get; set; } // Why should we accept you as a staff member of Kibii? Why do you feel that you are fit for this job?

        [MyStringLength(5000, "Your text data")]
        [Required(AllowEmptyStrings = false)]
        public string UserDetails { get; set; } // Tell us a little bit about yourself

        [MyStringLength(5000, "Your text data")]
        [Required(AllowEmptyStrings = false)]
        public string UserReferredFrom { get; set; } // How did you discover Kibii?

        [MyStringLength(5000, "Your text data")]
        public string UserExperince { get; set; } // Tell us a little about your previous experinces, if any.

        [MyStringLength(5000, "Your text data")]
        public string FavoriteAnime { get; set; }

        [Required]
        public GenderType Gender { get; set; }

        public bool HasExperince { get; set; } // Do you have experince in this role or title?
        public string RoleId { get; set; }
        public string GoogleResumeLink { get; set; }
    }

    public class MyStringLengthAttribute : StringLengthAttribute
    {
        public string Name { get; set; }

        public MyStringLengthAttribute(int maximumLength)
            : base(maximumLength)
        { }

        public MyStringLengthAttribute(int maximumLength, string name)
            : base(maximumLength)
        {
            Name = name;
        }

        public MyStringLengthAttribute(int maximumLength, int minimumLength)
            : base(maximumLength)
        {
            MinimumLength = minimumLength;
        }

        public MyStringLengthAttribute(int maximumLength, int minimumLength, string name)
            : base(maximumLength)
        {
            MinimumLength = minimumLength;
            Name = name;
        }


        public override bool IsValid(object value)
        {
            string val = Convert.ToString(value);
            if (val.Length < base.MinimumLength || val.Length > base.MaximumLength)
                base.ErrorMessage = $"{Name} must be in between {MinimumLength} to {MaximumLength} in length";
            return base.IsValid(value);
        }
    }
}
