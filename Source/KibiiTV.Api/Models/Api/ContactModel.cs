﻿using KibiiTV.Data.Support;
using System.ComponentModel.DataAnnotations;

namespace KibiiTV.Models.Api
{
    public class ContactModel
    {
        [MyStringLength(64, "Name")]
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [MyStringLength(256, "Email")]
        [Required(AllowEmptyStrings = false)]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MyStringLength(128, "Subject")]
        public string Subject { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MyStringLength(6500, "Message")]
        public string Message { get; set; }

        [Required]
        public TicketType Type { get; set; }
    }
}
