﻿using KibiiTV.Data.Sessions;
using System;

namespace KibiiTV.Models.Api
{
    public class ApiSessionModel
    {
        public string Id { get; set; }
        public bool Valid { get; set; }
        public string Key { get; set; }
        public SessionType Type { get; set; }
        public DateTime TimeOfDeath { get; set; }

        public static ApiSessionModel From(KibiiSession session)
        {
            var model = new ApiSessionModel()
            {
                Id = session.Id,
                Key = session.Token,
                Valid = session.Valid,
                Type = session.Type,
            };

            if (session.Type == SessionType.API)
                model.TimeOfDeath = session.StartedAt.AddMinutes(30);
            else if (session.Type == SessionType.Embed)
                model.TimeOfDeath = session.StartedAt.AddMinutes(10);
            else if (session.Type == SessionType.View)
                model.TimeOfDeath = session.StartedAt.AddHours(4);

            return model;
        }
    }
}
