﻿using KibiiTV.Data.Support;
using System.ComponentModel.DataAnnotations;

namespace KibiiTV.Models.Api
{
    public class ReportModel
    {
        [MyStringLength(64, "Name")]
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [MyStringLength(512, 3, "URL")]
        public string ReportedLink { get; set; }

        [MyStringLength(256, "Email")]
        [Required(AllowEmptyStrings = false)]
        public string Email { get; set; }

        [MyStringLength(128, "Subject")]
        public string Subject { get; set; }

        [MyStringLength(6500, "Message")]
        public string Message { get; set; }

        [Required]
        public ReportType Type { get; set; }
    }
}
