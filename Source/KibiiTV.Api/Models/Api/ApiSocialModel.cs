﻿using KibiiTV.Data.Misc;

namespace KibiiTV.Models.Api
{
    public class ApiSocialModel
    {
        public SocialType Type { get; set; }
        public string Data { get; set; }
    }
}