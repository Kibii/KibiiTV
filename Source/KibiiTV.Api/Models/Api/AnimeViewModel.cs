﻿using KibiiTV.Data.Media;
using System;
using System.Collections.Generic;

namespace Kibii.Models.Api
{
    public class AnimeViewModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string JapaneseTitle { get; set; }
        public string EnglishTitle { get; set; }
        public string YoutubeTrailerId { get; set; }
        public string Description { get; set; }
        public AnimeType Type { get; set; }
        public AnimeStatus Status { get; set; }
        public DateTime Timestamp { get; set; }
        public DateTime? StartedAiringDate { get; set; }
        public DateTime? FinishedAiringDate { get; set; }
        public string WallpaperId { get; set; }
        public string CoverId { get; set; }
        public string AgeRating { get; set; }
        public double Score { get; set; }
        public int Views { get; set; }
        public int EpisodeCount { get; set; }
        public IEnumerable<string> GenreIds { get; set; }
        public int MalId { get; set; }
        public int AnnictId { get; set; }
    }
}
