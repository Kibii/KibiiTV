﻿using KibiiTV.Data.Sessions;

namespace KibiiTV.Models
{
    public class NewSessionModel
    {
        public string AnimeId { get; set; }
        public string EpisodeId { get; set; }
        public SessionType Type { get; set; } = SessionType.API;
        public bool AdsOptedOut { get; set; } = false;
        public string CaptchaKey { get; set; }
    }
}