﻿using KibiiTV.Data.Sessions;

namespace KibiiTV.Models
{
    public class SessionResponse
    {
        public bool Success { get; set; }
        public KibiiSession Session { get; set; }
    }
}