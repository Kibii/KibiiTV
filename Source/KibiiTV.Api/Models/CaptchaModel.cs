﻿using Newtonsoft.Json;

namespace KibiiTV.Models
{
    public class CaptchaModel
    {
        [JsonProperty("success")] //This prop is needed since Google doesn't want to give us Pascal
        public bool Success { get; set; }

        //TODO Implement the rest of the captcha.
    }
}
