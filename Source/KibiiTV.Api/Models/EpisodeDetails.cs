﻿namespace Kibii.Models
{
    public class EpisodeDetails
    {
        public EpisodeDetails() { }

        public EpisodeDetails(string name, string posterId)
        {
            Name = name;
            PosterId = posterId;
        }

        public EpisodeDetails(string id, string name, string posterId)
        {
            EpisodeId = id;
            Name = name;
            PosterId = posterId;
        }

        public string EpisodeId { get; set; }
        public string Name { get; set; }
        public string PosterId { get; set; }
    }
}
