﻿using KibiiTV.Data.Sessions;
using KibiiTV.Models;
using KibiiTV.Models.Api;
using KibiiTV.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Controllers.Authentication
{
    [Route("/api/sessions")]
    public class SessionController : KibiiController
    {
        private ILogger l;
        private ISessionService sessionService;
        private ICaptchaService captchaService;

        public SessionController(ISessionService sessionService, ICaptchaService captchaService, ILogger<SessionController> logger)
        {
            this.sessionService = sessionService;
            this.captchaService = captchaService;
            l = logger;
        }

        [Route("")]
        [AcceptVerbs("POST")]
        public async Task<IActionResult> GenerateSessionAsync([FromBody] NewSessionModel Model = null) //get session types, data, ect
        {
            string secret = Global.Configuration.GetValue<string>("GoogleCaptchaSecret");
            string ip = Request.Headers["CF-Connecting-IP"].FirstOrDefault() ?? Request.HttpContext.Connection.RemoteIpAddress.ToString();

            if (Model == null)
                return BadRequest(new { Code = 0, Message = "400 Bad Request" });

            if (Model.Type == SessionType.API)
            {
                var session = await sessionService.CreateSessionAsync(SessionType.API);
                return Ok(ApiSessionModel.From(session));
            }
            else if (Model.Type == SessionType.Embed)
            {
           //       if (!await captchaService.ValidateCaptchaAsync(secret, Model.CaptchaKey, ip))
           //         return BadRequest(new { Code = 1000, Message = "Invalid Captcha" });

                var session = await sessionService.CreateSessionAsync(SessionType.Embed);
                return Ok(ApiSessionModel.From(session));
            }
            else if (Model.Type == SessionType.View)
            {
                var session = await sessionService.CreateSessionAsync(SessionType.View, Model.EpisodeId, Model.AnimeId);
                return Ok(ApiSessionModel.From(session));
            }
            else
            {
                return BadRequest(new { Code = 0, Message = "400 Bad Request" });
            }
        }

        [Route("{key}/continue")]
        [AcceptVerbs("POST")]
        public async Task<IActionResult> ContinueViewSessionAsync(string key)
        {
            try
            {
                var session = await sessionService.ContinueViewSessionAsync(key);

                if (session.Success)
                    return Ok(new { Success = session.Success, Session = ApiSessionModel.From(session.Session) });
                else
                    return NotFound(new { Code = 0, Message = "404 Not Found" });
            }
            catch (Exception thrownException)
            {
                l.LogCritical(new EventId(1005, "ERR_VIEWCOUNT_BUMP"), thrownException, $"An exception occured while updating the viewcount for '{key}'");
                Response.StatusCode = 500;
                return Json(new { Code = 0, Message = "500 Internal Server Error" });
            }
        }
    }
}
