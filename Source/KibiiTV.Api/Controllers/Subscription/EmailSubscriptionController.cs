﻿using KibiiTV.Data;
using KibiiTV.Data.Broadcast;
using KibiiTV.Data.Media;
using KibiiTV.Data.Sessions;
using KibiiTV.Filters;
using KibiiTV.Models.Api;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace KibiiTV.Controllers.Subscription
{
    [Captcha]
    [ValidateModel]
    [Route("/api/subscribe")]
    [RequireSession(SessionType.API)]
    public class EmailSubscriptionController : KibiiController
    {
        private ILogger l;
        private KibiiContext db;

        public EmailSubscriptionController(ILogger<EmailSubscriptionController> logger, KibiiContext kibiiContext)
        {
            l = logger;
            db = kibiiContext;
        }

        [Route("email")]
        public async Task<IActionResult> SubscribeToEmailAsync(ApiEmailSubscriptionModel Model)
        {
            Anime a = null;
            if (Model.AnimeId != null)
                a = await db.Anime.FirstOrDefaultAsync(x => x.Id == Model.AnimeId);

            EmailSubscription emailSubscription = new EmailSubscription()
            {
                Email = Model.Email,
                Type = Model.Type,
                Ip = GetUserIp()
            };

            a.EmailSubscriptions.Add(emailSubscription);
            await db.SaveChangesAsync();

            return NoContent();
        }
    }
}
