﻿using KibiiTV.Data.Media;
using KibiiTV.Filters;
using KibiiTV.Repositories.Media;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace KibiiTV.Controllers.Administration
{
    [RequireRoot]
    [Route("/api/administration/media")]
    public class MediaController : KibiiController
    {
        private IAnimeRepository animeRepository;
        private IEpisodeRepository episodeRepository;
        private ILogger l;

        public MediaController(IAnimeRepository animeRepository, IEpisodeRepository episodeRepository, ILoggerFactory loggerFactory)
        {
            this.animeRepository = animeRepository;
            this.episodeRepository = episodeRepository;
            l = loggerFactory.CreateLogger("MediaAdministrationController");
        }

        [Route("anime")]
        [AcceptVerbs("POST")]
        public async Task<IActionResult> AddAnimeAsync([FromBody] Anime anime)
        {
            if (!ModelState.IsValid)
                return BadRequest(new { Code = 400, Message = "400 Bad Request" });

            await animeRepository.AddAsync(anime);
            await animeRepository.SaveAsync();

            return Json(await animeRepository.GetByIdAsync(anime.Id));
        }

        [Route("episodes")]
        [AcceptVerbs("POST")]
        public async Task<IActionResult> AddEpisodeAsync([FromQuery] string animeId, [FromBody] Anime anime)
        {
            if (!ModelState.IsValid)
                return BadRequest(new { Code = 400, Message = "400 Bad Request" });

            await animeRepository.AddAsync(anime);
            await animeRepository.SaveAsync();

            return Json(await animeRepository.GetByIdAsync(anime.Id));
        }

        [Route("blobs")]
        [AcceptVerbs("POST")]
        public async Task<IActionResult> AddBlobAsync([FromQuery] string episodeId, [FromBody] Blob embed)
        {
            if (!ModelState.IsValid)
                return BadRequest(new { Code = 400, Message = "400 Bad Request" });

            var ep = await episodeRepository.GetByIdAsync(episodeId);

            episodeRepository.AddEmbed(ep, embed);
            await episodeRepository.SaveAsync();

            return Json(episodeRepository.GetBlobById(embed.Id));
        }

        [Route("genres")]
        [AcceptVerbs("POST")]
        public async Task<IActionResult> AddGenreAsync([FromBody] Genre genre)
        {
            if (!ModelState.IsValid)
                return BadRequest(new { Code = 400, Message = "400 Bad Request" });

            await animeRepository.AddGenreAsync(genre);
            await animeRepository.SaveAsync();

            return Json(await animeRepository.GetGenreByIdAsync(genre.Id));
        }
    }
}