﻿using KibiiTV.Middleware;
using Microsoft.AspNetCore.Mvc;

namespace KibiiTV.Controllers
{
    public class KibiiController : Controller
    {
        [NonAction]
        public string GetUserIp() =>
            KibiiLoggingMiddleware.GetIpHelper(Request);
    }
}
