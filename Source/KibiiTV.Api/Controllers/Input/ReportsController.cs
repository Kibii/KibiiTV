﻿using KibiiTV.Data;
using KibiiTV.Data.Sessions;
using KibiiTV.Data.Support;
using KibiiTV.Filters;
using KibiiTV.Models.Api;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace KibiiTV.Controllers.Input
{
    [Captcha]
    [ValidateModel]
    [Route("/api/reports")]
    [RequireSession(SessionType.API)]
    public class ReportsController : KibiiController
    {
        private ILogger l;
        private KibiiContext db;

        public ReportsController(ILogger<ReportsController> logger, KibiiContext context)
        {
            db = context;
            l = logger;
        }

        [Route("submit")]
        public async Task<IActionResult> SubmitReportAsync(ReportModel Model)
        {
            Report report = new Report()
            {
                Email = Model.Email ?? "N/A-P",
                Message = Model.Message ?? "N/A-P",
                Name = Model.Name ?? "N/A-P",
                ReportedLink = Model.ReportedLink ?? "N/A-P",
                Subject = Model.Subject ?? "N/A-P",
                Type = Model.Type,
                Ip = GetUserIp()
            };

            await db.Reports.AddAsync(report);
            await db.SaveChangesAsync();

            return NoContent();
        }
    }
}
