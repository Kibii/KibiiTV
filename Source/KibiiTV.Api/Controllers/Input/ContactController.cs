﻿using KibiiTV.Data;
using KibiiTV.Data.Sessions;
using KibiiTV.Data.Support;
using KibiiTV.Filters;
using KibiiTV.Models.Api;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace KibiiTV.Controllers.Input
{
    [Captcha]
    [ValidateModel]
    [Route("/api/contact")]
    [RequireSession(SessionType.API)]
    public class ContactController : KibiiController
    {
        private ILogger l;
        private KibiiContext db;

        public ContactController(ILogger<ContactController> logger, KibiiContext context)
        {
            db = context;
            l = logger;
        }

        [Route("submit")]
        public async Task<IActionResult> SubmitContactFormAsync(ContactModel Model)
        {
            Ticket ticket = new Ticket()
            {
                Email = Model.Email,
                Message = Model.Message,
                Name = Model.Name,
                Subject = Model.Subject,
                Type = Model.Type,
                Ip = GetUserIp()
            };

            await db.Tickets.AddAsync(ticket);
            await db.SaveChangesAsync();

            return NoContent();
        }
    }
}
