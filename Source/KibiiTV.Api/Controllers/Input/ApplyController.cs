﻿using KibiiTV.Data;
using KibiiTV.Data.Affiliates;
using KibiiTV.Data.Misc;
using KibiiTV.Data.Sessions;
using KibiiTV.Data.Staff;
using KibiiTV.Filters;
using KibiiTV.Models.Api;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace KibiiTV.Controllers.Input
{
    [Captcha]
    [ValidateModel]
    [Route("/api/apply")]
    [RequireSession(SessionType.API)]
    public class ApplyController : KibiiController
    {
        private ILogger l;
        private KibiiContext db;

        public ApplyController(ILogger<ApplyController> logger, KibiiContext context)
        {
            db = context;
            l = logger;
        }

        [Route("staff")]
        public async Task<IActionResult> ApplyForStaffAsync(ApplicationModel Model)
        {
            StaffRole role = await db.StaffRoles.FirstOrDefaultAsync(x => x.Id == Model.RoleId);

            if (role == null)
                return NotFound(new { Code = 0, Message = "404 Not Found" });

            StaffApplication app = new StaffApplication()
            {
                City = Model.City,
                Country = Model.Country,
                DiscordName = Model.DiscordName ?? "N/A-P",
                Email = Model.Email,
                HasExperince = Model.HasExperince,
                UserExperince = Model.UserExperince ?? "N/A-P",
                UserAcceptanceReason = Model.UserAcceptanceReason,
                UserDetails = Model.UserDetails,
                FullName = Model.FullName,
                Username = Model.Username,
                Gender = Model.Gender,
                GoogleResumeLink = Model.GoogleResumeLink ?? "N/A-P",
                Status = ApprovalStatus.Pending,
                LinkedIn = Model.LinkedIn ?? "N/A-P",
                Website = Model.Website ?? "N/A-P",
                PhoneNumber = Model.PhoneNumber,
                Twitter = Model.Twitter ?? "N/A-P",
                UserReason = Model.UserReason ?? "N/A-P",
                UserReferredFrom = Model.UserReferredFrom ?? "N/A-P",
                Ip = GetUserIp()
            };

            role.Applications.Add(app);
            await db.SaveChangesAsync();

            return NoContent();
        }

        [Route("affiliate")]
        public async Task<IActionResult> ApplyForAffiliateAsync(CommunityApplicationModel Model)
        {
            CommunityApplication communityApplication = new CommunityApplication()
            {
                Country = Model.Country,
                Email = Model.Email,
                Username = Model.Username,
                FullName = Model.FullName,
                Status = ApprovalStatus.Pending,
                Type = ApplicationType.Affiliate,
                UserDetails = Model.UserDetails ?? "N/A-P",
                UserReason = Model.UserReason ?? "N/A-P",
                UserReferredFrom = Model.UserReferredFrom ?? "N/A-P",
                Gender = Model.Gender,
                Ip = GetUserIp(),
            };

            foreach (var s in Model.SocialMedia)
            {
                communityApplication.SocialMedia.Add(new Social
                {
                    Type = s.Type,
                    Data = s.Data,
                });
            }

            await db.Applications.AddAsync(communityApplication);
            await db.SaveChangesAsync();

            return NoContent();
        }

        [Route("partner")]
        public async Task<IActionResult> ApplyForPartnerAsync(CommunityApplicationModel Model)
        {
            CommunityApplication communityApplication = new CommunityApplication()
            {
                Country = Model.Country,
                Email = Model.Email,
                Username = Model.Username,
                FullName = Model.FullName,
                Status = ApprovalStatus.Pending,
                Type = ApplicationType.Partner,
                UserDetails = Model.UserDetails ?? "N/A-P",
                UserReason = Model.UserReason,
                UserReferredFrom = Model.UserReferredFrom,
                Gender = Model.Gender,
                Ip = GetUserIp(),
            };

            foreach (var s in Model.SocialMedia)
            {
                communityApplication.SocialMedia.Add(new Social
                {
                    Type = s.Type,
                    Data = s.Data,
                });
            }

            await db.Applications.AddAsync(communityApplication);
            await db.SaveChangesAsync();

            return NoContent();
        }
    }
}
