﻿using KibiiTV.Data;
using KibiiTV.Data.Broadcast;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace KibiiTV.Controllers.Information
{
    [Route("/api/news")]
    public class NewsController : KibiiController
    {
        private ILogger l;
        private KibiiContext db;

        public NewsController(ILogger<NewsController> logger, KibiiContext context)
        {
            db = context;
            l = logger;
        }

        [Route("")]
        public async Task<IActionResult> GetNewsAsync([FromQuery] int page = 0, [FromQuery] string sort = "desc")
        {
            sort = sort == "asc" ? "asc" : "desc";
            page = page >= 1 ? page : 1;

            IQueryable<News> News = sort == "desc"
                ? db.News.OrderByDescending(x => x.Timestamp)
                : db.News.OrderBy(x => x.Timestamp);

            IPagedList<News> NewsList = await News.ToPagedListAsync(page, 5);

            return Json(new
            {
                Page = page,
                PageCount = NewsList.PageCount,
                PerPage = 5,
                Results = await NewsList.ToListAsync()
            });
        }

        [Route("latest")]
        public async Task<IActionResult> GetLatestNewsAsync() =>
            Json(await db.News.OrderBy(x => x.Timestamp).FirstOrDefaultAsync());

        [Route("{id}")]
        public async Task<IActionResult> GetNewsById(string id)
        {
            News news = await db.News.FirstOrDefaultAsync(x => x.Id == id);

            if (news == null)
                return NotFound(new { Code = 0, Message = "404 Not Found" });

            return Json(news);
        }
    }
}
