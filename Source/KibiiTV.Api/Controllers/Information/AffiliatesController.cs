﻿using KibiiTV.Data;
using KibiiTV.Data.Affiliates;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Controllers.Information
{
    [Route("/api/affiliates")]
    public class AffiliatesController : KibiiController
    {
        private ILogger l;
        private KibiiContext db;

        public AffiliatesController(ILogger<AffiliatesController> logger, KibiiContext context)
        {
            db = context;
            l = logger;
        }

        [Route("")]
        public async Task<IActionResult> GetAffiliatesAsync() =>
            Json(await db.Affiliates
                .Include(x => x.SocialMedia)
                .ToListAsync());

        [Route("latest")]
        public async Task<IActionResult> GetLatestAffiliateAsync() =>
            Json(await db.Affiliates
                .Include(x => x.SocialMedia)
                .OrderBy(x => x.Timestamp)
                .FirstOrDefaultAsync());

        [Route("{id}")]
        public async Task<IActionResult> GetAffiliateByIdAsync(string id)
        {
            Affiliate affiliate = await db.Affiliates
                .Include(x => x.SocialMedia)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (affiliate == null)
                return NotFound(new { Code = 0, Message = "404 Not Found" });

            return Json(affiliate);
        }
    }
}
