﻿using KibiiTV.Data;
using KibiiTV.Data.Legal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Controllers.Information
{
    [Route("/api/terms")]
    public class TermsController : KibiiController
    {
        private ILogger l;
        private KibiiContext db;

        public TermsController(ILogger<TermsController> logger, KibiiContext context)
        {
            db = context;
            l = logger;
        }

        [Route("")]
        public async Task<IActionResult> GetTermsAsync() => 
            await GetLatestTermsAsync();

        [Route("latest")]
        public async Task<IActionResult> GetLatestTermsAsync() =>
            Json(await db.TermsOfService
                .OrderBy(x => x.Timestamp)
                .FirstOrDefaultAsync());

        [Route("{id}")]
        public async Task<IActionResult> GetTermsByIdAsync(string id)
        {
            TermsOfService terms = await db.TermsOfService
                .FirstOrDefaultAsync(x => x.Id == id);

            if (terms == null)
                return NotFound(new { Code = 0, Message = "404 Not Found" });

            return Json(terms);
        }
    }
}
