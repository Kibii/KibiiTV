﻿using KibiiTV.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace KibiiTV.Controllers.Information
{
    [Route("/api/notifications")]
    public class NotificationController : KibiiController
    {
        private ILogger l;
        private KibiiContext db;

        public NotificationController(ILogger<NotificationController> logger, KibiiContext kibiiContext)
        {
            db = kibiiContext;
            l = logger;
        }

        [Route("all")]
        public async Task<IActionResult> GetNotificationsAsync([FromQuery] int page = 1)
        {
            page = page >= 1 ? page : 1;

            var list = await db.Notifications.ToPagedListAsync(page, 10);

            return Json(new
            {
                Page = page,
                PageCount = list.PageCount,
                PerPage = 10,
                Results = list
            });
        }

        [Route("available")]
        public async Task<IActionResult> GetAvaliableNotificationsAsync() =>
            Json(await db.Notifications
                .Where(x => !x.Expired)
                .ToListAsync());

        [Route("latest")]
        public async Task<IActionResult> GetLatestNotificationAsync() =>
            Json(await db.Notifications
                .OrderByDescending(x => x.Timestamp)
                .FirstOrDefaultAsync());
    }
}
