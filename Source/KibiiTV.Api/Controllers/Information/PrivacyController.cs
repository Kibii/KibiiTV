﻿using KibiiTV.Data;
using KibiiTV.Data.Legal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Controllers.Information
{
    [Route("/api/privacy")]
    public class PrivacyController : KibiiController
    {
        private ILogger l;
        private KibiiContext db;

        public PrivacyController(ILogger<PrivacyController> logger, KibiiContext context)
        {
            db = context;
            l = logger;
        }

        [Route("")]
        public async Task<IActionResult> GetPrivacyAsync() =>
            await GetLatestPrivacyAsync();

        [Route("latest")]
        public async Task<IActionResult> GetLatestPrivacyAsync() =>
            Json(await db.PrivacyPolicies.OrderBy(x => x.Timestamp).FirstOrDefaultAsync());

        [Route("{id}")]
        public async Task<IActionResult> GetPrivacyByIdAsync(string id)
        {
            PrivacyPolicy privacyPolicy = await db.PrivacyPolicies.FirstOrDefaultAsync(x => x.Id == id);

            if (privacyPolicy == null)
                return NotFound(new { Code = 0, Message = "404 Not Found" });

            return Json(privacyPolicy);
        }
    }
}
