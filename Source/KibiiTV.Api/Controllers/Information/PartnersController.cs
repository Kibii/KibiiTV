﻿using KibiiTV.Data;
using KibiiTV.Data.Affiliates;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Controllers.Information
{
    [Route("/api/partners")]
    public class PartnersController : KibiiController
    {
        private ILogger l;
        private KibiiContext db;

        public PartnersController(ILogger<PartnersController> logger, KibiiContext context)
        {
            db = context;
            l = logger;
        }

        [Route("")]
        public async Task<IActionResult> GetPartnersAsync() =>
            Json(await db.Partners
                .Include(x => x.SocialMedia)
                .ToListAsync());

        [Route("latest")]
        public async Task<IActionResult> GetLatestPartnerAsync() =>
            Json(await db.Partners
                .Include(x => x.SocialMedia)
                .OrderBy(x => x.Timestamp)
                .FirstOrDefaultAsync());

        [Route("{id}")]
        public async Task<IActionResult> GetPartnerByIdAsync(string id)
        {
            Partner partner = await db.Partners
                .Include(x => x.SocialMedia)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (partner == null)
                return NotFound(new { Code = 0, Message = "404 Not Found" });

            return Json(partner);
        }
    }
}
