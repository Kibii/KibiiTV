﻿using KibiiTV.Data;
using KibiiTV.Data.Media;
using KibiiTV.Data.Staff;
using KibiiTV.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Controllers.Information
{
    [Route("/api/staff")]
    public class StaffController : KibiiController
    {
        private KibiiContext db;

        public StaffController(KibiiContext context)
        {
            db = context;
        }

        [Route("available")]
        public async Task<IActionResult> GetStaffRolesAsync() =>
            Json(await db.StaffRoles.ToListAsync());

        [Route("")]
        public async Task<IActionResult> GetStaffAsync() =>
            Json(await db.Staff
                .Include(x => x.SocialMedia)
                .ToListAsync());

        [Route("latest")]
        public async Task<IActionResult> GetLatestStaffAsync() =>
            Json(await db.Staff
                .Include(x => x.SocialMedia)
                .OrderBy(x => x.Timestamp)
                .FirstOrDefaultAsync());

        [Route("{id}")]
        public async Task<IActionResult> GetStaffByIdAsync(string id)
        {
            StaffMember member = await db.Staff
                .Include(x => x.SocialMedia)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (member == null)
                return NotFound(new { Code = 0, Message = "404 Not Found" });

            return Json(member);
        }

        [Route("/dummy")]
        [Disabled]
        public IActionResult Test()
        {
            var g = new Genre() { Name = "Comedy" };
            var g2 = new Genre() { Name = "School" };
            var g3 = new Genre() { Name = "Slice of Life" };

            db.Genres.Add(g);
            db.Genres.Add(g2);
            db.Genres.Add(g3);

            var a = new Anime()
            {
                AgeRating = "PG-13 - Teens 13 or older",
                Score = 4.15,
                Status = AnimeStatus.Completed,
                StartedAiringDate = DateTime.Parse("2016-04-09"),
                FinishedAiringDate = DateTime.Parse("2016-06-25"),
                Description = "For high school student Tanaka, the act of being listless is a way of life. Known for his inattentiveness and ability to fall asleep anywhere, Tanaka prays that each day will be as uneventful as the last, seeking to preserve his lazy lifestyle however he can by avoiding situations that require him to exert himself. Along with his dependable friend Oota who helps him with tasks he is unable to accomplish, the lethargic teenager constantly deals with events that prevent him from experiencing the quiet and peaceful days he longs for.↵[Written by MAL Rewrite]",
                Title = "Tanaka-kun wa Itsumo Kedaruge",
                YoutubeTrailerId = "r0U83wtmk28",
                WallpaperId = "1798kBAAFcpD",
                EnglishTitle = "Tanaka-kun is Always Listless",
                JapaneseTitle = "田中くんはいつもけだるげ",
                CoverId = "1798K3g6ZYpt",
                Type = AnimeType.Series,
            };

            a.AddViewCount(1839);

            var gg = new AnimeGenre() { Genre = g, Anime = a };
            var gg2 = new AnimeGenre() { Genre = g2, Anime = a };
            var gg3 = new AnimeGenre() { Genre = g3, Anime = a };

            db.Add(gg);
            db.Add(gg2);
            db.Add(gg3);

            var e = new Episode()
            {
                ThumbnailId = "319075DASLJDZ",
                Aired = DateTime.Parse("2016-04-09"),
                EpisodeNumber = 1,
                Title = "Tanaka-kun and Ohta-kun",
                Description = "On a very pleasant morning, Tanaka is taking a nap in the school courtyard. His classmate, Ohta, carries him to class, but Tanaka continues to sleep on his desk. Tanaka’s listlessness is nothing new to his classmates, so no one is surprised. But, the usually lazy Tanaka is willing to participate in PE. His goal is to train his body and prevent his limbs from going numb or injuring himself from being lazy all day. He teams up with Ohta to play badminton...",
            };

            e.AddViewCount(80);

            var em = new Blob()
            {
                Name = "MP4Upload",
                BlobId = "2uqub4twvblp",
                BlobPrefix = "https://mp4upload.com//embed-",
                BlobSuffix = ".html",
                Quality = 720,
            };

            e.Blobs.Add(em);
            a.Episodes.Add(e);
            db.Anime.Add(a);
            db.SaveChanges();

            return Ok(new { ok = true });
        }

        private class StaffMemberApiModel
        {
            public StaffMemberApiModel()
            {
                Titles = new List<string>();
            }

            public string Name;
            public string RealName;
            public string Discord;
            public string WithKibiiSince;
            public string Credits;
            public List<string> Titles;
        }

        [Route("old")]
        public IActionResult GetStaffMeme()
        {
            var l = new List<StaffMemberApiModel>();

            var apap = new StaffMemberApiModel()
            {
                Name = "apap04",
                RealName = "Andreas",
                Discord = "apap04#6217",
                WithKibiiSince = "May 3, 2017",
                Credits = "Main development with Yara (pre-kibii) and Kibii ever since joining us. Huge help with frontend development and the Otako project."
            };
            apap.Titles.Add("General Developer");

            var marvin = new StaffMemberApiModel()
            {
                Name = "NurMarvin",
                RealName = "Marvin",
                Discord = "NurMarvin#1337",
                WithKibiiSince = "April 17, 2018",
                Credits = "Jumped in last-moment to help us with the front-end. He's a great help and has been supporting Kibii from the back since the start. We're glad to introduce him to the team."
            };
            marvin.Titles.Add("Frontend Developer");

            var terre = new StaffMemberApiModel()
            {
                Name = "Terrewee",
                RealName = "N/A",
                Discord = "Terrewee#9440",
                WithKibiiSince = "October 9, 2017",
                Credits = "Main designer for Kibii, designed our logo and helped progress Kibii much farther with her words of wisdom and motivation. Thank you Terre, you're one of the main reasons we've built Kibii."
            };
            terre.Titles.Add("Lead Designer");

            var megumin = new StaffMemberApiModel()
            {
                Name = "Megumin",
                RealName = "Kristian",
                Discord = "Megumin#4949",
                WithKibiiSince = "May 7, 2017",
                Credits = "Lead development, motivation, and always there to help us out with programming issues. Huge thanks with our anime series and backend work. Good job buddy."
            };
            megumin.Titles.Add("Backend Developer");

            var bin = new StaffMemberApiModel()
            {
                Name = "bin",
                RealName = "Sarmad",
                Discord = "bin#0160",
                WithKibiiSince = "July 8, 2015",
                Credits = "Owner, leader, manager of this project. His dream. Management of people, gathering staff, growing the community, and main development lead. \"Hopefully we'll get somewhere with Kibii TV...\""
            };
            bin.Titles.Add("CEO, CTO, Founder & Owner");
            bin.Titles.Add("Management");
            bin.Titles.Add("Lead Developer");
            bin.Titles.Add("Community Leader");

            var arise = new StaffMemberApiModel()
            {
                Name = "Arise",
                RealName = "N/A",
                Discord = "Arise#2049",
                WithKibiiSince = "December 8, 2017",
                Credits = "Supporting Kibii since join with money, motivation, emergency help and management. Super big thanks to Arise for giving all of us hype and motiviation to keep going."
            };
            arise.Titles.Add("Management");

            var shiro = new StaffMemberApiModel()
            {
                Name = "Shiro",
                RealName = "Andrew",
                Discord = "Zero#6249",
                WithKibiiSince = "July 9, 2015",
                Credits = "Overall secondry leader of the project, management of moderation, people, and emergency situations. Big thanks to Shiro for making the Kibii project possible."
            };
            shiro.Titles.Add("Co-Founder");
            shiro.Titles.Add("Community Leader");
            shiro.Titles.Add("Management");

            var kuroi = new StaffMemberApiModel()
            {
                Name = "Kuroi",
                RealName = "N/A",
                Discord = "Ｍｉｚｕ　閲ぶ宛#4899",
                WithKibiiSince = "April 14, 2017",
                Credits = "I wanted to personally make this for Kuroi. You jump started Yara/Kibii with your first designs for that community chat-bot. You helped us out so much, and without that jumpstart we never would've made it here. This one's a special thank you directly from us. The Kibii staff welcomes you and thanks you for your contribution, pride, and motivation towards making Kibii what it is today.",
            };
            kuroi.Titles.Add("Former Designer");
            kuroi.Titles.Add("Thank you~");

            l.Add(bin);
            l.Add(kuroi);
            l.Add(apap);
            l.Add(shiro);
            l.Add(arise);
            l.Add(terre);
            l.Add(marvin);
            l.Add(megumin);

            return Ok(l);
        }
    }
}
