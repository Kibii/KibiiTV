﻿using KibiiTV.Data.Sessions;
using KibiiTV.Filters;
using KibiiTV.Repositories.Media;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections;
using System.Threading.Tasks;
using X.PagedList;

namespace KibiiTV.Controllers.Media
{
    [Route("/api/episodes")]
    [RequireSession(SessionType.API)]
    public class EpisodeController : KibiiController
    {
        private IEpisodeRepository db;
        private ILogger l;

        public EpisodeController(IEpisodeRepository repo, ILogger<EpisodeController> logger)
        {
            db = repo;
            l = logger;
        }

        [Route("{id}")]
        public async Task<IActionResult> GetEpisodeByIdAsync(string id)
        {
            var episode = await db.GetByIdAsync(id);

            if (episode == null)
            {
                l.LogDebug($"Episode with ID {id} was not found.");
                return NotFound(new { Code = 0, Message = "404 Not Found" });
            }

            return Ok(episode);
        }

        [Route("{id}/blobs")]
        public async Task<IActionResult> GetEpisodeBlobsAsync(string id)
        {
            var episode = await db.GetByIdAsync(id);

            if (episode == null)
            {
                l.LogDebug($"Episode with ID {id} was not found.");
                return NotFound(new { Code = 0, Message = "404 Not Found" });
            }

            var blobs = await episode.Blobs.ToListAsync();

            if (blobs.Count < 1)
                return Ok(new ArrayList());

            return Ok(blobs);
        }
    }
}
