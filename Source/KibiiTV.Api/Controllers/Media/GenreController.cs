﻿using KibiiTV.Data.Sessions;
using KibiiTV.Filters;
using KibiiTV.Repositories.Media;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace KibiiTV.Controllers.Media
{
    [Route("/api/genres")]
    [RequireSession(SessionType.API)]
    public class GenreController : KibiiController
    {
        private ILogger l;
        private IAnimeRepository db;

        public GenreController(IAnimeRepository animeRepository, ILogger<GenreController> logger)
        {
            db = animeRepository;
            l = logger;
        }

        [Route("all")]
        [AcceptVerbs("GET")]
        public async Task<IActionResult> GetAllGenresAsync() =>
            Ok(await db.GetAllGenresAsync());

        [Route("{id}")]
        [AcceptVerbs("GET")]
        public async Task<IActionResult> GetGenreByIdAsync(string id) //get session types, data, ect
        {
            var genre = await db.GetGenreByIdAsync(id);

            if (genre == null)
            {
                l.LogDebug($"Genre with ID {id} was not found.");
                return NotFound(new { Code = 0, Message = "404 Not Found" });
            }

            return Ok(genre);
        }
    }
}
