﻿using Kibii.Models;
using Kibii.Services;
using KibiiTV.Data.Sessions;
using KibiiTV.Filters;
using KibiiTV.Repositories.Media;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace KibiiTV.Controllers.Media
{
    [Route("/api/blobs")]
    [RequireSession(SessionType.API)]
    public class BlobController : KibiiController
    {
        private readonly IBlobTemplateService _blobTemplateService;
        private readonly IEpisodeRepository _episodeRepository;
        private readonly ILogger _logger;

        public BlobController(IBlobTemplateService blobTemplateService, IEpisodeRepository episodeRepository, ILogger<BlobController> logger)
        {
            _blobTemplateService = blobTemplateService;
            _episodeRepository = episodeRepository;
            _logger = logger;
        }

        [Route("{id}")]
        public async Task<IActionResult> GetBlobAsync(string id)
        {
            var blob = await _episodeRepository.GetBlobByIdAsync(id);
            if (blob == null)
            {
                _logger.LogDebug($"Blob with ID {id} not found.");
                return NotFound(new { Code = 0, Message = "404 Not Found" });
            }

            _logger.LogDebug($"Queried blob for episode ID {blob.Episode.Id} for blob ID {id}.");
            return Ok(blob);
        }

        [Route("{id}/generate")]
        public async Task<IActionResult> GenerateBlobAsync(string id)
        {
            var blob = await _episodeRepository.GetBlobByIdAsync(id);

            if (blob == null)
            {
                _logger.LogDebug($"Blob with ID {id} not found.");
                return NotFound(new { Code = 0, Message = "404 Not Found" });
            }

            EpisodeDetails details = null;

            if (blob.Name == "Aika" || blob.Name == "Kibii Cloud")
                details = await _episodeRepository.GetEpisodeDetailsAsync(blob.EpisodeId);

            string html = _blobTemplateService.GenerateBlob(blob, details);

            _logger.LogDebug($"Finished generating real-time blob for episode ID {blob.EpisodeId} for blob ID {id}.");

            Response.ContentType = "text/html";
            return Content(html);
        }
    }
}
