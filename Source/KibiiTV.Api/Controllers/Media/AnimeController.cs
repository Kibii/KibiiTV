﻿using Kibii.Models.Api;
using KibiiTV.Data;
using KibiiTV.Data.Media;
using KibiiTV.Data.Sessions;
using KibiiTV.Filters;
using KibiiTV.Repositories.Media;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using X.PagedList;

namespace KibiiTV.Controllers.Media
{
    // todo
    //
    // add gender to partner, affiliate, and staff apps - done
    // complete changes to the db - done
    // finish the staff and staff application api - done
    // finish partner/affiliate and partner/affiliate application api - done
    // finish administrative endpoints
    // finish notification services - done
    // finish news services - done
    // finish reporting and contact services - done
    // finish terms and privacy - done
    // implement email subscribing - halfway done
    // implement webhooks
    // implement rss feed - scrapped, too old
    // implement custom scraper - done
    // implement dark theme
    // fix up random bugs
    // finish up the landing page
    // implement meta tag support - done
    // move to ASP.NET Core + VueJS for frontend - done
    // complete the viewcounter and implement a better version - done
    // fix up frontend issues and add full anime info 
    // fix up frontend css and all the margin issues - halfway done
    // implement better embeds & no ad support (see: carl) - halfway done
    // create and implement custom CDN and file tracker for it. - done
    // implement avaliable staff roles table. - done
    // verify all unused using statements, namespaces, and fix all folders - done
    // move all of this to gitlab issues and state them
    // add attributes for managing text size.
    // rename blobs to embeds - done
    // ----------------------------------------------
    // finish documentation
    // realtime api services
    // captcha - done
    // auto build - done
    // ratelimiting - done
    // genre endpoint - done
    // move to mysql - done
    // 404 middleware - done
    // rewrite anime search - done
    // middleware extentions - done 
    // finish session services - done
    // implement api error codes - done
    // implement popularity system - done
    // move controller actions to async - done
    // move repository services to async - done
    // add the changes specified in docs to the backend - done
    // auto deploy - done, but need a way to bypass prompt with git.
    // move repos, middleware, services, runners, models to their correct location. - done

    [Route("/api/anime")]
    [RequireSession(SessionType.API)]
    public class AnimeController : KibiiController
    {
        private const int ResultsPerPage = 50;
        private static string[] sortTypes = { "popular", "added", "rating", "date", "views", "title" };
        private ILogger l;
        private IAnimeRepository animeRepository;
        private KibiiContext db;

        public AnimeController(IAnimeRepository repository, KibiiContext context, ILogger<AnimeController> logger)
        {
            l = logger;
            animeRepository = repository;
            db = context;
        }

        [Route("search")] // Make a SearchModel
        public async Task<IActionResult> GetAnimeAsync([FromQuery] string query = "", [FromQuery] int page = 1, [FromQuery] string genre = "", [FromQuery] string sort = "rating", [FromQuery] string order = "descending")
        {
            if (query.Length > 2000 || sort.Length > 50 || order.Length > 50)
            {
                l.LogDebug($"Search query was empty and/or invalid; returning status code 400.");
                return BadRequest(new { Code = 0, Message = "400 Bad Request" });
            }

            if (page < 1) page = 1;

            var q = query?.ToUpper()?.Replace('+', ' ') ?? "";

            IQueryable<Anime> anime = String.IsNullOrWhiteSpace(query) != true ? animeRepository.GetAllBySearch(q) : animeRepository.GetAllQuery();
            IPagedList<dynamic> output;

            if (!String.IsNullOrWhiteSpace(genre))
                anime = anime.Where(a => a.AnimeGenres.Any(c => c.GenreId == genre));

            FixSortAndOrder(ref sort, ref order); // This handy and neat little function fixes up the strings for us.

            // Non-OCD friendly switch of different sorts and their logic.
            switch (sort)
            {
                case "popular_desc": // Viewcount total descending
                    output = await anime
                        .Select(toViewModel)
                        .OrderByDescending(x => x.Views)
                        .ToPagedListAsync(page, 60);
                    break;
                case "popular_asc": // Viewcount total ascending
                    output = await anime
                        .Select(toViewModel)
                        .OrderBy(x => x.Views)
                        .ToPagedListAsync(page, 60);
                    break;
                case "rating_desc": // Rating descending 
                    output = await anime
                        .Select(toViewModel)
                        .OrderByDescending(x => x.Score)
                        .ToPagedListAsync(page, 60);
                    break;
                case "rating_asc": // Rating ascending 
                    output = await anime
                        .Select(toViewModel)
                        .OrderBy(x => x.Score)
                        .ToPagedListAsync(page, 60);
                    break;
                case "title_desc": // Title descending 
                    output = await anime
                        .Select(toViewModel)
                        .OrderByDescending(x => x.Title)
                        .ToPagedListAsync(page, 60);
                    break;
                case "title_asc": // Title ascending 
                    output = await anime
                        .Select(toViewModel)
                        .OrderBy(x => x.Title)
                        .ToPagedListAsync(page, 60);
                    break;
                case "date_desc": // Aired date descending 
                    output = await anime
                        .Select(toViewModel)
                        .OrderByDescending(x => x.StartedAiringDate)
                        .ToPagedListAsync(page, 60);
                    break;
                case "date_asc": // Aired date ascending 
                    output = await anime
                        .Select(toViewModel)
                        .OrderBy(x => x.StartedAiringDate)
                        .ToPagedListAsync(page, 60);
                    break;
                case "added_desc": // Added timestamp today descending 
                    output = await anime
                        .Select(toViewModel)
                        .OrderByDescending(x => x.Timestamp)
                        .ToPagedListAsync(page, 60);
                    break;
                case "added_asc": // Added timestamp ascending 
                    output = await anime
                        .Select(toViewModel)
                        .OrderBy(x => x.Timestamp)
                        .ToPagedListAsync(page, 60);
                    break;
                default: // Score descending 
                    output = await anime
                        .Select(toViewModel)
                        .OrderByDescending(x => x.Score)
                        .ToPagedListAsync(page, 60);
                    break;
            }

            var count = output.Count;
            l.LogDebug($"Queried {count} series of anime from search query '{query}' with sort '{sort.ToUpper()}' with genre '{(String.IsNullOrWhiteSpace(genre) ? genre.ToString() : "N/A")}' on page {page} out of {output.PageCount} pages.");

            if (count == 0)
                l.LogDebug($"Search query '{query}' with sort '{sort.ToUpper()}' with genre '{(String.IsNullOrWhiteSpace(genre) ? genre.ToString() : "N/A")}' returned no results.");

            return Ok(new
            {
                Page = page,
                PerPage = 60,
                PageCount = output.PageCount,
                Results = await output.ToListAsync()
            });
        }

        private readonly Expression<Func<Anime, AnimeViewModel>> toViewModel = anime => new AnimeViewModel
        {
            Id = anime.Id,
            Title = anime.Title,
            JapaneseTitle = anime.JapaneseTitle,
            Views = anime.Views,
            Type = anime.Type,
            Timestamp = anime.Timestamp,
            EnglishTitle = anime.EnglishTitle,
            CoverId = anime.CoverId,
            WallpaperId = anime.WallpaperId,
            Status = anime.Status,
            AgeRating = anime.AgeRating,
            Description = anime.Description,
            EpisodeCount = anime.Episodes.Count(),
            Score = anime.Score,
            StartedAiringDate = anime.StartedAiringDate,
            FinishedAiringDate = anime.FinishedAiringDate,
            MalId = anime.MalId,
            AnnictId = anime.AnnictId,
            GenreIds = anime.AnimeGenres.Select(c => c.GenreId)
        };

        [Route("{id}")]
        public async Task<IActionResult> GetAnimeByIdAsync(string id)
        {
            var anime = await animeRepository.GetByIdAsync(id);
            if (anime == null)
            {
                l.LogDebug($"Anime with ID {id} not found.");
                return NotFound(new { Code = 0, Message = "404 Not Found" });
            }

            l.LogDebug($"Queried the anime '{anime.Title}' by ID {id}.");

            return Ok(new
            {
                Info = anime,
                Episodes = await anime.Episodes.OrderBy(x => x.EpisodeNumber).ToListAsync(),
                Pvs = await anime.AnimePvs.ToListAsync(),
            });
        }

        private void FixSortAndOrder(ref string sort, ref string order)
        {
            sort = sort.ToLower();
            order = order.ToLower();

            if (order != "ascending" || order != "descending")
                order = "descending";

            if (!sortTypes.Contains(sort))
                sort = sortTypes[0];

            sort = sort + "_" + (order == "descending" ? "desc" : "asc");
        }
    }
}
