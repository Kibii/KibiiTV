﻿using KibiiTV.Data;
using KibiiTV.Data.Media;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Repositories.Media
{
    public class AnimeRepository : IAnimeRepository
    {
        private KibiiContext db { get; set; }
        private ILoggerFactory LoggerFactory { get; set; }
        private ILogger l { get; set; }

        public AnimeRepository(KibiiContext context, ILoggerFactory factory)
        {
            db = context;
            LoggerFactory = factory;
            l = factory.CreateLogger("AnimeRepository");
        }

        public void AddEpisode(Anime anime, Episode episode) =>
            anime.Episodes.Add(episode);

        public IEnumerable<Anime> GetAll() =>
            db.Anime.Include(x => x.Episodes).Include(x => x.AnimeGenres).ThenInclude(d => d.Genre).ToList();

        //      public IQueryable<Anime> GetAllBySearch(string q) =>
        //      db.Anime.Include(x => x.Views).Include(x => x.Episodes).Include(x => x.AnimeGenres).ThenInclude(d => d.Genre)
        //  .Where(a => a.Title.ToUpper().Contains(q) || a.JapaneseTitle.ToUpper().Contains(q) || a.EnglishTitle.ToUpper().Contains(q));

        public IQueryable<Anime> GetAllBySearch(string q) =>
            db.Anime.Where(a => a.Title.ToUpper().Contains(q) || a.JapaneseTitle.ToUpper().Contains(q) || a.EnglishTitle.ToUpper().Contains(q));

        public IEnumerable<Genre> GetAllGenres() =>
            db.Genres.ToList();

        public Anime GetByEpisodeId(string episodeId) =>
            db.Anime.Where(a => a.Episodes.Count(e => e.Id == episodeId) > 0).FirstOrDefault();

        public Anime GetById(string id) =>
            db.Anime.Include(x => x.Views).Include(x => x.Episodes).Include(x => x.AnimeGenres).ThenInclude(d => d.Genre)
            .FirstOrDefault(a => a.Id == id);

        public Anime GetBySearch(string q) =>
            db.Anime.Include(x => x.Views).Include(x => x.Episodes).Include(x => x.AnimeGenres).ThenInclude(d => d.Genre)
            .Where(a => a.Title.Contains(q) || a.JapaneseTitle.Contains(q) || a.EnglishTitle.Contains(q)).FirstOrDefault();

        public IEnumerable<Genre> GetGenres(Anime anime) =>
            anime.AnimeGenres.Select(g => g.Genre);

        public IEnumerable<Anime> GetInGenre(Genre Genre) =>
            Genre.Anime.Select(a => a.Anime);

        public void Add(Anime anime) =>
            db.Anime.Add(anime);

        public void RemoveEpisode(Anime anime, Episode episode) =>
            anime.Episodes.Remove(episode);

        public void Update(Anime anime) =>
            db.Anime.Update(anime);

        public void Save() =>
            db.SaveChanges();

        public Task SaveAsync() =>
            db.SaveChangesAsync();

        public Genre GetGenre(string g) =>
            db.Genres.FirstOrDefault(p => p.Name.ToUpper() == g.ToUpper());

        public Task<Genre> GetGenreAsync(string g) =>
            db.Genres.FirstOrDefaultAsync(p => p.Name.ToUpper() == g.ToUpper());

        public Task<List<Anime>> GetAllAsync() =>
            db.Anime.Include(x => x.Views).Include(x => x.Episodes).Include(x => x.AnimeGenres).ThenInclude(d => d.Genre).ToListAsync();

        public Task<List<Genre>> GetAllGenresAsync() => db.Genres.ToListAsync();

        public Task<Anime> GetByIdAsync(string id) =>
             db.Anime
            .Include(x => x.Episodes)
            .Include(x => x.AnimePvs)
            .Include(x => x.AnimeGenres)
            .FirstOrDefaultAsync(a => a.Id == id);

        public Task<Anime> GetBySearchAsync(string q) =>
            db.Anime
            .Where(a => 
            a.Title.Contains(q) ||
            a.JapaneseTitle.Contains(q) ||
            a.EnglishTitle.Contains(q))
            .FirstOrDefaultAsync();

        public Task AddAsync(Anime anime) =>
            db.Anime.AddAsync(anime);

        public Task<Genre> GetGenreByIdAsync(string id) =>
            db.Genres.FirstOrDefaultAsync(x => x.Id == id);

        //    public IQueryable<Anime> GetAllQuery() =>
        //      db.Anime.Include(x => x.Views).Include(x => x.Episodes).Include(x => x.AnimeGenres).ThenInclude(d => d.Genre).AsQueryable();

        public IQueryable<Anime> GetAllQuery() =>
           db.Anime
         //   .Include(x => x.AnimeGenres) // We removed these because we'll get better performance, not like
         //   .ThenInclude(c => c.Genre)   // they're being used anyway.
            .AsQueryable();

        public Task AddGenreAsync(Genre genre) =>
            db.Genres.AddAsync(genre);
    }
}
