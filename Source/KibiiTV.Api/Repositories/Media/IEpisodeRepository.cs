﻿using Kibii.Models;
using KibiiTV.Data.Media;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KibiiTV.Repositories.Media
{
    public interface IEpisodeRepository
    {
        Episode GetById(string id);
        Task<Episode> GetByIdAsync(string id);
        IEnumerable<Blob> GetBlobs(Episode episode);
        void Add(Anime anime, Episode episode);
        void Remove(Episode episode);
        void AddEmbed(Episode episode, Blob embed);
        void RemoveEmbed(Episode episode, Blob embed);
        void Update(Episode episode);
        void UpdateBlob(Blob embed);
        void Save();
        Task SaveAsync();
        Blob GetBlobById(string id);
        Task<Blob> GetBlobByIdAsync(string id);
        Task<EpisodeDetails> GetEpisodeDetailsAsync(string id);
    }
}
