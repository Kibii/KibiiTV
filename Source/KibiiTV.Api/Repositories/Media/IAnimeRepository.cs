﻿using KibiiTV.Data.Media;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Repositories.Media
{
    public interface IAnimeRepository
    {
        IEnumerable<Anime> GetAll();
        Task<List<Anime>> GetAllAsync();
        IQueryable<Anime> GetAllBySearch(string query);
        IEnumerable<Anime> GetInGenre(Genre Genre);
        IEnumerable<Genre> GetAllGenres();
        Task<List<Genre>> GetAllGenresAsync();
        IEnumerable<Genre> GetGenres(Anime anime);
        Anime GetById(string id);
        Task<Anime> GetByIdAsync(string id);
        Anime GetByEpisodeId(string episodeId);
        Anime GetBySearch(string query);
        Task<Anime> GetBySearchAsync(string query);
        void Add(Anime anime);
        Task AddAsync(Anime anime);
        void AddEpisode(Anime anime, Episode episode);
        void RemoveEpisode(Anime anime, Episode episode);
        void Update(Anime anime);
        void Save();
        Task SaveAsync();
        Genre GetGenre(string g);
        Task<Genre> GetGenreAsync(string g);
        Task<Genre> GetGenreByIdAsync(string id);
        IQueryable<Anime> GetAllQuery();
        Task AddGenreAsync(Genre anime);
    }
}
