﻿using Kibii.Models;
using KibiiTV.Data;
using KibiiTV.Data.Media;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Repositories.Media
{
    public class EpisodeRepository : IEpisodeRepository
    {
        private KibiiContext _context;

        public EpisodeRepository(KibiiContext context)
        {
            _context = context;
        }

        public void Add(Anime anime, Episode episode) =>
            anime.Episodes.Add(episode);

        public void AddEmbed(Episode episode, Blob embed) =>
            episode.Blobs.Add(embed);

        public Episode GetById(string id) =>
            _context.Episodes.Include(x => x.Blobs).FirstOrDefault(x => x.Id == id);

        public Task<Episode> GetByIdAsync(string id) =>
            _context.Episodes.Include(x => x.Blobs).FirstOrDefaultAsync(x => x.Id == id);

        public Blob GetBlobById(string id) =>
            _context.Blobs.Include(x => x.Episode).FirstOrDefault(x => x.Id == id);

        public Task<Blob> GetBlobByIdAsync(string id) =>
            _context.Blobs.FirstOrDefaultAsync(x => x.Id == id); // Removed .Include()

        public IEnumerable<Blob> GetBlobs(Episode episode) =>
            episode.Blobs;

        public void Remove(Episode episode) =>
            _context.Episodes.Remove(episode);

        public void RemoveEmbed(Episode episode, Blob embed) =>
            _context.Remove(embed);

        public void Save() =>
            _context.SaveChanges();

        public Task SaveAsync() =>
            _context.SaveChangesAsync();

        public void Update(Episode episode) =>
            _context.Episodes.Update(episode);

        public void UpdateBlob(Blob blob) =>
            _context.Blobs.Update(blob);

        public Task<EpisodeDetails> GetEpisodeDetailsAsync(string id) =>
            _context.Episodes.Select(e => new EpisodeDetails(e.Id, e.Title, e.ThumbnailId))
                .FirstOrDefaultAsync(e => e.EpisodeId == id);
    }
}
