﻿using System.Threading.Tasks;

namespace KibiiTV.Runners
{
    public class CacheBackgroundRunner
    {
        // TODO: Implement a proper in-memory and/or redis cache. Create a service that automatically removes items from the cache after it's reached a limit.
        // This cache will replace later versions of the Kibii TV API and lighten load on the overall infrastracture.

        public static async Task Start()
        {
            // To be implemented later.
            while(true)
            {
                await ServiceFunc();
            }
        }

        public static Task ServiceFunc()
        {
            return Task.CompletedTask; //to be used later
        }
    }
}
