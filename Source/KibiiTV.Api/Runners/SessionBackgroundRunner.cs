﻿using KibiiTV.Data;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Runners
{
    public class SessionBackgroundRunner
    {
        public static async Task Start()
        {
            while (true)
            {
                await ServiceFunc();
                await Task.Delay(60000);
            }
        }

        public static async Task ServiceFunc()
        {
            using (var db = new KibiiContext(Global.Configuration.GetConnectionString("Main")))
            {
                var sessions = db.Sessions.Where(x => x.IsDeletable);
                if (sessions != null)
                {
                    db.RemoveRange(sessions);
                    await db.SaveChangesAsync();
                }
            }
        }
    }
}
