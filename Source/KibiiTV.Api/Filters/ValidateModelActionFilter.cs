﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;

namespace KibiiTV.Filters
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext Context)
        {
            if (!Context.ModelState.IsValid)
            {
                var errors = Context.ModelState.Values
                    .SelectMany(c => c.Errors.Select(x => x.ErrorMessage))
                    .ToList();

                Context.Result = new BadRequestObjectResult(new
                {
                    Code = 400,
                    Message = "400 Bad Request",
                    Errors = errors
                });
            }
        }
    }
}
