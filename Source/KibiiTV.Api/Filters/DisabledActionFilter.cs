﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace KibiiTV.Filters
{
    public class DisabledAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext Context)
        {
            var res = new JsonResult(new
            {
                Code = 1001,
                Message = "This endpoint has been disabled"
            });

            res.StatusCode = 403;
            Context.Result = res;
        }
    }
}
