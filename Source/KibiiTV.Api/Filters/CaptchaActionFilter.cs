﻿using KibiiTV.Middleware;
using KibiiTV.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Filters
{
    public class CaptchaAttribute : TypeFilterAttribute
    {
        public CaptchaAttribute() : base(typeof(CaptchaImpl))
        { }

        private class CaptchaImpl : IAsyncActionFilter
        {
            private ICaptchaService captchaService;
            private ILogger l;

            public CaptchaImpl(ICaptchaService cs, ILoggerFactory loggerFactory)
            {
                captchaService = cs;
                l = loggerFactory.CreateLogger("CaptchaVerificationActionFilter");
            }

            public async Task OnActionExecutionAsync(ActionExecutingContext Context, ActionExecutionDelegate next)
            {
                bool validCaptcha = false;

                string header = Context.HttpContext.Request.Headers["X-Kibii-Yokyu-Token"].FirstOrDefault();
                string captcha = Context.HttpContext.Request.Headers["X-Kibii-Captcha"].FirstOrDefault();
                string ip = KibiiLoggingMiddleware.GetIpHelper(Context.HttpContext);
                string secret = Global.Configuration.GetValue<string>("GoogleCaptchaSecret");

                validCaptcha =
                    Global.Configuration.GetValue<string>("RootToken") == header 
                        ? true : string.IsNullOrWhiteSpace(captcha)
                        ? false : await captchaService.ValidateCaptchaAsync(secret, captcha, ip);

                if (!validCaptcha)
                {
                    var res = new BadRequestObjectResult(new
                    {
                        Code = 1003,
                        Message = "Invalid Captcha"
                    });

                    Context.Result = res;
                }
                else await next();
            }
        }
    }
}