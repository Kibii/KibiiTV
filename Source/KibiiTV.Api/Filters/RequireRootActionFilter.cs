﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace KibiiTV.Filters
{
    public class RequireRootAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext Context)
        {
            var header = Context.HttpContext.Request.Headers["Kibii-API-Token"].FirstOrDefault();
            if (Global.Configuration.GetValue<string>("RootToken") != header)
            {
                var res = new JsonResult(new { Code = 0, Message = "401 Unauthorized" });
                res.StatusCode = 401;
                Context.Result = res;
            }
        }
    }
}
