﻿using KibiiTV.Data.Sessions;
using KibiiTV.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Filters
{
    public class RequireSessionAttribute : TypeFilterAttribute
    {
        public RequireSessionAttribute(SessionType sessionType) : base(typeof(RequireSessionImpl))
        {
            Arguments = new object[] { sessionType };
        }

        private class RequireSessionImpl : IAsyncActionFilter
        {
            private ISessionService sessionService;
            private SessionType sessionType;
            private ILogger l;

            public RequireSessionImpl(ISessionService sv, ILoggerFactory loggerFactory, SessionType st)
            {
                sessionService = sv;
                sessionType = st;
                l = loggerFactory.CreateLogger("SessionVerifcationActionFilter");
            }

            public async Task OnActionExecutionAsync(ActionExecutingContext Context, ActionExecutionDelegate next)
            {
                bool validSession = false;
                var key = Context.HttpContext.Request.Headers["X-Session-Key"].FirstOrDefault() ??
                          Context.HttpContext.Request.Query["X-Session-Key"].FirstOrDefault() ??
                          Context.HttpContext.Request.Query["Session-Key"].FirstOrDefault() ??
                          Context.HttpContext.Request.Query["Session"].FirstOrDefault() ??
                          Context.HttpContext.Request.Query["Key"].FirstOrDefault();

                if (!String.IsNullOrWhiteSpace(key))
                {
                    if (sessionType == SessionType.API)
                        validSession = true;
                    else
                    {
                        validSession = await sessionService.ValidateSessionAsync(key, sessionType);
                        l.LogDebug($"Session key '{key}' is '{(validSession ? "VALID" : "INVALID")}' with '{sessionType}' session type.");
                    }
                }

                if (!validSession)
                {
                    var res = new JsonResult(new
                    {
                        Code = 1002,
                        Message = "401 Unauthorized"
                    });

                    res.StatusCode = 401;
                    Context.Result = res;
                }
                else await next();
            }
        }
    }
}
