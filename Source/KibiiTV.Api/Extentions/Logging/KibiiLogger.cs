﻿using KibiiTV.Data.Logging;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace KibiiTV.Extentions.Logging
{
    public class KibiiLogger : ILogger
    {
        private readonly string Name;

        public KibiiLogger(string Name)
        {
            this.Name = Name;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            Task.Run(async () =>
            {
                using (var Context = new LoggingContext())
                {
                    var m = new KibiiLog()
                    {
                        EventId = eventId.Id,
                        EventName = eventId.Name,
                        LogLevel = logLevel.ToString(),
                        Source = Name,
                        Details = formatter(state, exception),
                        Exception = null
                    };

                    try
                    {
                        if (exception != null)
                            m.Exception = exception.ToString();
                    }
                    catch { m.Exception = null; }

                    await Context.Logs.AddAsync(m);
                    await Context.SaveChangesAsync();
                }
            });
        }
    }
}
