﻿using Microsoft.Extensions.Logging;

namespace KibiiTV.Extentions.Logging
{
    public static class KibiiLoggerFactoryExtensions
    {
        public static ILoggerFactory AddKibiiDb(this ILoggerFactory factory)
        {
            factory.AddProvider(new KibiiLoggingProvider());
            return factory;
        }
    }
}
