﻿using Microsoft.Extensions.Logging;

namespace KibiiTV.Extentions.Logging
{
    public static class KibiiLoggingBuilderExtentions
    {
        public static ILoggingBuilder AddKibiiDb(this ILoggingBuilder builder) =>
            builder.AddProvider(new KibiiLoggingProvider());
    }
}
