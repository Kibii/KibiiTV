﻿using Microsoft.Extensions.Logging;

namespace KibiiTV.Extentions.Logging
{
    public class KibiiLoggingProvider : ILoggerProvider
    {
        public ILogger CreateLogger(string Name) => new KibiiLogger(Name);

        public void Dispose()
        {
            // clean up any resources??? 
        }
    }
}
