﻿using KibiiTV.Middleware;
using Microsoft.AspNetCore.Builder;

namespace KibiiTV.Extentions.Middleware
{
    public static class MiddlewareExtentions
    {
        public static IApplicationBuilder UseHttpLogging(this IApplicationBuilder app)
        {
            return app.UseMiddleware<KibiiLoggingMiddleware>();
        }

        public static IApplicationBuilder UseNotFoundResponse(this IApplicationBuilder app)
        {
            return app.UseMiddleware<NotFoundMiddleware>();
        }
    }
}
