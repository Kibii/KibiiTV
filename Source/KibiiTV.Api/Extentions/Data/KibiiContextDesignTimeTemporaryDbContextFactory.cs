﻿using KibiiTV.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace KibiiTV.Extentions.Data
{
    public class KibiiContextDesignTimeTemporaryDbContextFactory : IDesignTimeDbContextFactory<KibiiContext>
    {
        public KibiiContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile("appsettings.Development.json")
                .AddCommandLine(args)
                .AddEnvironmentVariables("KIBII_")
                .Build();

            string connectionString = configuration.GetConnectionString("Main");

            DbContextOptionsBuilder<KibiiContext> builder = new DbContextOptionsBuilder<KibiiContext>();
            builder.UseMySql(connectionString);

            return new KibiiContext(builder.Options);
        }
    }
}
