﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Kibii.Extensions.Configuration.InMemoryJson;
using System;

namespace KibiiTV
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IWebHostBuilder webHostBuilder = WebHost.CreateDefaultBuilder();

            webHostBuilder
                .ConfigureAppConfiguration((builderContext, configuration) =>
                {
                    IHostingEnvironment environment = builderContext.HostingEnvironment;
                    configuration
                        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                        .AddJsonFile($"appsettings.{environment.EnvironmentName}.json", optional: true, reloadOnChange: true)
                        .AddCommandLine(args)
                        .AddEnvironmentVariables("KIBII_");
                })
                .UseKestrel()
                .UseStartup<Global>()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .CaptureStartupErrors(captureStartupErrors: true);

            IWebHost host = webHostBuilder.Build();
            host.Run();
        }
    }
}
