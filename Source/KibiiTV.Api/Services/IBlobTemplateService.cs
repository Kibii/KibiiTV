﻿using Kibii.Models;
using KibiiTV.Data.Media;

namespace Kibii.Services
{
    public interface IBlobTemplateService
    {
        string GenerateBlob(Blob blob, EpisodeDetails details = null);
    }
}
