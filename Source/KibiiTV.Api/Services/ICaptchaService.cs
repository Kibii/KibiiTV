﻿using System.Threading.Tasks;

namespace KibiiTV.Services
{
    public interface ICaptchaService
    {
        bool ValidateCaptcha(string secret, string key, string remoteIp = null);
        Task<bool> ValidateCaptchaAsync(string secret, string key, string remoteIp = null);
    }
}
