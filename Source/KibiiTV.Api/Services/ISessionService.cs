﻿using KibiiTV.Data.Sessions;
using KibiiTV.Models;
using System.Threading.Tasks;

namespace KibiiTV.Services
{
    public interface ISessionService
    {
        KibiiSession CreateSession(SessionType Type, string EpisodeId = null, string AnimeId = null);
        Task<KibiiSession> CreateSessionAsync(SessionType Type, string EpisodeId = null, string AnimeId = null);
        SessionResponse ContinueViewSession(string SessionToken);
        Task<SessionResponse> ContinueViewSessionAsync(string SessionToken);
        bool ValidateSession(string key, SessionType sessionType);
        Task<bool> ValidateSessionAsync(string key, SessionType sessionType);
    }
}
