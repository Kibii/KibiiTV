﻿using KibiiTV.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace KibiiTV.Services
{
    public class CaptchaService : ICaptchaService
    {
        private ILogger l;

        public CaptchaService(ILoggerFactory loggerFactory)
        {
            l = loggerFactory.CreateLogger("CaptchaService");
        }

        public bool ValidateCaptcha(string secret, string key, string remoteIp = null) =>
            ValidateCaptchaAsync(secret, key, remoteIp).GetAwaiter().GetResult();

        public async Task<bool> ValidateCaptchaAsync(string secret, string key, string remoteIp = null)
        {
            string url = "https://www.google.com/recaptcha/api/siteverify?secret=" + secret + "&response=" + key;
            if (!String.IsNullOrWhiteSpace(remoteIp))
                url += "&remoteip=" + remoteIp;

            using (var client = new HttpClient())
            using (var req = new HttpRequestMessage(HttpMethod.Post, url))
            {
                var res = await client.SendAsync(req);
                var json = await res.Content.ReadAsStringAsync();
                var model = JsonConvert.DeserializeObject<CaptchaModel>(json);

                l.LogDebug($"Validated captcha key of '{key}' with response of '{(model.Success ? "SUCCESS" : "FAILURE")}'");
                return model.Success;
            }
        }
    }
}
