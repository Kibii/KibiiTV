﻿using Kibii.Models;
using KibiiTV.Data.Helpers;
using KibiiTV.Data.Media;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace Kibii.Services
{
    public class BlobTemplateService : IBlobTemplateService
    {
        public BlobTemplateService(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;

            if (_htmlTemplate == null)
                _htmlTemplate = GetTemplate();

            _random = new Random();
        }

        private readonly IHostingEnvironment _hostingEnvironment;
        private static string _htmlTemplate;
        private readonly Random _random;

        private static char[] _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuv_".ToCharArray();
        private const string _plyrTags =
            "<link rel=\"stylesheet\" href=\"https://cdn.plyr.io/3.3.7/plyr.css\">" +
            "<script src=\"https://cdn.plyr.io/3.3.7/plyr.polyfilled.js\"></script>";

        public string GenerateBlob(Blob blob, EpisodeDetails details = null)
        {
            if (!_hostingEnvironment.IsProduction())
                _htmlTemplate = GetTemplate();

            string blobUrl = blob.BlobPrefix
                           + blob.BlobId
                           + blob.BlobSuffix;

            if (blob.Name != "Aika" && blob.Name != "Kibii Cloud")
                blobUrl = blobUrl + "#kibii_xsrf="
                          + IdGenerator.NewId()
                          + "&"
                          + RandomCharacter() + Guid.NewGuid().ToString("N")
                          + "="
                          + IdGenerator.NewId();

            return GenerateTemplate(blob, blobUrl, details);
        }

        private string GenerateTemplate(Blob blob, string blobUrl, EpisodeDetails details)
        {
            var replacers = GenerateReplacers();
            string template = _htmlTemplate;

            foreach (KeyValuePair<string, string> replacer in replacers)
            {
                template = template.Replace(replacer.Key, replacer.Value);
            }

            string debugString = _hostingEnvironment.IsProduction() ? "true" : "false";
            string dubbedString = blob.Dubbed ? "true" : "false";
            string adPublisherId = "NONE";
            string blobName = blob.Name?.Replace("'", "\\'");

            template = template.Replace("BLOB_NAME", blobName);
            template = template.Replace("ISDEBUG_RP", debugString);
            template = template.Replace("BLOB_URL", blobUrl);
            template = template.Replace("BLOB_QUALITY", blob.Quality.ToString());
            template = template.Replace("BLOB_DUBBED", dubbedString);
            template = template.Replace("ADS_PUBLISHER_ID", adPublisherId);

            string epName = details?.Name ?? blob.Episode?.Title;
            epName = epName?.Replace("'", "\\'");
            string blobPosterUrl = details?.PosterId == null
                ? (blob.Episode?.ThumbnailId == null
                ? null : "https://deru.kibii.tv/" + blob.Episode?.ThumbnailId)
                : "https://deru.kibii.tv/" + details?.PosterId;

            //move deru.kibii.tv to config lol

            if (blob.Name == "Aika" || blob.Name == "Kibii Cloud")
                template = template.Replace("__PLYR_ASSETS__", _plyrTags);
            else
                template = template.Replace("__PLYR_ASSETS__", string.Empty);

            template = template.Replace("BLOB_EPISODE_NAME", epName);
            template = template.Replace("BLOB_POSTER_URL", blobPosterUrl);

            return template;
        }

        private IDictionary<string, string> GenerateReplacers()
        {
            string[] replacerKeys = new string[]
            {
                "UUID_ZERO_TWO",
                "__UUID_ZERO_ONE",
                "IS_DEBUG",
                "RND_FRAME",
                "RND_DIV",
                "RND_TWO",
                "RND_NN",
                "RND_TYPE",
                "RND_URL",
                "RND_QUALITY",
                "RND_DUBBED",
                "RND_LBL",
                "RND_EP_NAME",
                "RND_POSTER",
                "RND_TMPL",
                "RND_ONE",
                "RANDOM__UUID",
            };

            Dictionary<string, string> replacers = new Dictionary<string, string>();

            for (int i = 0; i < replacerKeys.Length; i++)
            {
                string rplcrKey = replacerKeys[i];
                string rplcrVal = NewId();

                replacers.Add(rplcrKey, rplcrVal);
            }

            return replacers;
        }

        private string NewId()
        {
            string mainIdString = Guid.NewGuid().ToString("N");
            int maxTrim = mainIdString.Length <= 10 ? 5 : 10;

            mainIdString = mainIdString.Substring(_random.Next(0, maxTrim));
            return RandomCharacter() + mainIdString;
        }

        private unsafe string RandomCharacter()
        {
            string charStr = _chars[_random.Next(_chars.Length - 1)].ToString();
            char* bstr = (char*)Marshal.StringToBSTR(charStr);

            string returnString = new string(bstr);
            Marshal.FreeBSTR((IntPtr)bstr);
            charStr = null;

            return returnString;
        }

        private string GetTemplate()
        {
            string fileName = "blob_template.html"; //_hostingEnvironment.IsProduction() ? "blob_template.min.html" : "blob_template.html";
            return File.ReadAllText(fileName);
        }
    }
}
