﻿using KibiiTV.Data;
using KibiiTV.Data.Sessions;
using KibiiTV.Models;
using KibiiTV.Repositories.Media;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Services
{
    public class SessionService : ISessionService
    {
        private KibiiContext db;
        private IAnimeRepository animeDb;
        private IEpisodeRepository episodeDb;

        public SessionService(KibiiContext context, IAnimeRepository repo, IEpisodeRepository repo2)
        {
            db = context;
            animeDb = repo;
            episodeDb = repo2;
        }

        public KibiiSession CreateSession(SessionType Type, string EpisodeId = null, string AnimeId = null)
        {
            var session = new KibiiSession()
            {
                StartedAt = DateTime.Now,
                Type = Type,
                Valid = true
            };

            db.Sessions.Add(session);
            db.SaveChanges();

            return session;
        }

        public async Task<KibiiSession> CreateSessionAsync(SessionType Type, string EpisodeId = null, string AnimeId = null)
        {
            var session = new KibiiSession()
            {
                StartedAt = DateTime.Now,
                Type = Type,
                Valid = true,
                EpisodeId = EpisodeId,
                AnimeId = AnimeId
            };

            await db.Sessions.AddAsync(session);
            await db.SaveChangesAsync();

            return session;
        }

        public SessionResponse ContinueViewSession(string SessionToken)
        {
            var session = db.Sessions.FirstOrDefault(x => x.Token == SessionToken);

            if (session == null)
                return new SessionResponse() { Session = null, Success = false };
            else if (!session.Valid)
                return new SessionResponse() { Session = session, Success = false };

            if (session.ViewValidated)
            {
                var anime = animeDb.GetById(session.AnimeId);
                var episode = episodeDb.GetById(session.EpisodeId);

                if (anime == null || episode == null)
                    throw new InvalidOperationException("The object ID was mislocated.");

                if (episode.EpisodeNumber == 1)
                {
                    anime.AddView();
                    animeDb.Update(anime);
                    animeDb.Save();
                }

                episode.AddView();
                episodeDb.Update(episode);
                episodeDb.Save();

                return new SessionResponse() { Session = session, Success = true };
            }
            else
                return new SessionResponse() { Session = session, Success = false };
        }

        public async Task<SessionResponse> ContinueViewSessionAsync(string SessionToken)
        {
            var session = await db.Sessions.FirstOrDefaultAsync(x => x.Token == SessionToken);

            if (session == null)
                return new SessionResponse() { Session = null, Success = false };
            else if (!session.Valid)
                return new SessionResponse() { Session = session, Success = false };

            if (session.ViewValidated)
            {
                var anime = await animeDb.GetByIdAsync(session.AnimeId);
                var episode = await episodeDb.GetByIdAsync(session.EpisodeId);

                if (anime == null || episode == null)
                    throw new InvalidOperationException("The object ID was mislocated.");

                if (episode.EpisodeNumber == 1)
                {
                    anime.AddView();
                    animeDb.Update(anime);
                    //await animeDb.SaveAsync(); // We don't actually need this since it's all in the same database.
                }

                episode.AddView();
                episodeDb.Update(episode);
                session.Valid = false;
                db.Update(session);
                await episodeDb.SaveAsync(); // So we're just going to save from here.

                return new SessionResponse() { Session = session, Success = true };
            }
            else
                return new SessionResponse() { Session = session, Success = false };
        }

        public bool ValidateSession(string key, SessionType sessionType)
        {
            var session = db.Sessions.FirstOrDefault(x => x.Token == key && x.Type == sessionType);
            if (session == null)
                return false;

            if (!session.IsInState || session.IsDeletable || !session.IsApiValid)
                return false;

            return true;
        }

        public async Task<bool> ValidateSessionAsync(string key, SessionType sessionType)
        {
            var session = await db.Sessions.FirstOrDefaultAsync(x => x.Token == key && x.Type == sessionType);

            if (session == null || !session.IsInState || session.IsDeletable || !session.IsApiValid)
                return false;

            return true;
        }
    }
}
