﻿using Kibii.Services;
using KibiiTV.Data;
using KibiiTV.Extentions.Logging;
using KibiiTV.Extentions.Middleware;
using KibiiTV.RateLimit;
using KibiiTV.Repositories.Media;
using KibiiTV.Runners;
using KibiiTV.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using System;
using System.Threading.Tasks;

namespace KibiiTV
{
    public class Global
    {
         // pls rember to rename the session things >.<
        public Global(IHostingEnvironment env, IConfiguration configuration)
        {
            Configuration = configuration;
            HostingEnvironment = env;
        }

        public static IConfiguration Configuration { get; private set;  }
        public static IHostingEnvironment HostingEnvironment { get; private set;  }
        public static IServiceProvider Services { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddSingleton<IConfiguration>(Configuration);
            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
            });

            services.AddCors();
            services.AddOptions();
            services.AddMemoryCache();
            services.AddLogging(builder =>
            {
                builder.AddConfiguration(Configuration.GetSection("Logging"))
                       .AddConsole()
                       .AddDebug()
                       .AddKibiiDb();
            });

            //load general configuration from appsettings.json
            services.Configure<IpRateLimitOptions>(Configuration.GetSection("IpRateLimiting"));
            services.Configure<IpRateLimitPolicies>(Configuration.GetSection("IpRateLimitPolicies"));

            // inject counter and rules stores
            services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
            services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
            services.AddSingleton<ICaptchaService, CaptchaService>();

            services.AddScoped<IAnimeRepository, AnimeRepository>();
            services.AddScoped<IEpisodeRepository, EpisodeRepository>();
            services.AddScoped<ISessionService, SessionService>();
            services.AddSingleton<IBlobTemplateService, BlobTemplateService>();

            // inject the db for services to use
            services.AddDbContext<KibiiContext>(o => o.UseMySql(Configuration.GetConnectionString("Main")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider services, ILoggerFactory LoggerFactory)
        {
            Services = services;

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseCors(o => o.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseHttpLogging();
            app.UseIpRateLimiting();
            app.UseMvc();
            app.UseNotFoundResponse();

            Task.Factory.StartNew(SessionBackgroundRunner.Start, TaskCreationOptions.LongRunning);
        }
    }
}
