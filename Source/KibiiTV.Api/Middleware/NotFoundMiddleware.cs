﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace KibiiTV.Middleware
{
    public class NotFoundMiddleware
    {
        private readonly RequestDelegate next;

        public NotFoundMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public Task Invoke(HttpContext context)
        {
            context.Response.StatusCode = 404;
            context.Response.ContentType = "application/json";

            var msg = JsonConvert.SerializeObject(new { Code = 0, Message = "404 Not Found" });
            return context.Response.WriteAsync(msg);
        }
    }
}
