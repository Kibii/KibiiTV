﻿using KibiiTV.Data.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Middleware
{
    public class KibiiLoggingMiddleware
    {
        private readonly RequestDelegate next;

        public KibiiLoggingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            await next(context);

            var requestIdFeature = context.Features.Get<IHttpRequestIdentifierFeature>();
            string reqId = null;

            if (requestIdFeature?.TraceIdentifier != null)
            {
                reqId = requestIdFeature.TraceIdentifier;
                try
                {
                    context.Response.Headers["X-Kibii-Yokyu-Id"] = requestIdFeature.TraceIdentifier;
                }
                catch { }
            }

            string scheme = (context.Request.Headers["X-Forwarded-Proto"].FirstOrDefault() ?? context.Request.Scheme ?? (context.Request.IsHttps ? "https" : "http"));
            string urlDisplay = (scheme + "://" + context.Request.Host.Host + context.Request.GetEncodedPathAndQuery());
            string ip = GetIpHelper(context);

            var l = new HttpLog()
            {
                ContentLength = context.Request.ContentLength?.ToString() ?? "0",
                ContentType = context.Request.ContentType,
                Method = context.Request.Method,
                Scheme = scheme,
                Host = context.Request.Host.Host,
                Path = context.Request.Path.ToUriComponent(),
                Query = context.Request.QueryString.ToUriComponent(),
                DisplayUrl = urlDisplay,
                Ip = ip,
                Headers = JsonConvert.SerializeObject(context.Request.Headers),
                UserAgent = context.Request.Headers["User-Agent"].FirstOrDefault(),
                Timestamp = DateTime.Now,
                RequestId = reqId
            };

            using (var Context = new LoggingContext())
            {
                await Context.HttpLogs.AddAsync(l);
                await Context.SaveChangesAsync();
            }
        }

        public static string GetIpHelper(HttpContext context)
        {
            string ip = null;

            if (Global.Configuration.GetValue("BehindProxy", false))
            {
                if (Global.Configuration.GetValue("BehindCloudflare", false))
                {
                    ip = context.Request.Headers["CF-Connecting-IP"].FirstOrDefault()
                         ?? context.Request.Headers["X-Forwarded-For"].FirstOrDefault()
                         ?? context.Connection.RemoteIpAddress.ToString();
                }
                else
                {
                    ip = context.Request.Headers["X-Forwarded-For"].FirstOrDefault()
                         ?? context.Connection.RemoteIpAddress.ToString();
                }
            }
            else
            {
                ip = context.Connection.RemoteIpAddress.ToString();
            }

            return ip;
        }

        public static string GetIpHelper(HttpRequest httpRequest) =>
            GetIpHelper(httpRequest.HttpContext);
    }
}
