﻿using KibiiTV.Data.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace KibiiTV.Data.Media
{
    public class Episode
    {
        public Episode()
        {
            Blobs = new HashSet<Blob>();
            Timestamp = DateTime.Now;
            Id = IdGenerator.NewId();
            MasterId = 0;
        }

        public string Id { get; set; }
        public DateTime Aired { get; set; }
        public DateTime Timestamp { get; set; }
        public string Title { get; set; }
        public string AnimeId { get; set; }
        public int Duration { get; set; }
        public AnimeType Type { get; set; }
        public int EpisodeNumber { get; set; }
        public string Description { get; set; }
        public string ThumbnailId { get; set; }
        public int Views { get; set; }

        [JsonIgnore]
        public virtual ICollection<Blob> Blobs { get; set; }

        [JsonIgnore]
        public Anime Anime { get; set; }

        [JsonIgnore]
        public int MasterId { get; set; }

        public void AddView() =>
            Views++;

        public void AddViewCount(int count) =>
            Views += count;
    }
}