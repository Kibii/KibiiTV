﻿using KibiiTV.Data.Helpers;
using Newtonsoft.Json;
using System;

namespace KibiiTV.Data.Media
{
    public class Blob
    {
        public Blob()
        {
            Timestamp = DateTime.Now;
            Id = IdGenerator.NewId();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string EpisodeId { get; set; }
        public DateTime Timestamp { get; set; }
        public int Quality { get; set; }
        public bool Dubbed { get; set; }

        [JsonIgnore]
        public string BlobPrefix { get; set; }

        [JsonIgnore]
        public string BlobId { get; set; }

        [JsonIgnore]
        public string BlobSuffix { get; set; }

        [JsonIgnore]
        public Episode Episode { get; set; }

        [JsonIgnore]
        public int MasterId { get; set; }
    }
}