﻿namespace KibiiTV.Data.Media
{
    public enum PvType
    {
        Opening = 0,
        Ending = 1,
        OST = 2,
        Misc = 3
    }
}