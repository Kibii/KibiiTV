﻿using KibiiTV.Data.Broadcast;
using KibiiTV.Data.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace KibiiTV.Data.Media
{
    public class Anime
    {
        public Anime()
        {
            MasterId = 0;   
            Id = IdGenerator.NewId();
            Timestamp = DateTime.Now;
            AnimePvs = new HashSet<AnimePv>();
            Episodes = new HashSet<Episode>();
            AnimeGenres = new HashSet<AnimeGenre>();
            EmailSubscriptions = new HashSet<EmailSubscription>();
        }

        public string Id { get; set; }
        public string Title { get; set; }
        public string JapaneseTitle { get; set; }
        public string EnglishTitle { get; set; }
        public string YoutubeTrailerId { get; set; }
        public string Description { get; set; }
        public AnimeType Type { get; set; }
        public AnimeStatus Status { get; set; }
        public DateTime Timestamp { get; set; }
        public DateTime? StartedAiringDate { get; set; }
        public DateTime? FinishedAiringDate { get; set; }
        public string WallpaperId { get; set; }
        public string CoverId { get; set; }
        public string AgeRating { get; set; }
        public double Score { get; set; }
        public int Views { get; set; }
        public int MalId { get; set; }
        public int AnnictId { get; set; }

        [NotMapped]
        public IEnumerable<string> GenreIds => AnimeGenres.Select(c => c.GenreId);

        [NotMapped]
        [JsonIgnore]
        public IEnumerable<Genre> Genres => AnimeGenres.Select(c => c.Genre);

        [JsonIgnore]
        public int MasterId { get; set; }

        [JsonIgnore]
        public virtual ICollection<Episode> Episodes { get; set; }

        [JsonIgnore]
        public virtual ICollection<AnimePv> AnimePvs { get; set; }

        [JsonIgnore]
        public virtual ICollection<AnimeGenre> AnimeGenres { get; set; }

        [JsonIgnore]
        public virtual ICollection<EmailSubscription> EmailSubscriptions { get; set; }

        public void AddView() => Views++;

        public void AddViewCount(int count) =>
            Views += count;
    }
}
