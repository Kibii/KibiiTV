﻿namespace KibiiTV.Data.Media
{
    public enum AnimeType
    {
        Series = 0,
        OVA = 1,
        Movie = 2,
        Special = 3,
        ONA = 4
    }
}