﻿namespace KibiiTV.Data.Media
{
    public enum AnimeStatus
    {
        Completed = 0,
        Ongoing = 1,
        NotYetAired = 2
    }
}