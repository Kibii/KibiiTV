﻿using KibiiTV.Data.Helpers;
using Newtonsoft.Json;

namespace KibiiTV.Data.Media
{
    public class AnimePv
    {
        public AnimePv()
        {
            Id = IdGenerator.NewId();
        }

        public string Id { get; set; }
        public string Data { get; set; }
        public bool IsYoutube { get; set; }
        public PvType Type { get; set; }

        [JsonIgnore]
        public Anime Anime { get; set; }
    }
}
