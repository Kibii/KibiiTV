﻿using KibiiTV.Data.Helpers;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace KibiiTV.Data.Media
{
    public class Genre
    {
        public Genre()
        {
            Anime = new HashSet<AnimeGenre>();
            Id = IdGenerator.NewId();
        }

        public string Id { get; set; }
        public string Name { get; set; }

        [JsonIgnore]
        public virtual ICollection<AnimeGenre> Anime { get; set; }
    }

    public class AnimeGenre
    {
        public string AnimeId { get; set; }
        public string GenreId { get; set; }

        [JsonIgnore]
        public Anime Anime { get; set; }

        [JsonIgnore]
        public Genre Genre { get; set; }
    }
}