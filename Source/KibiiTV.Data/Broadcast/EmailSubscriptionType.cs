﻿namespace KibiiTV.Data.Broadcast
{
    public enum EmailSubscriptionType
    {
        Anime = 0,
        News = 1,
        General = 2
    }
}