﻿using KibiiTV.Data.Helpers;
using System;

namespace KibiiTV.Data.Broadcast
{
    public class Notification
    {
        public Notification()
        {
            Id = IdGenerator.NewId();
            Timestamp = DateTime.Now;
            ExpiresAt = DateTime.Now.AddDays(1);
        }

        public string Id { get; set; }
        public string Author { get; set; } // For keeping record
        public DateTime Timestamp { get; set; } // Also for record
        public bool Expired { get { return DateTime.Now > ExpiresAt; } }
        public DateTime ExpiresAt { get; set; }
        public bool OverrideUserSettings { get; set; } // Override the local user's settings for notifications
        public bool UpdateCache { get; set; } // Forces Kibii to update its local cache
        public bool ForceRefresh { get; set; } // Forces the browser to refresh
        public bool IsDismissable { get; set; } // If the notification is dismissable
        public bool HasIcon { get; set; } // If the notification (alert) should have an icon
        public string Raw { get; set; } // Raw content
        public string Html { get; set; } // HTML content
        public string Title { get; set; } // Title of the content
        public NotificationLevel NotificationLevel { get; set; } // Level of the content such as warning, danger, info, ect
        public NotificationType Type { get; set; } // Type of notification
    }
}