﻿namespace KibiiTV.Data.Broadcast
{
    public enum NotificationType
    {
        Snackbar = 0,
        Bar = 1,
        Toast = 2,
        Modal = 3,
    }
}