﻿using KibiiTV.Data.Helpers;
using KibiiTV.Data.Media;
using Newtonsoft.Json;
using System;

namespace KibiiTV.Data.Broadcast
{
    public class EmailSubscription
    {
        public EmailSubscription()
        {
            Id = IdGenerator.NewId();
            Timestamp = DateTime.Now;
        }

        public string Id { get; set; }
        public string Ip { get; set; }
        public string Email { get; set; }
        public string AnimeId { get; set; }
        public EmailSubscriptionType Type { get; set; }
        public DateTime Timestamp { get; set; }

        [JsonIgnore]
        public Anime Anime { get; set; }
    }
}
