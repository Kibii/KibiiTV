﻿using KibiiTV.Data.Helpers;
using System;

namespace KibiiTV.Data.Broadcast
{
    public class Webhook
    {
        public Webhook() // Currently only supporting Discord
        {
            Id = IdGenerator.NewId();
            Timestamp = DateTime.Now;
        }

        public string Id { get; set; }
        public string Url { get; set; }
        public string OwnerId { get; set; }
        public bool IsActive { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
