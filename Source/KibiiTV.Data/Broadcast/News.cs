﻿using KibiiTV.Data.Helpers;
using System;

namespace KibiiTV.Data.Broadcast
{
    public class News
    {
        public News()
        {
            Id = IdGenerator.NewId();
            Timestamp = DateTime.Now;
        }

        public string Id { get; set; }
        public string Title { get; set; }
        public string Raw { get; set; }
        public string Html { get; set; }
        public DateTime Timestamp { get; set; }
        public string Author { get; set; }
        public NewsType Type { get; set; }
    }
}