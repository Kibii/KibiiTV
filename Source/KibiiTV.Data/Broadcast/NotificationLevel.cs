﻿namespace KibiiTV.Data.Broadcast
{
    public enum NotificationLevel
    {
        None = 0,
        Info = 1,
        Danger = 2,
        Warning = 3,
        Success = 4
    }
}