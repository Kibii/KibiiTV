﻿namespace KibiiTV.Data.Broadcast
{
    public enum NewsType
    {
        Blog = 0,
        Anime = 1,
        Site = 2,
        Reveiew = 3,
        Manga = 4,
        Other = 5
    }
}