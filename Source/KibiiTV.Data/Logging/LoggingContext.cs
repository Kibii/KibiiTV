﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace KibiiTV.Data.Logging
{
    public class LoggingContext : DbContext
    {
        public LoggingContext(DbContextOptions<LoggingContext> options) : base(options)
        { }

        public LoggingContext() : base()
        { }

        public DbSet<KibiiLog> Logs { get; set; }
        public DbSet<HttpLog> HttpLogs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder OptionsBuilder)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            var str = configuration.GetConnectionString("Logs");

            OptionsBuilder.UseMySql(str);
            base.OnConfiguring(OptionsBuilder);
        }
    }
}
