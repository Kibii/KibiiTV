﻿using System;

namespace KibiiTV.Data.Logging
{
    public class HttpLog
    {
        public int Id { get; set; }
        public string Ip { get; set; }
        public string Method { get; set; }
        public string ContentLength { get; set; }
        public string UserAgent { get; set; }
        public string Headers { get; set; }
        public string Epoch {  get { return Timestamp.ToUniversalTime().ToFileTimeUtc().ToString(); } }
        public DateTime Timestamp { get; set; }
        public string ContentType { get; set; }
        public string Scheme { get; set; }
        public string Host { get; set; }
        public string Path { get; set; }
        public string Query { get; set; }
        public string DisplayUrl { get; set; }
        public string RequestId { get; set; }
    }
}