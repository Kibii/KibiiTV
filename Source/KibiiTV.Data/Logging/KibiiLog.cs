﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace KibiiTV.Data.Logging
{
    public class KibiiLog
    {
        public KibiiLog()
        {
            LoggedOn = DateTime.Now;
         //   Id = GenerateId();
        }

        private static string GenerateId()
        {
            var random = new Random();
            return new string(Enumerable.Repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", 10)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        
        [Key]
        public int Id { get; set; }
        public int EventId { get; set; }
        public string EventName { get; set; }
        public string LogLevel { get; set; }
        public string Source { get; set; }
        public string Details { get; set; }
        public string Exception { get; set; }
        public string Timestamp { get { return LoggedOn.ToUniversalTime().ToFileTimeUtc().ToString(); } }
        public DateTime LoggedOn { get; set; }
    }
}
