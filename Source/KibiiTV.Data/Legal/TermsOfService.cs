﻿using KibiiTV.Data.Helpers;
using System;

namespace KibiiTV.Data.Legal
{
    public class TermsOfService
    {
        public TermsOfService()
        {
            Id = IdGenerator.NewId();
            Timestamp = DateTime.Now;
        }

        public string Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string Raw { get; set; }
        public string Html { get; set; }
    }
}