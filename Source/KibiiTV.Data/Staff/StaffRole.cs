﻿using KibiiTV.Data.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace KibiiTV.Data.Staff
{
    public class StaffRole
    {
        public StaffRole()
        {
            Id = IdGenerator.NewId();
            Timestamp = DateTime.Now;
            Applications = new HashSet<StaffApplication>();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Raw { get; set; }
        public string Html { get; set; }
        public DateTime Timestamp { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public virtual ICollection<StaffApplication> Applications { get; set; }
    }
}
