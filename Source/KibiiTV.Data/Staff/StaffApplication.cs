﻿using KibiiTV.Data.Affiliates;
using KibiiTV.Data.Helpers;
using KibiiTV.Data.Misc;
using System;

namespace KibiiTV.Data.Staff
{
    public class StaffApplication
    {
        public StaffApplication()
        {
            Id = IdGenerator.NewId();
            Timestamp = DateTime.Now;
        }

        public string Id { get; set; }
        public string Ip { get; set; }
        public string PhoneNumber { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string DiscordName { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Twitter { get; set; }
        public string LinkedIn { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string UserReason { get; set; } // Why do you want to be a part of Kibii?
        public string UserAcceptanceReason { get; set; } // Why should we accept you as a staff member of Kibii? Why do you feel that you are fit for this job?
        public string UserDetails { get; set; } // Tell us a little bit about yourself
        public string UserReferredFrom { get; set; } // How did you discover Kibii?
        public bool HasExperince { get; set; } // Do you have experince in this role or title?
        public string UserExperince { get; set; } // Tell us a little about your previous experinces, if any.
        public string GoogleResumeLink { get; set; }
        public string FavoriteAnime { get; set; }
        public GenderType Gender { get; set; }
        public ApprovalStatus Status { get; set; }
        public StaffRole Role { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
