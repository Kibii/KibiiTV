﻿using KibiiTV.Data.Helpers;
using KibiiTV.Data.Misc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace KibiiTV.Data.Staff
{
    public class StaffMember
    {
        public StaffMember()
        {
            Id = IdGenerator.NewId();
            Timestamp = DateTime.Now;
            SocialMedia = new HashSet<Social>();
        }

        public string Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string Name { get; set; }
        public string RealName { get; set; }
        public string DiscordId { get; set; }
        public string WithKibiiSince { get; set; }
        public string Motto { get; set; }
        public string PersonalNote { get; set; }
        public string OwnersNote { get; set; }
        public string WallpaperId { get; set; }
        public string AvatarId { get; set; }
        public string Title { get; set; }
        public virtual ICollection<Social> SocialMedia { get; set; }

        [JsonIgnore]
        public string StaffToken { get; set; }
    }
}