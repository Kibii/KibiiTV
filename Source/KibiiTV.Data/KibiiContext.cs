﻿using KibiiTV.Data.Affiliates;
using KibiiTV.Data.Broadcast;
using KibiiTV.Data.Legal;
using KibiiTV.Data.Media;
using KibiiTV.Data.Misc;
using KibiiTV.Data.Sessions;
using KibiiTV.Data.Staff;
using KibiiTV.Data.Support;
using Microsoft.EntityFrameworkCore;
using System;

namespace KibiiTV.Data
{
    public class KibiiContext : DbContext
    {
        private string connectionString;

        public KibiiContext(DbContextOptions<KibiiContext> options) : base(options) {}

        public KibiiContext(string connectionString) : base()
        {
            this.connectionString = connectionString;
        }

        public DbSet<News> News { get; set; }
        public DbSet<Blob> Blobs { get; set; }
        public DbSet<Anime> Anime { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Social> SocialMedia { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<StaffMember> Staff { get; set; }
        public DbSet<Episode> Episodes { get; set; }
        public DbSet<Webhook> Webhooks { get; set; }
        public DbSet<Partner> Partners { get; set; }
        public DbSet<Affiliate> Affiliates { get; set; }
        public DbSet<Blacklist> Blacklists { get; set; }
        public DbSet<StaffRole> StaffRoles { get; set; }
        public DbSet<KibiiSession> Sessions { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<PrivacyPolicy> PrivacyPolicies { get; set; }
        public DbSet<TermsOfService> TermsOfService { get; set; }
        public DbSet<StaffApplication> StaffApplications { get; set; }
        public DbSet<EmailSubscription> EmailSubscriptions { get; set; }
        public DbSet<CommunityApplication> Applications { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<KibiiSession>()
                .Property(d => d.StartedAt)
                .HasColumnType("datetime(0)");

            modelBuilder.Entity<Anime>()
                .HasKey(k => k.Id);

            modelBuilder.Entity<Genre>()
                .HasKey(k => k.Id);

            modelBuilder.Entity<AnimeGenre>()
                .HasKey(ag => new { ag.AnimeId, ag.GenreId });

            modelBuilder.Entity<AnimeGenre>()
                .HasOne(ag => ag.Anime)
                .WithMany(a => a.AnimeGenres)
                .HasForeignKey(ag => ag.AnimeId);

            modelBuilder.Entity<AnimeGenre>()
                .HasOne(ag => ag.Genre)
                .WithMany(g => g.Anime)
                .HasForeignKey(ag => ag.GenreId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!String.IsNullOrWhiteSpace(connectionString))
                optionsBuilder.UseMySql(connectionString);
            base.OnConfiguring(optionsBuilder);
        }
    }
}
