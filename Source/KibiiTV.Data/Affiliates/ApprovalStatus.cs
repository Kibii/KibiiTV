﻿namespace KibiiTV.Data.Affiliates
{
    public enum ApprovalStatus
    {
        Approved = 0,
        Rejected = 1,
        Ignored = 3,
        Pending = 4
    }
}