﻿using KibiiTV.Data.Helpers;
using KibiiTV.Data.Misc;
using System;
using System.Collections.Generic;

namespace KibiiTV.Data.Affiliates
{
    public class CommunityApplication
    {
        public CommunityApplication()
        {
            Id = IdGenerator.NewId();
            SocialMedia = new HashSet<Social>();
            Timestamp = DateTime.Now;
        }

        public string Id { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public DateTime Timestamp { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string UserReason { get; set; } // Why do you want to join the Kibii Affiliate/Partner program? Why should we accept you?
        public string UserDetails { get; set; } // Tell us a little bit about yourself
        public string UserReferredFrom { get; set; } // How did you find about Kibii? How did you find out about Kibii Affiliate or Kibii Partner?
        public string Ip { get; set; }
        public string FavoriteAnime { get; set; } // What is your favorite anime, and why?
        public GenderType Gender { get; set; }
        public ApprovalStatus Status { get; set; }
        public ApplicationType Type { get; set; }
        public virtual ICollection<Social> SocialMedia { get; set; }
    }
}