﻿namespace KibiiTV.Data.Affiliates
{
    public enum ApplicationType
    {
        Affiliate = 0,
        Partner = 1
    }
}