﻿using KibiiTV.Data.Helpers;
using KibiiTV.Data.Misc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace KibiiTV.Data.Affiliates
{
    public class Partner
    {
        public Partner()
        {
            Id = IdGenerator.NewId();
            Timestamp = DateTime.Now;
            SocialMedia = new HashSet<Social>();
        }

        public string Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string Name { get; set; }
        public string Motto { get; set; }
        public string Description { get; set; }
        public string WallpaperId { get; set; }
        public string AvatarId { get; set; }
        public virtual ICollection<Social> SocialMedia { get; set; }

        [JsonIgnore]
        public string PartnerToken { get; set; }
    }
}