﻿using KibiiTV.Data.Helpers;
using System;

namespace KibiiTV.Data.Misc
{
    public class Blacklist
    {
        public Blacklist()
        {
            Id = IdGenerator.NewId();
            Timestamp = DateTime.Now;
        }

        public string Id { get; set; }
        public string Reason { get; set; }
        public string Author { get; set; }
        public string AffectedId { get; set; }
        public BlacklistModel Model { get; set; }
        public BlacklistType Type { get; set; }
        public DateTime Timestamp { get; set; }
    }
}