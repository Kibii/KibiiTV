﻿namespace KibiiTV.Data.Misc
{
    public enum BlacklistType
    {
        NSFW = 0,
        NSFL = 1,
        BrokenEpisode = 2,
        BrokenAnime = 3,
        BrokenEmbed = 4,
    }
}