﻿using KibiiTV.Data.Affiliates;
using KibiiTV.Data.Helpers;
using KibiiTV.Data.Staff;
using Newtonsoft.Json;
using System;

namespace KibiiTV.Data.Misc
{
    public class Social
    {
        public Social()
        {
            Id = IdGenerator.NewId();
            Timestamp = DateTime.Now;
        }

        public string Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string Data { get; set; }
        public SocialType Type { get; set; }

        [JsonIgnore]
        public Affiliate Affiliate { get; set; }

        [JsonIgnore]
        public Partner Partner { get; set; }

        [JsonIgnore]
        public StaffMember Staff { get; set; }

        [JsonIgnore]
        public CommunityApplication CommunityApplication { get; set; }
    }
}