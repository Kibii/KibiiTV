﻿namespace KibiiTV.Data.Misc
{
    public enum BlacklistModel
    {
        Anime = 0,
        Episode = 1,
        Embed = 2
    }
}