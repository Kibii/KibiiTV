﻿namespace KibiiTV.Data.Misc
{
    public enum GenderType
    {
        Male = 0,
        Female = 1,
        Other = 2
    }
}