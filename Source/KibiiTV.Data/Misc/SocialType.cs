﻿namespace KibiiTV.Data.Misc
{
    public enum SocialType
    {
        Twitter = 0,
        Discord = 1,
        DiscordInvite = 2,
        Facebook = 3,
        Instagram = 4,
        Twitch = 5,
        YouTube = 6,
        Steam = 7,
        Reddit = 8,
        Email = 9,
        Website = 10,
        Other = 11
    }
}