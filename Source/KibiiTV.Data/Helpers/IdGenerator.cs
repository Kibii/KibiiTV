﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace KibiiTV.Data.Helpers
{
    public class Id
    {
        public const int Length = 12;

        public static string NewId() => 
            IdGenerator.NewId();
    }

    public class IdGenerator
    {
        public static string NewId() =>
            Guid.NewGuid().ToString().Replace("-", "").Substring(0, Id.Length).ToUpper();
    }

    public class OldIdGenerator
    {
        // This is the base date aka when bin met ゼロツー for the first time.
        private readonly DateTime _baseDate = new DateTime(2018, 5, 17, 2, 30, 20);
        private readonly IDictionary<long, IList<long>> _cache = new Dictionary<long, IList<long>>();

        public static string NewId() => new OldIdGenerator().Generate();

        public string Generate()
        {
            var now = DateTime.Now.ToString("HHmmssfff");
            var daysDiff = ((DateTime.Now.Add(DateTime.Now.Subtract(_baseDate)))).ToString("HHmmss");
            //(DateTime.Today - _baseDate).Days;
            var current = long.Parse(string.Format("{0}{1}", daysDiff, now));
            return OldIdGeneratorHelper.NewId(_cache, current);
        }

        static class OldIdGeneratorHelper
        {
            public static string NewId(IDictionary<long, IList<long>> cache, long current)
            {
                if (cache.Any() && cache.Keys.Max() < current)
                {
                    cache.Clear();
                }

                if (!cache.Any())
                {
                    cache.Add(current, new List<long>());
                }

                string secondPart;
                if (cache[current].Any())
                {
                    var maxValue = cache[current].Max();
                    cache[current].Add(maxValue + 1);
                    secondPart = maxValue.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    cache[current].Add(0);
                    secondPart = string.Empty;
                }

                var nextValueFormatted = string.Format("{0}{1}", current, secondPart);
                return UInt64.Parse(nextValueFormatted).ToString("X8");
            }
        }
    }
}
