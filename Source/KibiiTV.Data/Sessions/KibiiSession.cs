﻿using KibiiTV.Data.Helpers;
using System;

namespace KibiiTV.Data.Sessions
{
    public class KibiiSession
    {
        public KibiiSession()
        {
            Token = Guid.NewGuid().ToString();
            Id = IdGenerator.NewId();
        }

        public string Id { get; set; }
        public string Ip { get; set; }
        public string Token { get; set; }
        public DateTime StartedAt { get; set; }
        public bool Valid { get; set; }
        public SessionType Type { get; set; }
        public bool IsInState { get { return DateTime.Now < StartedAt.AddMinutes(10); } }
        public bool ViewValidated { get { return DateTime.Now > StartedAt.AddSeconds(70); } }
        public bool IsApiValid {  get { return DateTime.Now < StartedAt.AddMinutes(30); } }
        public bool IsDeletable { get { return DateTime.Now > StartedAt.AddHours(4); } }
        public string AnimeId { get; set; } //TODO: RENAME THESE
        public string EpisodeId { get; set; }
    }
}
