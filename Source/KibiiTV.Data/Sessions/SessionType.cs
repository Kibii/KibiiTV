﻿namespace KibiiTV.Data.Sessions
{
    public enum SessionType
    {
        View = 0,
        API = 1,
        Embed = 2,
    }
}