﻿namespace KibiiTV.Data.Support
{
    public enum ReportType
    {
        Episode = 0,
        Anime = 1,
        Embed = 2,
        Bug = 3,
        Other = 4,
    }
}