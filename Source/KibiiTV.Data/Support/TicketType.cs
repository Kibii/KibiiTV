﻿namespace KibiiTV.Data.Support
{
    public enum TicketType
    {
        General = 0,
        DMCA = 1,
        Terms = 2,
        Privacy = 3,
        Suggestion = 4,
        Sponsor = 5,
        Partnership = 6,
        Takedown = 7,
        Praise = 8,
        Avaliablity = 9,
        NewContentSuggestion = 10,
        Other = 11
    }
}