﻿using KibiiTV.Data.Helpers;
using System;

namespace KibiiTV.Data.Support
{
    public class Ticket
    {
        public Ticket()
        {
            Id = IdGenerator.NewId();
            Timestamp = DateTime.Now;
        }

        public string Id { get; set; }
        public string Ip { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public DateTime Timestamp { get; set; }
        public TicketType Type { get; set; }
    }
}