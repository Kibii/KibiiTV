﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace KibiiTV.Data.Migrations
{
    public partial class InitialKibiiProd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Affiliates",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AffiliateToken = table.Column<string>(nullable: true),
                    AvatarId = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Motto = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    WallpaperId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Affiliates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Anime",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AgeRating = table.Column<string>(nullable: true),
                    CoverId = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    EnglishTitle = table.Column<string>(nullable: true),
                    FinishedAiringDate = table.Column<DateTime>(nullable: true),
                    JapaneseTitle = table.Column<string>(nullable: true),
                    MasterId = table.Column<int>(nullable: false),
                    Score = table.Column<double>(nullable: false),
                    StartedAiringDate = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Views = table.Column<int>(nullable: false),
                    WallpaperId = table.Column<string>(nullable: true),
                    YoutubeTrailerId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Anime", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Applications",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Country = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    FavoriteAnime = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    Ip = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    UserDetails = table.Column<string>(nullable: true),
                    UserReason = table.Column<string>(nullable: true),
                    UserReferredFrom = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Applications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Blacklists",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AffectedId = table.Column<string>(nullable: true),
                    Author = table.Column<string>(nullable: true),
                    Model = table.Column<int>(nullable: false),
                    Reason = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blacklists", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Genres",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Genres", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "News",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Author = table.Column<string>(nullable: true),
                    Html = table.Column<string>(nullable: true),
                    Raw = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_News", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Author = table.Column<string>(nullable: true),
                    ExpiresAt = table.Column<DateTime>(nullable: false),
                    ForceRefresh = table.Column<bool>(nullable: false),
                    HasIcon = table.Column<bool>(nullable: false),
                    Html = table.Column<string>(nullable: true),
                    IsDismissable = table.Column<bool>(nullable: false),
                    NotificationLevel = table.Column<int>(nullable: false),
                    OverrideUserSettings = table.Column<bool>(nullable: false),
                    Raw = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    UpdateCache = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Partners",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AvatarId = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Motto = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    PartnerToken = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    WallpaperId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Partners", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PrivacyPolicies",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Html = table.Column<string>(nullable: true),
                    Raw = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrivacyPolicies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Reports",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Ip = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ReportedLink = table.Column<string>(nullable: true),
                    Subject = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reports", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sessions",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AnimeId = table.Column<string>(nullable: true),
                    EpisodeId = table.Column<string>(nullable: true),
                    Ip = table.Column<string>(nullable: true),
                    StartedAt = table.Column<DateTime>(type: "datetime(0)", nullable: false),
                    Token = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Valid = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sessions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Staff",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AvatarId = table.Column<string>(nullable: true),
                    DiscordId = table.Column<string>(nullable: true),
                    Motto = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    OwnersNote = table.Column<string>(nullable: true),
                    PersonalNote = table.Column<string>(nullable: true),
                    RealName = table.Column<string>(nullable: true),
                    StaffToken = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    WallpaperId = table.Column<string>(nullable: true),
                    WithKibiiSince = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Staff", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StaffRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Html = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Raw = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StaffRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TermsOfService",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Html = table.Column<string>(nullable: true),
                    Raw = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TermsOfService", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tickets",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Ip = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Subject = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tickets", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Webhooks",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    OwnerId = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    Url = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Webhooks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AnimePv",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AnimeId = table.Column<string>(nullable: true),
                    Data = table.Column<string>(nullable: true),
                    IsYoutube = table.Column<bool>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnimePv", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AnimePv_Anime_AnimeId",
                        column: x => x.AnimeId,
                        principalTable: "Anime",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmailSubscriptions",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AnimeId = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Ip = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailSubscriptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmailSubscriptions_Anime_AnimeId",
                        column: x => x.AnimeId,
                        principalTable: "Anime",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Episodes",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Aired = table.Column<DateTime>(nullable: false),
                    AnimeId = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Duration = table.Column<int>(nullable: false),
                    EpisodeNumber = table.Column<int>(nullable: false),
                    MasterId = table.Column<int>(nullable: false),
                    ThumbnailId = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Views = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Episodes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Episodes_Anime_AnimeId",
                        column: x => x.AnimeId,
                        principalTable: "Anime",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AnimeGenre",
                columns: table => new
                {
                    AnimeId = table.Column<string>(nullable: false),
                    GenreId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnimeGenre", x => new { x.AnimeId, x.GenreId });
                    table.ForeignKey(
                        name: "FK_AnimeGenre_Anime_AnimeId",
                        column: x => x.AnimeId,
                        principalTable: "Anime",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AnimeGenre_Genres_GenreId",
                        column: x => x.GenreId,
                        principalTable: "Genres",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SocialMedia",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AffiliateId = table.Column<string>(nullable: true),
                    CommunityApplicationId = table.Column<string>(nullable: true),
                    Data = table.Column<string>(nullable: true),
                    PartnerId = table.Column<string>(nullable: true),
                    StaffId = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SocialMedia", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SocialMedia_Affiliates_AffiliateId",
                        column: x => x.AffiliateId,
                        principalTable: "Affiliates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SocialMedia_Applications_CommunityApplicationId",
                        column: x => x.CommunityApplicationId,
                        principalTable: "Applications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SocialMedia_Partners_PartnerId",
                        column: x => x.PartnerId,
                        principalTable: "Partners",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SocialMedia_Staff_StaffId",
                        column: x => x.StaffId,
                        principalTable: "Staff",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StaffApplications",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    DiscordName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    FavoriteAnime = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    GoogleResumeLink = table.Column<string>(nullable: true),
                    HasExperince = table.Column<bool>(nullable: false),
                    Ip = table.Column<string>(nullable: true),
                    LinkedIn = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    RoleId = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    Twitter = table.Column<string>(nullable: true),
                    UserAcceptanceReason = table.Column<string>(nullable: true),
                    UserDetails = table.Column<string>(nullable: true),
                    UserExperince = table.Column<string>(nullable: true),
                    UserReason = table.Column<string>(nullable: true),
                    UserReferredFrom = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true),
                    Website = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StaffApplications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StaffApplications_StaffRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "StaffRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Blobs",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    BlobId = table.Column<string>(nullable: true),
                    BlobPrefix = table.Column<string>(nullable: true),
                    BlobSuffix = table.Column<string>(nullable: true),
                    Dubbed = table.Column<bool>(nullable: false),
                    EpisodeId = table.Column<string>(nullable: true),
                    MasterId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Quality = table.Column<int>(nullable: false),
                    Timestamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blobs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Blobs_Episodes_EpisodeId",
                        column: x => x.EpisodeId,
                        principalTable: "Episodes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AnimeGenre_GenreId",
                table: "AnimeGenre",
                column: "GenreId");

            migrationBuilder.CreateIndex(
                name: "IX_AnimePv_AnimeId",
                table: "AnimePv",
                column: "AnimeId");

            migrationBuilder.CreateIndex(
                name: "IX_Blobs_EpisodeId",
                table: "Blobs",
                column: "EpisodeId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailSubscriptions_AnimeId",
                table: "EmailSubscriptions",
                column: "AnimeId");

            migrationBuilder.CreateIndex(
                name: "IX_Episodes_AnimeId",
                table: "Episodes",
                column: "AnimeId");

            migrationBuilder.CreateIndex(
                name: "IX_SocialMedia_AffiliateId",
                table: "SocialMedia",
                column: "AffiliateId");

            migrationBuilder.CreateIndex(
                name: "IX_SocialMedia_CommunityApplicationId",
                table: "SocialMedia",
                column: "CommunityApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_SocialMedia_PartnerId",
                table: "SocialMedia",
                column: "PartnerId");

            migrationBuilder.CreateIndex(
                name: "IX_SocialMedia_StaffId",
                table: "SocialMedia",
                column: "StaffId");

            migrationBuilder.CreateIndex(
                name: "IX_StaffApplications_RoleId",
                table: "StaffApplications",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AnimeGenre");

            migrationBuilder.DropTable(
                name: "AnimePv");

            migrationBuilder.DropTable(
                name: "Blacklists");

            migrationBuilder.DropTable(
                name: "Blobs");

            migrationBuilder.DropTable(
                name: "EmailSubscriptions");

            migrationBuilder.DropTable(
                name: "News");

            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.DropTable(
                name: "PrivacyPolicies");

            migrationBuilder.DropTable(
                name: "Reports");

            migrationBuilder.DropTable(
                name: "Sessions");

            migrationBuilder.DropTable(
                name: "SocialMedia");

            migrationBuilder.DropTable(
                name: "StaffApplications");

            migrationBuilder.DropTable(
                name: "TermsOfService");

            migrationBuilder.DropTable(
                name: "Tickets");

            migrationBuilder.DropTable(
                name: "Webhooks");

            migrationBuilder.DropTable(
                name: "Genres");

            migrationBuilder.DropTable(
                name: "Episodes");

            migrationBuilder.DropTable(
                name: "Affiliates");

            migrationBuilder.DropTable(
                name: "Applications");

            migrationBuilder.DropTable(
                name: "Partners");

            migrationBuilder.DropTable(
                name: "Staff");

            migrationBuilder.DropTable(
                name: "StaffRoles");

            migrationBuilder.DropTable(
                name: "Anime");
        }
    }
}
