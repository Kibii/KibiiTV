﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace KibiiTV.Data.Migrations.Logging
{
    public partial class InitialKibiiProd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HttpLogs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ContentLength = table.Column<string>(nullable: true),
                    ContentType = table.Column<string>(nullable: true),
                    DisplayUrl = table.Column<string>(nullable: true),
                    Headers = table.Column<string>(nullable: true),
                    Host = table.Column<string>(nullable: true),
                    Ip = table.Column<string>(nullable: true),
                    Method = table.Column<string>(nullable: true),
                    Path = table.Column<string>(nullable: true),
                    Query = table.Column<string>(nullable: true),
                    RequestId = table.Column<string>(nullable: true),
                    Scheme = table.Column<string>(nullable: true),
                    Timestamp = table.Column<DateTime>(nullable: false),
                    UserAgent = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HttpLogs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Details = table.Column<string>(nullable: true),
                    EventId = table.Column<int>(nullable: false),
                    EventName = table.Column<string>(nullable: true),
                    Exception = table.Column<string>(nullable: true),
                    LogLevel = table.Column<string>(nullable: true),
                    LoggedOn = table.Column<DateTime>(nullable: false),
                    Source = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logs", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HttpLogs");

            migrationBuilder.DropTable(
                name: "Logs");
        }
    }
}
