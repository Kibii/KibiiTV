﻿using KibiiTV.Data.Generator.Services.Cdn;
using KibiiTV.Data.Generator.Services.Data;
using KibiiTV.Data.Generator.Services.Discord;
using KibiiTV.Data.Generator.Services.Master;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace KibiiTV.Data.Generator
{
    public class Program
    {
        public static async Task<int> Main(string[] args)
        {
            Global.GetConfiguration(args);

            IServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection = ConfigureServices(serviceCollection);

            IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
            ILogger<Program> logger = serviceProvider.GetService<ILogger<Program>>();
            DataApp Generator;

            try
            {
                Generator = serviceProvider.GetService<DataApp>();
                Generator.SetCommandLineArguments(args);
                bool runModeSuccess = Generator.SetRunMode();
                if (!runModeSuccess)
                    return 1;

                await Task.Delay(5000);
                await Generator.BeginAsync();
            }
            catch (Exception)
            {
                // logger.LogCritical(exception, "An exception was thrown by the ServiceProvider");
                // This is nothing for now.

                return 1;
            }

            logger.LogInformation("The DataApp has exited.");

            if (Generator.GetExitMode() == "keep-alive")
            {
                await Task.Delay(-1);
                Console.ReadLine();
            }

            return 0;
        }

        public static IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Global.Config);
            services.AddOptions();

            ILoggerFactory loggerFactory = new LoggerFactory()
                .AddConsole(Global.Config.GetSection("Logging"))
                .AddDebug();

            services.AddSingleton(loggerFactory);
            services.AddLogging();

            services.AddDbContext<KibiiContext>(o => o.UseMySql(Global.DbConnectionString));

            services.AddTransient<IMasterHttpService, MasterHttpService>();
            services.AddSingleton<IMasterMirrorService, MasterMirrorService>();
            services.AddSingleton<IMasterCdnService, MasterCdnService>();
            services.AddSingleton<IMasterApiService, MasterApiService>();

            services.AddSingleton<IDiscordService, DiscordService>();
            services.AddSingleton<IS3Service, S3Service>();
            services.AddSingleton<ICdnService, CdnService>();

            services.AddSingleton<IDataService, DataService>();
            services.AddSingleton<IGenreService, GenreService>();
            services.AddSingleton<IConverterService, ConverterService>();

            services.AddTransient<DataApp>();

            return services;
        }
    }
}
