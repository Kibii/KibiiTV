﻿using KibiiTV.Data.Generator.Models.Http;
using System.Net.Http;
using System.Threading.Tasks;

namespace KibiiTV.Data.Generator.Services.Master
{
    public interface IMasterHttpService
    {
        void ConfigureData(bool includeData);
        void ConfigureResponse(bool includeResponseData);
        Task<MasterHttpResponse> ExecuteRequestAsync(HttpMethod requestMethod, string requestPath);
        Task<MasterHttpResponse> ExecuteRequestAsync(HttpMethod requestMethod, string requestPath, string requestBody);
        Task<MasterHttpResponse> ExecuteRequestAsync(HttpMethod requestMethod, string requestPath, string requestBody, string contentType);
    }
}
