﻿using KibiiTV.Data.Generator.Models.Remote;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace KibiiTV.Data.Generator.Services.Master
{
    public interface IMasterApiService
    {
        Task<IEnumerable<MAnime>> GetAllAnimeAsync();
        Task<IEnumerable<Mirror>> GetEpisodeMirrorsAsync(MAnime anime, MEpisode episode);
        Task<AnimeInfo> GetAnimeInfoAsync(MAnime anime);
        Task<AnimeInfo> GetAnimeByIdAsync(int animeId);
        List<MAnime> GetAnimeRangeByIdsAsync(List<int> list, IEnumerable<MAnime> allAnimeList);
    }
}
