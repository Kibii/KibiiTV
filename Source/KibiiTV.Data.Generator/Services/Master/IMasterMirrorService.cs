﻿using KibiiTV.Data.Generator.Models.Remote;
using System.Collections.Generic;

namespace KibiiTV.Data.Generator.Services.Master
{
    public interface IMasterMirrorService
    {
        string[] ExtractMirrors(string data);
        string[] ExtractSpecialMirrors(string data);
        IEnumerable<Mirror> ParseSpecialMirrors(string mData);
        IEnumerable<Mirror> ParseNormalMirrors(string mData);
    }
}