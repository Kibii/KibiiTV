﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KibiiTV.Data.Generator.Models.Http;
using KibiiTV.Data.Generator.Models.Remote;
using Microsoft.Extensions.Logging;

namespace KibiiTV.Data.Generator.Services.Master
{
    public class MasterApiService : IMasterApiService
    {
        private IMasterHttpService _httpService { get; set; }
        private IMasterMirrorService _mirrorService { get; set; }
        private ILogger _logger { get; set; }

        public MasterApiService(ILogger<MasterApiService> Logger, IMasterHttpService masterHttpService, IMasterMirrorService masterMirrorService)
        {
            _logger = Logger;
            _httpService = masterHttpService;
            _mirrorService = masterMirrorService;
        }

        public async Task<IEnumerable<MAnime>> GetAllAnimeAsync()
        {
            bool Completed = false;
            int CurrentPage = 1;
            List<MAnime> Anime = new List<MAnime>();

            while (!Completed)
            {
                MasterHttpResponse masterHttpResponse = await _httpService.ExecuteRequestAsync(Global.GET, Global.GetPagedSearchUrl(CurrentPage));
                MasterResponse masterResponse;

                try
                { 
                    MasterInitialResponse masterInitialResponse = masterHttpResponse.AsObject<MasterInitialResponse>();
                    masterResponse = new MasterResponse()
                    {
                        To = masterInitialResponse.To,
                        From = masterInitialResponse.From,
                        Total = masterInitialResponse.Total,
                        PerPage = masterInitialResponse.PerPage,
                        LastPage = masterInitialResponse.LastPage,
                        CurrentPage = masterInitialResponse.CurrentPage,
                        PrevPageUrl = masterInitialResponse.PrevPageUrl,
                        NextPageUrl = masterInitialResponse.NextPageUrl,
                        Anime = masterInitialResponse.Anime.Select(c => c.Value).ToList(),
                    };
                }
                catch
                {
                    masterResponse = masterHttpResponse.AsObject<MasterResponse>();
                }

                Anime.AddRange(masterResponse.Anime);
                _logger.LogDebug($"{masterResponse.Anime.Count} series loaded from {CurrentPage} request(s)");

                CurrentPage = masterResponse.CurrentPage + 1; // Increment the current page.
                Completed = CurrentPage > masterResponse.LastPage ? true : false;
                // Logic Note - This value is pre-incremented so we just check if it's greater.
            }

            _logger.LogInformation($"{Anime.Count} series loaded from {CurrentPage} total request(s)");
            return Anime;
        }

        public IEnumerable<MEpisode> GetAnimeEpisodesAsync(MAnime anime) =>
            anime?.Info?.Episodes;

        public Task<AnimeInfo> GetAnimeInfoAsync(MAnime anime) =>
            GetAnimeByIdAsync(anime.Id);

        public async Task<IEnumerable<Mirror>> GetEpisodeMirrorsAsync(MAnime anime, MEpisode episode)
        {
            string URL = Global.EPISODE_URL
                .Replace("[ANIME_SLUG]", anime.Slug)
                .Replace("[EPISODE_NUMBER]", episode.Info.Episode);

            MasterHttpResponse Response = await _httpService.ExecuteRequestAsync(Global.GET, URL);

            string[] MirrorData = _mirrorService.ExtractMirrors(Response.Data);
            string[] SpecialMirrorData = _mirrorService.ExtractSpecialMirrors(Response.Data);

            List<Mirror> Mirrors = new List<Mirror>();

            foreach (string mData in MirrorData)
                Mirrors.AddRange(_mirrorService.ParseNormalMirrors(mData));

            foreach (string mData in SpecialMirrorData)
                Mirrors.AddRange(_mirrorService.ParseSpecialMirrors(mData));

            _logger.LogDebug($"{Mirrors.Count} mirrors loaded for episode " + episode.Info.Id);
            return Mirrors;
        }

        public async Task<AnimeInfo> GetAnimeByIdAsync(int animeId)
        {
            string URL = Global.API_ANIME_DETAILS_URL
                .Replace("[ANIME_ID]", animeId.ToString());

            _logger.LogDebug($"Grabbing information for series {animeId}.");

            bool Completed = false;
            int Tries = 0;

            while (!Completed && Tries < 10)
            {
                Tries++;

                try
                {
                    return (await _httpService.ExecuteRequestAsync(Global.GET, URL))
                                  .AsObject<AnimeInfo>();
                }
                catch (Exception exception)
                {
                    _logger.LogCritical(exception, "Exception thrown in GetAnimeInfoAsync");
                    await Task.Delay(1500);
                }
            }

            throw new Exception("Unable to grab the anime information!");
        }

        public List<MAnime> GetAnimeRangeByIdsAsync(List<int> list, IEnumerable<MAnime> allAnimeList)
        {
            return allAnimeList.Where(c => list.Contains(c.Id)).ToList();
        }
    }
}
