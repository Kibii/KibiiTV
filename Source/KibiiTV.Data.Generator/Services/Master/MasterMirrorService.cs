﻿using HtmlAgilityPack;
using KibiiTV.Data.Generator.Models.Http;
using KibiiTV.Data.Generator.Models.Remote;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KibiiTV.Data.Generator.Services.Master
{
    public class MasterMirrorService : IMasterMirrorService
    {
        public string[] ExtractMirrors(string data)
        {
            if (data == null)
                data = "";

            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(data);

            /* string[] rawMirrors = document?.DocumentNode
                 ?.SelectNodes("//body/script")
                 ?.Descendants()
                 ?.Where(c => c?.InnerText?.Contains("var args = ") ?? false)
                 ?.Where(c => c?.InnerText?.Contains("mirrors: [") ?? false)
                 ?.Select(c => c?.InnerText)
                 ?.ToArray(); */

            string[] rawMirrors = document?.DocumentNode?.Descendants("video-mirrors")
                ?.Select(c => c?.Attributes.Where(a => a.OriginalName == ":mirrors"))
                ?.Select(m => m.Select(c => c.Value))
                ?.SelectMany(i => i)
                ?.ToArray();

            return rawMirrors != null ? rawMirrors : new string[0];
        }

        public IEnumerable<Mirror> ParseNormalMirrors(string mData)
        {
            /* string dataToParse = mData
                  ?.Replace("var args = ", string.Empty)
                  ?.Replace("anime: {", "\"anime\": {")
                  ?.Replace("mirrors: [", "\"mirrors\": [")
                  ?.Replace("auto_update: [", "\"auto_update\": ["); */

            return new MasterHttpResponse { Data = mData }
                 ?.AsObject<List<Mirror>>();
        }

        public string[] ExtractSpecialMirrors(string data)
        {
            if (data == null)
                data = "";

            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(data);

            string[] rawMirrors = document.DocumentNode
                ?.SelectNodes("//body/script")
                ?.Descendants()
                ?.Where(c => c?.InnerText?.Contains("var videos = ") ?? false)
                ?.Where(c => c?.InnerText?.Contains("[{\"label\":") ?? false)
                ?.Select(c => c?.InnerText)
                ?.ToArray();

            return rawMirrors != null ? rawMirrors : new string[0];
        }

        public IEnumerable<Mirror> ParseSpecialMirrors(string mData)
        {
            try
            {
                string dataToParse = mData
                     ?.Replace("var videos = ", string.Empty);
                dataToParse = dataToParse?.Trim()?.Split('\n')[0];

                List<SpecialVideoMirror> specialMirrors = new MasterHttpResponse { Data = dataToParse }
                     ?.AsObject<List<SpecialVideoMirror>>();

                List<Mirror> mirrors = new List<Mirror>();

                foreach (SpecialVideoMirror specialMirror in specialMirrors)
                    mirrors.Add(new Mirror
                    {
                        EmbedId = specialMirror.Source,
                        Quality = Convert.ToInt32(specialMirror.Resolution),
                        Type = 1,
                        Host = new Host { Name = "Aika" }
                    });

                return mirrors;
            }
            catch
            {
                return null;
            }
        }
    }
}
