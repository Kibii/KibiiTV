﻿using KibiiTV.Data.Generator.Models.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace KibiiTV.Data.Generator.Services.Master
{
    public class MasterHttpService : IMasterHttpService
    {
        private readonly HttpClient _client;
        private readonly ILogger _logger;
        private bool _includeResponseString = true;
        private bool _includeResponseClass = true;

        public MasterHttpService(ILogger<MasterHttpService> logger)
        {
            _logger = logger;

            var handler = new HttpClientHandler()
            {
                AllowAutoRedirect = true,
            };

            _client = new HttpClient(handler)
            {
                Timeout = TimeSpan.FromSeconds(Global.API_MAX_TIMEOUT)
            };

            _client.DefaultRequestHeaders.UserAgent.ParseAdd(Global.API_USER_AGENT);
            _client.DefaultRequestHeaders.Add("Accept", "application/json,text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        }

        public Task<MasterHttpResponse> ExecuteRequestAsync(HttpMethod requestMethod, string requestPath, string requestBody) =>
            ExecuteRequestAsync(requestMethod, requestPath, requestBody, "application/json");

        public Task<MasterHttpResponse> ExecuteRequestAsync(HttpMethod requestMethod, string requestPath) =>
            ExecuteRequestAsync(requestMethod, requestPath, null, null);

        public async Task<MasterHttpResponse> ExecuteRequestAsync(HttpMethod requestMethod, string requestPath, string requestBody, string contentType)
        {
            HttpResponseMessage response;
            bool isRateLimited = false;
            int requestTries = default(int);

            Stopwatch stopwatch = new Stopwatch();
            _logger.LogDebug($"Request starting '{requestPath}'");
            stopwatch.Start();

            do
            {
                using (HttpRequestMessage httpReq = new HttpRequestMessage(requestMethod, requestPath))
                {
                    requestTries++;

                    if (requestBody != null)
                        httpReq.Content = new StringContent(requestBody, Encoding.UTF8, contentType);

                    response = await _client.SendAsync(httpReq);
                    int statusCode = (int)response.StatusCode;

                    if (statusCode == 429)
                    {
                        isRateLimited = true;
                        await ParseRateLimit(response);
                        continue;
                    }

                    if(response.Content.Headers.ContentType.ToString().Contains("text/html"))
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        if (content.ToLower().Contains("moved permanently"))
                            continue;
                    }

                    if (statusCode == 301 || statusCode == 302)
                        continue;

                    if(statusCode == 502)
                    {
                        _logger.LogError("HTTP Request reported with a 502 bad gateway error!");
                        await Task.Delay(2500);
                    }

                    isRateLimited = false;

                    _logger.LogDebug($"Request finished in {stopwatch.ElapsedMilliseconds}ms");
                    stopwatch.Stop();

                    try
                    {
                        return new MasterHttpResponse()
                        {
                            Response = _includeResponseClass ? response : null,
                            StatusCode = statusCode,
                            Data = _includeResponseString ? await response.Content.ReadAsStringAsync() : null,
                            Headers = response.Headers.ToDictionary(c => c.Key, o => o.Value.First())
                        };
                    }
                    catch
                    {
                        continue;
                    }
                }
            }
            while (isRateLimited && (requestTries < Global.API_MAX_TRIES));

            _logger.LogCritical(requestMethod.Method + " request failed with " + requestTries + " tries");
            return new MasterHttpResponse()
            {
                Response = _includeResponseClass ? response : null, // Return the previous response
                StatusCode = 506, // Internal Client Error?
                Data = _includeResponseString ? await response?.Content?.ReadAsStringAsync() : null,
                Headers = response?.Headers?.ToDictionary(c => c.Key, o => o.Value?.First())
            };
        }

        private Task ParseRateLimit(HttpResponseMessage Response)
        {
            TimeSpan RetryAfter = Response.Headers.RetryAfter.Delta.Value;
            _logger.LogWarning("Ratelimit hit! Now waiting " + RetryAfter.TotalSeconds + " seconds before attempting to retry the request");

            return Task.Delay(RetryAfter, default);
        }

        public void ConfigureResponse(bool includeResponseData) =>
            _includeResponseClass = includeResponseData;

        public void ConfigureData(bool includeData) =>
            _includeResponseString = includeData;
    }
}
