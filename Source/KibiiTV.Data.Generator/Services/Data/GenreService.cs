﻿using KibiiTV.Data.Generator.Models.Remote;
using KibiiTV.Data.Media;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace KibiiTV.Data.Generator.Services.Data
{
    public class GenreService : IGenreService
    {
        private IDataService dataService;
        private ILogger logger;

        public GenreService(IDataService dataService, ILogger<GenreService> logger)
        {
            this.logger = logger;
            this.dataService = dataService;
        }

        public async Task<Genre> CreateGenreAsync(string Name)
        {
            Genre genre = new Genre() { Name = Name };
            await dataService.AddGenreAsync(genre);
            await dataService.SaveChangesAsync();

            return genre;
        }

        public async Task<Anime> FillGenresAsync(Anime anime, MAnime mAnime)
        {
            foreach(MGenre mGenre in mAnime.Genres)
            {
                Genre genre = await GetOrCreateAsync(mGenre.Name);

                AnimeGenre joinGenre = new AnimeGenre()
                {
                    Anime = anime,
                    Genre = genre
                };

                anime.AnimeGenres.Add(joinGenre);
            }

            return anime;
        }

        public async Task<Genre> GetOrCreateAsync(string Name)
        {
            Genre genre = await dataService.GetGenreAsync(Name);

            if (genre != null)
                return genre;

            return await CreateGenreAsync(Name);
        }

        public Task<Genre> GetGenreAsync(string Name) =>
            dataService.GetGenreAsync(Name);

        public Task<Genre> GetGenreByIdAsync(string Id) =>
            dataService.GetGenreByIdAsync(Id);

        public Genre GetOrCreate(string Name) =>
            GetOrCreateAsync(Name).GetAwaiter().GetResult();

        public Genre GetGenreById(string Id) =>
            GetGenreByIdAsync(Id).GetAwaiter().GetResult();

        public Genre GetGenre(string Name) =>
            GetGenreAsync(Name).GetAwaiter().GetResult();

        public Genre CreateGenre(string Name) =>
            CreateGenreAsync(Name).GetAwaiter().GetResult();

        public Anime FillGenres(Anime anime, MAnime mAnime) =>
            FillGenresAsync(anime, mAnime).GetAwaiter().GetResult();
    }
}
