﻿using KibiiTV.Data.Generator.Models.Remote;
using KibiiTV.Data.Media;

namespace KibiiTV.Data.Generator.Services.Data
{
    public interface IConverterService
    {
        Anime CreateAnime(MAnime anime);
        Episode CreateEpisode(MEpisode episode, MAnime anime);
        Blob CreateBlob(Mirror mirror);
    }
}
