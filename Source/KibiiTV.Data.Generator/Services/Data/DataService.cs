﻿using KibiiTV.Data.Media;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Data.Generator.Services.Data
{
    public class DataService : IDataService
    {
        private KibiiContext _db;

        public DataService(KibiiContext db)
        {
            _db = db;
        }

        public Task AddAnimeAsync(Anime anime) =>
            _db.Anime.AddAsync(anime);

        public Task AddGenreAsync(Genre genre) =>
            _db.Genres.AddAsync(genre);

        public void AddEpisode(Anime anime, Episode episode)
        {
            anime.Episodes.Add(episode);
            _db.Entry(anime).State = EntityState.Modified;
        }

        public Task<Anime> GetAnimeByMasterIdAsync(int id) =>
            _db.Anime.Include(c => c.Episodes).FirstOrDefaultAsync(x => x.MasterId == id);

        public IQueryable<Episode> GetEpisodeRangeAsync(List<int> episodeIds) =>
            _db.Episodes.Include(a => a.Anime).Where(c => episodeIds.Contains(c.MasterId));

        public Task<Genre> GetGenreAsync(string name) =>
            _db.Genres.FirstOrDefaultAsync(x => x.Name == name);

        public Task<Genre> GetGenreByIdAsync(string id) =>
            _db.Genres.FirstOrDefaultAsync(x => x.Id == id);

        public IQueryable<Anime> GetUncompletedAnimeAsync() =>
            _db.Anime.Include(c => c.Episodes)
               .Where(a => a.Status == AnimeStatus.Ongoing || a.Status == AnimeStatus.NotYetAired);

        public Task SaveChangesAsync() =>
            _db.SaveChangesAsync();

        public void UpdateAnime(Anime anime)
        {
            _db.Anime.Attach(anime);
            _db.Entry(anime).State = EntityState.Modified;
        }

        public void UpdateEpisode(Episode episode)
        {
            _db.Episodes.Update(episode);
            _db.Entry(episode).State = EntityState.Modified;
        }

        public Task<Episode> GetEpisodeByMasterIdAsync(int id) =>
            _db.Episodes.Include(c => c.Blobs).FirstOrDefaultAsync(c => c.MasterId == id);

        public async Task<List<int>> GetUnexistingAnimeAsync(List<int> animeIds)
        {
            List<int> existingAnimeIds = await _db.Anime
                .Select(c => c.MasterId)
                .Where(a => animeIds.Contains(a))
                .ToListAsync();

            animeIds.RemoveAll(id => existingAnimeIds.Contains(id));
            return animeIds;
        }
    }
}
