﻿using KibiiTV.Data.Generator.Models.Remote;
using KibiiTV.Data.Generator.Services.Cdn;
using KibiiTV.Data.Media;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace KibiiTV.Data.Generator.Services.Data
{
    public class ConverterService : IConverterService
    {
        private ICdnService cdnService;
        private IDataService dataService;
        private ILogger logger;

        public ConverterService(IDataService dataService, ICdnService cdnService, ILogger<ConverterService> logger)
        {
            this.cdnService = cdnService;
            this.dataService = dataService;
            this.logger = logger;
        }

        public Anime CreateAnime(MAnime anime)
        {
            string newEnglishTitle = anime.Info?.Synonyms?.FirstOrDefault(x => x?.Type == 1)?.Title;
            string newJapaneseTitle = anime.Info?.Synonyms?.FirstOrDefault(x => x?.Type == 2)?.Title;

            Anime newAnime = new Anime()
            {
                AgeRating = anime.Info.Info.AgeRating,
                Description = anime.Info.Info.Synopsis,
                FinishedAiringDate = anime.FinishedAiringDate,
                StartedAiringDate = anime.StartedAiringDate,
                Type = (AnimeType)anime.Type,
                MasterId = anime.Id,
                Title = anime.Title,
                Score = anime.Score,
                YoutubeTrailerId = anime.Info.Info.YoutubeTrailerId,
                Status = (AnimeStatus)anime.Status,
                EnglishTitle = newEnglishTitle,
                WallpaperId = anime.GeneratedWallaperId,
                CoverId = anime.GeneratedCoverId,
                JapaneseTitle = newJapaneseTitle
            };

            if (anime.Info.Info.UsersCompleted != null)
                newAnime.AddViewCount(anime.Info.Info.UsersCompleted.Value);

            return newAnime;
        }


        public Episode CreateEpisode(MEpisode episode, MAnime anime)
        {
            string durationString = episode.Info.Duration
                                    ?? anime.Info.Info.EpisodeLength?.ToString()
                                    ?? "0";

            int duration;
            int episodeNumber;

            try
            { duration = Convert.ToInt32(durationString); }
            catch
            { duration = 0; }

            try
            { episodeNumber = Convert.ToInt32(episode.Info.Episode ?? "1"); }
            catch
            { episodeNumber = 0; }

            DateTime airedOn = episode.Info.Aired
                               ?? anime.StartedAiringDate
                               ?? new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

            Episode newEpisode = new Episode()
            {
                Duration = duration,
                Aired = airedOn,
                Description = episode.Info.Description,
                EpisodeNumber = episodeNumber,
                MasterId = episode.Info.Id,
                Type = (AnimeType)episode.Info.Type,
                Title = episode.Info.Title,
                ThumbnailId = episode.GeneratedThumbnailId
            };

            return newEpisode;
        }

        public Blob CreateBlob(Mirror mirror)
        {
            Blob newEmbed = new Blob()
            {
                Dubbed = IsDubbed(mirror.Type),
                BlobId = mirror.EmbedId,
                BlobPrefix = mirror.Host.EmbedPrefix,
                BlobSuffix = mirror.Host.EmbedSuffix,
                MasterId = mirror.Id,
                Quality = mirror.Quality ?? 128,
                Name = mirror.Host.Name
            };

            return newEmbed;
        }

        private bool IsDubbed(int? Type) =>
            ((Type ?? 0) == 1 ? false : true);
    }
}
