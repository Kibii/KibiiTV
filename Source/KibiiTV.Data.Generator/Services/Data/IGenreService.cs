﻿using KibiiTV.Data.Generator.Models.Remote;
using KibiiTV.Data.Media;
using System.Threading.Tasks;

namespace KibiiTV.Data.Generator.Services.Data
{
    public interface IGenreService
    {
        Task<Genre> GetGenreAsync(string Name);
        Task<Genre> GetGenreByIdAsync(string Id);
        Task<Genre> CreateGenreAsync(string Name);
        Task<Genre> GetOrCreateAsync(string Name);
        Task<Anime> FillGenresAsync(Anime anime, MAnime mAnime);
        Genre GetGenre(string Name);
        Genre GetGenreById(string Id);
        Genre CreateGenre(string Name);
        Genre GetOrCreate(string Name);
        Anime FillGenres(Anime anime, MAnime mAnime);
    }
}
