﻿using KibiiTV.Data.Media;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Data.Generator.Services.Data
{
    public interface IDataService
    {
        Task AddGenreAsync(Genre genre);
        Task SaveChangesAsync();
        Task<Genre> GetGenreAsync(string name);
        Task<Genre> GetGenreByIdAsync(string id);
        Task AddAnimeAsync(Anime anime);
        Task<Anime> GetAnimeByMasterIdAsync(int id);
        IQueryable<Episode> GetEpisodeRangeAsync(List<int> episodeIds);
        IQueryable<Anime> GetUncompletedAnimeAsync();
        void AddEpisode(Anime anime, Episode episode);
        void UpdateAnime(Anime anime);
        void UpdateEpisode(Episode episode);
        Task<Episode> GetEpisodeByMasterIdAsync(int id);
        Task<List<int>> GetUnexistingAnimeAsync(List<int> animeIds);
    }
}
