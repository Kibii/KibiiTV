﻿using System.IO;
using System.Threading.Tasks;

namespace KibiiTV.Data.Generator.Services.Cdn
{
    public interface IS3Service
    {
        Task UploadFileAsync(string newCdnId, string contenType, Stream inputStream);
    }
}