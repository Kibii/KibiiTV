﻿using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Threading.Tasks;

namespace KibiiTV.Data.Generator.Services.Cdn
{
    public class S3Service : IS3Service
    {
        private ILogger Logger { get; set; }
        private IAmazonS3 Client { get; set; }

        public S3Service(ILogger<S3Service> logger)
        {
            Logger = logger;
            Client = new AmazonS3Client(Global.S3AccessKey, Global.S3SecretKey, Global.S3Endpoint);
        }

        public Task UploadFileAsync(string newCdnId, string contentType, Stream inputStream)
        {
            PutObjectRequest Request = new PutObjectRequest()
            {
                BucketName = Global.S3BucketName,
                Key = newCdnId,
                AutoCloseStream = true,
                ContentType = contentType,
                CannedACL = S3CannedACL.PublicRead,
                AutoResetStreamPosition = true,
                InputStream = inputStream
            };

            return Client.PutObjectAsync(Request);
        }
    }
}
