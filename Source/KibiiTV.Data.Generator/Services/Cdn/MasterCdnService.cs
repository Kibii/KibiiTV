﻿using KibiiTV.Data.Generator.Models.Cdn;
using KibiiTV.Data.Generator.Models.Http;
using KibiiTV.Data.Generator.Services.Master;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace KibiiTV.Data.Generator.Services.Cdn
{
    public class MasterCdnService : IMasterCdnService
    {
        private ILogger Logger { get; set; }
        private IMasterHttpService HttpService { get; set; }

        public MasterCdnService(ILogger<MasterCdnService> logger, IMasterHttpService masterHttpService)
        {
            Logger = logger;
            masterHttpService.ConfigureData(includeData: false);
            HttpService = masterHttpService;
        }

        public async Task<MasterHttpResponse> DownloadFileAsync(CdnUpload Upload)
        {
            MasterHttpResponse response = await HttpService.ExecuteRequestAsync(Global.GET, Upload.ToUri());

            if (response.StatusCode != 200)
                return null;

            return response;
        }
    }
}
