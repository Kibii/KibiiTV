﻿using KibiiTV.Data.Generator.Models.Cdn;
using KibiiTV.Data.Generator.Models.Http;
using System.Threading.Tasks;

namespace KibiiTV.Data.Generator.Services.Cdn
{
    public interface IMasterCdnService
    {
        Task<MasterHttpResponse> DownloadFileAsync(CdnUpload Upload);
    }
}