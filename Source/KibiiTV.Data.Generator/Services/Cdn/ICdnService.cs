﻿using KibiiTV.Data.Generator.Models.Cdn;

namespace KibiiTV.Data.Generator.Services.Cdn
{
    public interface ICdnService
    {
        string GenerateCdnId(string oldCdnId);
        void QueueCdnUpload(CdnUpload upload);
        CdnUpload CreateCoverUpload(string oldCdnId, string newCdnId);
        CdnUpload CreateThumbnailUpload(string oldCdnId, string newCdnId);
        CdnUpload CreateWallpaperUpload(string oldCdnId, string newCdnId);
        CdnUpload CreateCdnUpload(string oldCdnId, string newCdnId, CdnUploadType type);
    }
}
