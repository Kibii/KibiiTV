﻿using KibiiTV.Data.Generator.Models.Cdn;
using KibiiTV.Data.Generator.Models.Http;
using KibiiTV.Data.Helpers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Data.Generator.Services.Cdn
{
    public class CdnService : ICdnService
    {
        private IMasterCdnService MasterCdnService { get; set; }
        private IS3Service S3Service { get; set; }
        private ILogger Logger { get; set; }
        private static List<Task> RunnerTasks = new List<Task>();
        private static List<CdnUpload> Uploads = new List<CdnUpload>();

        public CdnService(IMasterCdnService masterCdnService, IS3Service s3Service, ILogger<CdnService> logger)
        {
            Logger = logger;
            S3Service = s3Service;
            MasterCdnService = masterCdnService;

            while (RunnerTasks.Count <= Global.MaxRunners)
            {
                Task RunnerTask = Task.Factory.StartNew(CdnManagementRunner, TaskCreationOptions.LongRunning);
                RunnerTasks.Add(RunnerTask);
            }
        }

        private async Task CdnManagementRunner()
        {
            int RunnerId = Task.CurrentId ?? 0;
            Logger.LogInformation("Runner " + RunnerId  + " has been started!");

            while (true)
            {
                await Task.Delay(Global.S3RunnerDelay); // Give the images a smol delay tbh.

                CdnUpload upload = Uploads.FirstOrDefault();

                if (upload == null) // Chances are that we'll have an empty list.
                {
                   // if(RunnerTasks.FirstOrDefault().Id == RunnerId)
                     //   Logger.LogWarning("No uploads available!");
                    continue;
                }

                Uploads.Remove(upload);

                Logger.LogInformation("Starting download/upload for " + upload.OldCdnId);
                MasterHttpResponse resp = await MasterCdnService.DownloadFileAsync(upload);

                if (resp == null)
                {
                    Logger.LogWarning("File " + upload.Type + " data is null! CDN Id: " + upload.OldCdnId);
                    continue;
                }

                string contentType = resp.Response.Content.Headers.ContentType.ToString();

                await S3Service.UploadFileAsync(upload.NewCdnId, contentType, await resp.Response.Content.ReadAsStreamAsync());

                Logger.LogInformation("Finished download/upload for " + upload.OldCdnId + " as " + upload.NewCdnId);
            }
        }

        public void QueueCdnUpload(CdnUpload upload)
        {
            Uploads.Add(upload);
            Logger.LogInformation("Enqueued upload for " + upload.OldCdnId + " to be uploaded as " + upload.NewCdnId);
        }

        public string GenerateCdnId(string oldCdnId)
        {
            string guid = Guid.NewGuid().ToString();
            string id = IdGenerator.NewId();
            string[] splits = oldCdnId.Split('.');
            string ext;

            if (splits.Length <= 1)
                ext = "jpg";
            else
                ext = splits[splits.Length - 1];

            return "zero-two/" + guid + "/" + id + "." + ext;
        }

        public CdnUpload CreateCdnUpload(string oldCdnId, string newCdnId, CdnUploadType type) =>
            new CdnUpload() { OldCdnId = oldCdnId, NewCdnId = newCdnId, Type = type };

        public CdnUpload CreateCoverUpload(string oldCdnId, string newCdnId) =>
            CreateCdnUpload(oldCdnId, newCdnId, CdnUploadType.Cover);

        public CdnUpload CreateThumbnailUpload(string oldCdnId, string newCdnId) =>
            CreateCdnUpload(oldCdnId, newCdnId, CdnUploadType.Thumbnail);

        public CdnUpload CreateWallpaperUpload(string oldCdnId, string newCdnId) =>
            CreateCdnUpload(oldCdnId, newCdnId, CdnUploadType.Wallpaper);
    }
}
