﻿using Discord;
using Discord.Net;
using Discord.Rest;
using Discord.Webhook;
using KibiiTV.Data.Generator.Models.Discord;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace KibiiTV.Data.Generator.Services.Discord
{
    // TODO: IMPLEMENT LOGGING!
    // alway rember shimakaze hetnai
    public class DiscordService : IDiscordService
    {
        private static Task WebhookRunner { get; set; }
        private static DiscordWebhookClient Client { get; set; }
        private static Dictionary<string, string> Anime { get; set; }
        private static int EmbedCount = 0;
        private static int EpisodeCount = 0;

        private const string DefaultFileName = "kibii.data.generator_file";

        private ILogger _logger { get; set; }

        public DiscordService(ILogger<DiscordService> logger)
        {
            _logger = logger;

            DiscordRestConfig restConfig = new DiscordRestConfig()
            {
                DefaultRetryMode = RetryMode.AlwaysRetry
            };

            if (Client == null)
                Client = new DiscordWebhookClient(Global.DiscordWebhookId, Global.DiscordWebhookToken, restConfig);

            if (Anime == null)
                Anime = new Dictionary<string, string>();

            // if (WebhookRunner == null)
            //      WebhookRunner = Task.Factory.StartNew(RunningWebhookTask, TaskCreationOptions.LongRunning);
        }


        private async Task RunningWebhookTask()
        {
            try
            {
                if (Global.DiscordInitialWaitEnabled)
                    await Task.Delay(120 * 1000); // Wait 60 seconds for the intial startup.

                while (true) // todo implement check for anime
                {
                    if (Anime.Count > 1 || EmbedCount > 1 || EpisodeCount > 1) // We'll only do it if we're above 1 or two episodes
                    {
                        var Embed = CreateAnimeEmbed(Anime);
                        await DeployEmbed(Embed);
                        EmbedCount = 0;
                        EpisodeCount = 0;
                        Anime.Clear();
                    }
                    else
                    {
                        return;
                    }

                    await Task.Delay(Global.DiscordRunnerDelay); // Should be above 1-2 minutes or similar
                }
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception, "An unhandled exception was thrown by the Discord service");
            }

            /*
             // Add this back if we encounter issues. 
             Dictionary<string, string> localAnime = Anime.ToDictionary(c => c.Key, o => o.Value);
             
             if (localAnime.Count > 1) // We'll only do it if we're above 1 or two episodes
             {
                var Embed = CreateAnimeEmbed(localAnime);
                EmbedCount = 0;
                Anime.Clear();
                localAnime.Clear();
             }
            */
        }

        private Embed CreateAnimeEmbed(Dictionary<string, string> anime)
        {
            string AnimeNames = "";
            string AnimeIds = "";

            foreach (var a in anime)
            {
                AnimeNames += "**" + a.Key + "**\n";
                AnimeIds += "「" + a.Value + "」\n";
            }

            return new EmbedBuilder()
                .WithAuthor(a => a
                .WithName("Kibii SeriesDeploy")
                .WithIconUrl(Global.DiscordIconUrl))
            .WithFooter(a => a
                .WithText("Brought to you by Kibii TV")
                .WithIconUrl(Global.DiscordIconUrl))
            .WithColor(255, 155, 208)
            .WithCurrentTimestamp()
            .WithThumbnailUrl(Global.DiscordIconUrl)
            .AddInlineField("Deployed Anime", AnimeNames)
            //  .AddInlineField("Id", AnimeIds)
            .AddInlineField("Deployed Episodes", $"**{EpisodeCount}**")
            .AddInlineField("Deployed Mirrors", $"**{EmbedCount}**")
            .WithDescription("Kibii SeriesDeploy brings live updates from Kibii's database of anime and any new additions to it.")
            .Build();
        }

        public void DeployItem(DiscordItemType Type) =>
            DeployItem(Type, null, null);

        public void DeployMirror() =>
            DeployItem(DiscordItemType.Blob);

        public void DeploySeries(string SeriesName, string SeriesId) =>
            DeployItem(DiscordItemType.Series, SeriesName, SeriesId);

        public void DeployEpisode() =>
            DeployItem(DiscordItemType.Episode);

        public void DeployItem(DiscordItemType Type, string ItemName, string ItemId)
        {
            if (ItemName != null || ItemId != null)
                return; // for now

            if (Type == DiscordItemType.Blob)
            {
                EmbedCount++;
            }
            else if (Type == DiscordItemType.Episode)
            {
                EpisodeCount++;
            }
            else
            {
                Anime.Add(ItemName, ItemId);
            }
        }

        public Task DeployNewEpisodeAsync(string animeName, string animeId, string episodeName, int episodeNumber, string posterId)
        {
            string animeSlug = UrlSlugger.ToUrlSlug(animeName ?? "anime-id-" + animeId);
            string episodeSlug = UrlSlugger.ToUrlSlug(episodeName ?? "episode " + HumanFriendlyInteger.IntegerToWritten(episodeNumber));
            string finalUrl = $"https://beta.kibii.tv/anime/{animeId}/{animeSlug}";
            string finalWatchUrl = finalUrl + $"/watch/{episodeNumber}/{episodeSlug}";
            string cdnUrl = $"https://deru.kibii.tv/{posterId}";

            episodeName = episodeName == null ? null : $" \"{episodeName}\"";

            string embedData = $"**{animeName}** episode **{episodeNumber}** released on "
                + $"[Kibii TV](https://beta.kibii.tv)!\n\nWatch [episode {episodeNumber}{episodeName} now.]({finalWatchUrl})"
                + $"\nCheck out [**{animeName}**]({finalUrl}) on Kibii TV.";

            Embed embed = new EmbedBuilder()
            .WithColor(255, 155, 208)
            .WithCurrentTimestamp()
            .WithThumbnailUrl(cdnUrl)
            .WithDescription(embedData)
            .WithAuthor(a => a
                .WithName($"「{animeName}」")
                .WithIconUrl(Global.DiscordIconUrl))
            .WithFooter(a => a
                .WithText("Brought to you by Kibii TV")
                .WithIconUrl(Global.DiscordIconUrl))
            .Build();

            async Task SendEmbedAsync()
            {
                try
                {
                    await DeployEmbed(embed);
                }
                catch (Exception ex)
                {
                    if (ex is TimeoutException || ex is RateLimitedException)
                    {
                        _logger.LogWarning(ex, "Exception while attempting to send a webhook");
                        await SendEmbedAsync();
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            return Task.Run(async () => await SendEmbedAsync());
        }

        public Task DeployMessage(string Message) =>
            Client.SendMessageAsync(Message);

        public Task DeployEmbed(Embed Embed) =>
            DeployEmbed(null, Embed);

        public Task DeployEmbed(string Message, Embed Embed) =>
            Client.SendMessageAsync(string.IsNullOrWhiteSpace(Message) ? "" : Message, embeds: new Embed[] { Embed });

        public Task DeployFile(Stream FileData, string Message, string FileName) =>
            Client.SendFileAsync(FileData, FileName, Message);

        public Task DeployFile(Stream FileData) =>
            DeployFile(FileData, null, DefaultFileName);

        public Task DeployFile(string FilePath, string Message) =>
            DeployFile(new FileStream(FilePath, FileMode.Open), Message, DefaultFileName);

        public Task DeployFile(byte[] FileData, string Message) =>
            DeployFile(new MemoryStream(FileData), Message, DefaultFileName);

        public Task DeployFile(string FilePath) =>
            DeployFile(new FileStream(FilePath, FileMode.Open), null, DefaultFileName);

        public Task DeployFile(byte[] FileData) =>
            DeployFile(new MemoryStream(FileData), null, DefaultFileName);

        public Task DeployFile(string FileName, Stream FileData, string Message) =>
            DeployFile(FileData, Message, FileName);

        public Task DeployFile(string FileName, string FilePath, string Message) =>
            DeployFile(new FileStream(FilePath, FileMode.Open), Message, FileName);

        public Task DeployFile(string FileName, byte[] FileData, string Message) =>
            DeployFile(new MemoryStream(FileData), Message, FileName);

        public Task DeployFile(Stream FileData, string Message) =>
            DeployFile(FileData, Message, DefaultFileName);

        public Task DeployFileFromPathWithMessage(string FilePath, string Message) =>
            DeployFile(DefaultFileName, FilePath, Message);

        public Task DeployFile(string FileName, Stream FileData) =>
            DeployFile(FileData, null, FileName);

        public Task DeployFile(string FileName, byte[] FileData) =>
            DeployFile(new MemoryStream(FileData), null, FileName);
    }
}
