﻿using Discord;
using KibiiTV.Data.Generator.Models.Discord;
using System.IO;
using System.Threading.Tasks;

namespace KibiiTV.Data.Generator.Services.Discord
{
    public interface IDiscordService
    {
        void DeployMirror();
        void DeployEpisode();
        void DeploySeries(string SeriesName, string SeriesId); 
        void DeployItem(DiscordItemType Type);
        void DeployItem(DiscordItemType Type, string ItemName, string ItemId);
        Task DeployNewEpisodeAsync(string animeName, string animeId, string episodeName, int episodeNumber, string posterId);
        Task DeployMessage(string Message);
        Task DeployEmbed(Embed Embed);
        Task DeployEmbed(string Message, Embed Embed);
        Task DeployFile(string FileName, Stream FileData, string Message);
        Task DeployFile(string FileName, string FilePath, string Message);
        Task DeployFile(string FileName, byte[] FileData, string Message);
        Task DeployFile(Stream FileData, string Message);
        Task DeployFileFromPathWithMessage(string FilePath, string Message); // thanks visual studio
        Task DeployFile(byte[] FileData, string Message);
        Task DeployFile(string FileName, Stream FileData);
        Task DeployFile(string FileName, string FilePath);
        Task DeployFile(string FileName, byte[] FileData);
        Task DeployFile(Stream FileData);
        Task DeployFile(string FilePath);
        Task DeployFile(byte[] FileData);
    }
}
