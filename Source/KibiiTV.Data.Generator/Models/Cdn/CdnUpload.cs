﻿using KibiiTV.Data.Helpers;

namespace KibiiTV.Data.Generator.Models.Cdn
{
    public class CdnUpload
    {
        public CdnUpload()
        {
            Id = IdGenerator.NewId();
        }

        public string Id { get; set; }
        public string OldCdnId { get; set; }
        public string NewCdnId { get; set; }
        public CdnUploadType Type { get; set; }

        public string ToUri()
        {
            string SubEndpoint = "/episodes/";

            if (Type == CdnUploadType.Wallpaper)
                SubEndpoint = "/wallpaper/0/";
            else if (Type == CdnUploadType.Cover)
                SubEndpoint = "/poster/";

            return Global.CDN_URL + SubEndpoint + OldCdnId;
        }
    }
}
