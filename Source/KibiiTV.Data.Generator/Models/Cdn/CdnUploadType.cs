﻿namespace KibiiTV.Data.Generator.Models.Cdn
{
    public enum CdnUploadType
    {
        Cover = 0,
        Thumbnail = 1,
        Wallpaper = 2,
    }
}