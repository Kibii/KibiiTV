﻿namespace KibiiTV.Data.Generator.Models.Discord
{
    public enum DiscordItemType
    {
        Blob = 0,
        Series = 1,
        Episode = 2
    }
}
