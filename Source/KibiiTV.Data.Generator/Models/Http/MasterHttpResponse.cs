﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace KibiiTV.Data.Generator.Models.Http
{
    public class MasterHttpResponse
    {
        public HttpResponseMessage Response { get; set; }
        public Dictionary<string, string> Headers { get; set; }
        public bool IsSuccess => ValidateSuccessStatusCode();
        public int StatusCode { get; set; }
        public string Data { get; set; }

        public string GetHeader(string key) => Headers[key];
        public T AsObject<T>() => JsonConvert.DeserializeObject<T>(Data, Global.JSON_SETTINGS);

        private bool ValidateSuccessStatusCode()
        {
            int sc = (StatusCode - 200);
            return sc < 100 && sc > 0 ? true : false;
        }
    }
}
