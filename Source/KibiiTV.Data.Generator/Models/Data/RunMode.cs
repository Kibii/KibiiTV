﻿namespace KibiiTV.Data.Generator.Models.Data
{
    public enum RunMode
    {
        Normal = 0,
        Listen = 1,
        Episode = 2,
        Anime = 3,
        All = 4,
    }
}
