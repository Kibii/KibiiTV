﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace KibiiTV.Data.Generator.Models.Remote
{
    public partial class AnimeInfo
    {
        [JsonProperty("info")]
        public Info Info { get; set; }

        [JsonProperty("synonyms")]
        public List<Synonym> Synonyms { get; set; }

        [JsonProperty("genres")]
        public List<MGenre> Genres { get; set; }

        [JsonProperty("poster")]
        public string Poster { get; set; }

        [JsonProperty("franchise_count")]
        public int? FranchiseCount { get; set; }

        [JsonProperty("wallpapers")]
        public List<Wallpaper> Wallpapers { get; set; }

        [JsonProperty("episodes")]
        public List<MEpisode> Episodes { get; set; }
    }
}
