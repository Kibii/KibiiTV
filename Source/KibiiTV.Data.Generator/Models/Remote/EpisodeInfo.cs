﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Runtime.Serialization;

namespace KibiiTV.Data.Generator.Models.Remote
{
    public partial class EpisodeInfo
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("anime_id")]
        public int? AnimeId { get; set; }

        [JsonProperty("episode")]
        public string Episode { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("tvdb_id")]
        public int? TvdbId { get; set; }

        [JsonProperty("aired")]
        public DateTime? Aired { get; set; }

        [JsonProperty("type")]
        public int? Type { get; set; }

        [JsonProperty("duration")]
        public string Duration { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [OnError]
        internal void OnError(StreamingContext context, ErrorContext errorContext)
        {
            errorContext.Handled = true;
        }
    }
}
