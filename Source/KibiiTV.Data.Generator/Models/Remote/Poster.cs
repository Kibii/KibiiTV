﻿using Newtonsoft.Json;

namespace KibiiTV.Data.Generator.Models.Remote
{
    public partial class Poster
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("path")]
        public string Path { get; set; }

        [JsonProperty("extension")]
        public string Extension { get; set; }

        [JsonProperty("file")]
        public string File { get; set; }
    }
}
