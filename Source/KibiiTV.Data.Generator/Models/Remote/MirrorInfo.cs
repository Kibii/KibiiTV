﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace KibiiTV.Data.Generator.Models.Remote
{
    public class MirrorInfo
    {
        [JsonProperty("mirrors")]
        public List<Mirror> Mirrors { get; set; }
    }
}
