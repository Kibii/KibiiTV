﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace KibiiTV.Data.Generator.Models.Remote
{
    public partial class MEpisode
    {
        [JsonProperty("info")]
        public EpisodeInfo Info { get; set; }

        [JsonProperty("thumbnail")]
        public string Thumbnail { get; set; }

        [JsonIgnore]
        public List<Mirror> Mirrors { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string GeneratedThumbnailId { get; set; }
    }
}
