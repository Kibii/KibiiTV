﻿using Newtonsoft.Json;

namespace KibiiTV.Data.Generator.Models.Remote
{
    public partial class Synonym
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("type")]
        public int Type { get; set; }
    }
}
