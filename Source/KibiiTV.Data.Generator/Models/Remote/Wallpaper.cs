﻿using Newtonsoft.Json;

namespace KibiiTV.Data.Generator.Models.Remote
{
    public partial class Wallpaper
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("file")]
        public string File { get; set; }
    }
}
