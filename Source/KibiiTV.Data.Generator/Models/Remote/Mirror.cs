﻿using Newtonsoft.Json;

namespace KibiiTV.Data.Generator.Models.Remote
{
    public partial class Mirror
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("host_id")]
        public int? HostId { get; set; }

        [JsonProperty("embed_id")]
        public string EmbedId { get; set; }

        [JsonProperty("quality")]
        public int? Quality { get; set; }

        [JsonProperty("type")]
        public int? Type { get; set; }

        [JsonProperty("host")]
        public Host Host { get; set; }
    }
}
