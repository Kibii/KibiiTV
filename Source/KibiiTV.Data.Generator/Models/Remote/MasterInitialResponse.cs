﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace KibiiTV.Data.Generator.Models.Remote
{
    public class MasterInitialResponse : MasterResponseCore
    {
        [JsonProperty("data")]
        public IDictionary<string, MAnime> Anime { get; set; }
    }
}
