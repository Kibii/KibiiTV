﻿using Newtonsoft.Json;

namespace KibiiTV.Data.Generator.Models.Remote
{
    public class SpecialVideoMirror
    {
        [JsonProperty("res")]
        public string Resolution { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("src")]
        public string Source { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
