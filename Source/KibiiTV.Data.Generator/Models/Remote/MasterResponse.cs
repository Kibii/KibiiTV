﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace KibiiTV.Data.Generator.Models.Remote
{
    public class MasterResponseCore
    {
        [JsonProperty("total")]
        public int? Total { get; set; }

        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

        [JsonProperty("current_page")]
        public int CurrentPage { get; set; }

        [JsonProperty("last_page")]
        public int LastPage { get; set; }

        [JsonProperty("next_page_url")]
        public string NextPageUrl { get; set; }

        [JsonProperty("prev_page_url")]
        public string PrevPageUrl { get; set; }

        [JsonProperty("from")]
        public int? From { get; set; }

        [JsonProperty("to")]
        public int? To { get; set; }
    }

    public class MasterResponse : MasterResponseCore
    {
        [JsonProperty("data")]
        public IList<MAnime> Anime { get; set; }
    }
}
