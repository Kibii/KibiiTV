﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace KibiiTV.Data.Generator
{
    public static class Extentions
    {
        private static Random rng = new Random();

        public static T RandomElement<T>(this IList<T> list) => 
            list[rng.Next(list.Count)];

        public static T RandomElement<T>(this T[] array) => 
            array[rng.Next(array.Length)];

        public static T Clone<T>(this T Source) =>
            Source == null ? default
            : JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(Source),
            new JsonSerializerSettings() { ObjectCreationHandling = ObjectCreationHandling.Replace });

        public static string ToJson<T>(this T Source) =>
            JsonConvert.SerializeObject(Source);

        public static string ToJson<T>(this T Source, Formatting Format) =>
            JsonConvert.SerializeObject(Source, Format);

        public static string ToJson<T>(this T Source, JsonSerializerSettings Settings) =>
            JsonConvert.SerializeObject(Source, Settings);

        public static string ToJson<T>(this T Source, Formatting Format, JsonSerializerSettings Settings) =>
            JsonConvert.SerializeObject(Source, Format, Settings);

        public static T FromJson<T>(this T Source, string Data) =>
            JsonConvert.DeserializeObject<T>(Data);

        public static T FromJson<T>(this T Source, string Data, JsonSerializerSettings Settings) =>
            JsonConvert.DeserializeObject<T>(Data, Settings);

        public static T FromJson<T>(this object Object, string Data) =>
            JsonConvert.DeserializeObject<T>(Data);
    }
}