﻿using Amazon;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;

namespace KibiiTV.Data.Generator
{
    public class Global
    {
        public const string API_SCHEMA = "https";
        public const string API_SCHEMA_SEPERATOR = "://";
        public const string API_WWW_DOMAIN = "www";
        public const string API_CORE_DOMAIN = "masterani.me";
        public const string API_CORE_URL = API_SCHEMA + API_SCHEMA_SEPERATOR + API_WWW_DOMAIN + "." + API_CORE_DOMAIN;
        public const string API_URL = API_CORE_URL + "/api";
        public const string API_ANIME_URL = API_URL + "/anime";
        public const string API_ANIME_DETAILS_URL = API_ANIME_URL + "/[ANIME_ID]/detailed";
        public const string SEARCH_URL = API_ANIME_URL + "/filter";
        public const string CDN_URL = API_SCHEMA + API_SCHEMA_SEPERATOR + "cdn." + API_CORE_DOMAIN;
        public const string EPISODE_URL = API_CORE_URL + "/anime/watch/[ANIME_SLUG]/[EPISODE_NUMBER]";

        public static string API_USER_AGENT => Config.GetSection("Http").GetValue("UserAgent", "NotDefined/1.0 UserAgentIsNotDefined");
        public static int API_MAX_TIMEOUT => Config.GetSection("Http").GetValue("MaxHttpTimeout", 10); // Change this TimeSpan to be dynamically parsed
        public static int API_MAX_TRIES => Config.GetSection("Http").GetValue("MaxHttpTries", 10);

        public static HttpMethod GET = new HttpMethod("GET");
        public static HttpMethod POST = new HttpMethod("POST");
        public static DateTime DEFAULT_TIME = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        public static JsonSerializerSettings JSON_SETTINGS = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Ignore };

        public static IConfiguration Config { get; private set; }
        public static bool DiscordInitialWaitEnabled => Config.GetSection("Discord").GetValue("InitialWaitEnabled", true);
        public static string DiscordWebhookToken => Config.GetSection("Discord").GetValue("WebhookToken", "N/A");
        public static string DiscordIconUrl => Config.GetSection("Discord").GetValue("IconUrl", "N/A");
        public static ulong DiscordWebhookId => Config.GetSection("Discord").GetValue("WebhookId", 0UL);
        public static int DiscordRunnerDelay => Config.GetSection("Discord").GetValue("RunnerDelay", 120 * 1000); // Change this to be a dynamic TimeSpan
        public static int MaxRunners => Config.GetSection("S3").GetValue("MaxRunners", 6);
        public static int S3RunnerDelay => Config.GetSection("S3").GetValue("RunneryDelay", 500);
        public static string S3SecretKey => Config.GetSection("S3").GetValue("SecretKey", "N/A");
        public static string S3AccessKey => Config.GetSection("S3").GetValue("AccessKey", "N/A");
        public static string S3BucketName => Config.GetSection("S3").GetValue("BucketName", "N/A");
        public static RegionEndpoint S3Endpoint => RegionEndpoint.GetBySystemName(Config.GetSection("S3").GetValue("Region", "us-east-1"));

        public static string GetPagedSearchUrl(int Page) =>
            GetPagedSearchUrl(Page.ToString());

        public static string GetPagedSearchUrl(string Page) =>
            SEARCH_URL + "?order=score_desc&page=" + Page;

        public static void GetConfiguration(string[] args = null)
        {
            if (args == null)
                args = new string[0];

            Config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddCommandLine(args)
                .AddEnvironmentVariables("KIBII_")
                .Build();
        }

        public static string DbConnectionString =>
            Config.GetValue<string>("ConnStr") ?? Config.GetConnectionString("ConnStr");

        public static bool DebugEnabled =>
            Config.GetValue<bool>("DebugEnabled", true);
    }
}
