﻿using KibiiTV.Data.Generator.Models.Cdn;
using KibiiTV.Data.Generator.Models.Data;
using KibiiTV.Data.Generator.Models.Remote;
using KibiiTV.Data.Generator.Services.Cdn;
using KibiiTV.Data.Generator.Services.Data;
using KibiiTV.Data.Generator.Services.Discord;
using KibiiTV.Data.Generator.Services.Master;
using KibiiTV.Data.Media;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Data.Generator
{
    public class DataApp
    {
        private readonly IMasterApiService _masterApiService;
        private readonly IConverterService _converterService;
        private readonly IDiscordService _discordService;
        private readonly IGenreService _genreService; // finish adding _'s to the rest of the files.
        private readonly IDataService _dataService;
        private readonly ICdnService _cdnService;
        private readonly ILogger _logger;
        private RunMode _mode;
        private string[] _args;
        private string _animeId;
        private string _episodeId;
        private string _overwriteMode;
        private string _exitMode;
        private readonly string _runModeHelpString =
            "Help - Displays this. System name: help\n" +
            "Normal - Runs the data generator on normal mode. System name: normal\n" +
            "Listen - Listens for new anime being released. System name: listen\n" +
            "Episode - Redownloads an episode and uploads it again. System name: episode\n" +
            "Anime - Redownloads an anime and its episodes and uploads it again. System name: anime\n" +
            "All - Redownloads all the anime and uploads it again. System name: all\n" +
            "\n" +
            "Usage:" +
            "dotnet data_generator.dll --mode=episode --episode=1 --anime=26";

        public DataApp(ILogger<DataApp> logger, IDataService dataService, ICdnService cdnService, IDiscordService discordService,
            IMasterApiService masterApiService, IGenreService genreService, IConverterService converterService)
        {
            _logger = logger;
            _cdnService = cdnService;
            _dataService = dataService;
            _genreService = genreService;
            _discordService = discordService;
            _converterService = converterService;
            _masterApiService = masterApiService;
        }

        public void SetCommandLineArguments(string[] args)
        {
            _args = args;
        }

        public string GetExitMode()
        {
            return _exitMode == "normal" || _exitMode == "keep-alive" ? _exitMode : "normal";
        }

        public bool SetRunMode()
        {
            if (_args == null)
                return false;

            IConfiguration runModeConfig = new ConfigurationBuilder()
                                               .AddCommandLine(_args)
                                               .AddEnvironmentVariables("KIBII_")
                                               .Build();

            string runMode = runModeConfig.GetValue<string>("mode");
            string episodeId = runModeConfig.GetValue<string>("episode-id");
            string animeId = runModeConfig.GetValue<string>("anime-id");
            string overwriteMode = runModeConfig.GetValue<string>("ow") ?? runModeConfig.GetValue<string>("overwrite");
            _exitMode = runModeConfig.GetValue<string>("exit") ?? "keep-alive";

            //remove listen run mode sometime in the future;
            if (overwriteMode != null)
            {
                if (overwriteMode == "true")
                {
                    _overwriteMode = "full";
                }
                else
                {
                    _overwriteMode = "none";
                }
            }
            else
            {
                _overwriteMode = "none";
            }

            switch (runMode)
            {
                case "normal":
                    _mode = RunMode.Normal;
                    break;
                case "bot":
                    _mode = RunMode.Listen;
                    break;
                case "all":
                    _mode = RunMode.All;
                    break;
                case "episode":
                    _mode = RunMode.Episode;
                    if (string.IsNullOrWhiteSpace(episodeId))
                    {
                        _logger.LogError("Invalid episode ID provided! Please fix or input the episode ID using `--anime=[id]`");
                        return false;
                    }
                    else if (string.IsNullOrWhiteSpace(animeId))
                    {
                        _logger.LogError("Invalid anime ID provided! Please fix or input the anime ID using `--anime=[id]`");
                        return false;
                    }
                    else
                    {
                        _episodeId = episodeId;
                    }
                    break;
                case "anime":
                    if (string.IsNullOrWhiteSpace(animeId))
                    {
                        _logger.LogError("Invalid anime ID provided! Please fix or input the anime ID using `--anime=[id]`");
                        return false;
                    }
                    else
                    {
                        _animeId = animeId;
                    }
                    _mode = RunMode.Anime;
                    break;
                case "help":
                    _logger.LogWarning("Run mode options:\n" + _runModeHelpString);
                    return false;
                default:
                    _logger.LogError("Invalid run mode! Use `--mode=help` for help!");
                    return false;
            }
            return true;
        }

        public async Task BeginAsync()
        {
            try
            {
                _logger.LogInformation("Verifying genre integrity!");
                await VerifyGenreCreationAsync();

                _logger.LogInformation("Running on mode: " + _mode.ToString());

                if (_mode == RunMode.Anime)
                {
                    int animeId = int.Parse(_animeId);
                    await GetAnimeAsync(animeId);
                }
                else if (_mode == RunMode.Episode)
                {
                    int animeId = int.Parse(_animeId);
                    int episodeId = int.Parse(_episodeId);
                    await GetAnimeEpisodeAsync(animeId, episodeId);
                }
                else if (_mode == RunMode.Listen)
                {

                }
                else if (_mode == RunMode.Normal)
                {
                    await GetOngoingAnimeAsync();
                }
                else if (_mode == RunMode.All)
                {
                    await MassBeginAsync();
                }
                else
                {
                    throw new Exception("Invalid run mode provided. Unable to continue");
                }
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "An exception has been thrown by the DataApp");
                throw;
            }
        }

        // modes:
        // --all - forces all anime to be downloaded and uploaded
        // --overwrite - forces overwrite of db anime
        // --anime - one specific anime
        // --episode - one specific episode
        // --ongoing - all ongoing anime


        private async Task GetAnimeAsync(int animeId)
        {
            DateTime Start = DateTime.Now;
            AnimeInfo mAnime = await _masterApiService.GetAnimeByIdAsync(animeId);

            if (mAnime == null)
            {
                throw new Exception("The anime specified could not be found.");
            }

            Anime anime = await _dataService.GetAnimeByMasterIdAsync(animeId);

            if (anime != null && _overwriteMode == "none")
            {
                var epList = await _dataService
                                        .GetEpisodeRangeAsync(mAnime.Episodes
                                        .Select(c => c.Info.Id)
                                        .ToList()).ToListAsync();
            }
        }

        private Task GetAnimeEpisodeAsync(int animeId, int episodeId)
        {
            throw new NotImplementedException();
        }

        private async Task GetOngoingAnimeAsync()
        {
            DateTime Start = DateTime.Now;

            IEnumerable<MAnime> allAnimeList = await _masterApiService.GetAllAnimeAsync();
            List<MAnime> onGoingAnimeList = allAnimeList
                                        .Where(c =>
                                        ((AnimeStatus)c.Status) == AnimeStatus.Ongoing ||
                                        ((AnimeStatus)c.Status) == AnimeStatus.NotYetAired)
                                        .ToList();

            List<MAnime> existingAnimeList = _masterApiService.GetAnimeRangeByIdsAsync(
                    await _dataService.GetUncompletedAnimeAsync()
                    .Select(c => c.MasterId)
                    .ToListAsync(), allAnimeList);

            List<MAnime> unAddedAnimeList = _masterApiService.GetAnimeRangeByIdsAsync(
                await _dataService.GetUnexistingAnimeAsync(allAnimeList
                .Select(a => a.Id).ToList()), allAnimeList);

            List<MAnime> animeList = onGoingAnimeList.Where(a => !existingAnimeList.Any(b => b.Id == a.Id)).ToList();
            animeList.AddRange(unAddedAnimeList.Where(a => !animeList.Any(b => b.Id == a.Id)));
            animeList.AddRange(existingAnimeList);

            foreach (var mAnime in animeList)
            {
                AnimeInfo animeInfo = await _masterApiService.GetAnimeInfoAsync(mAnime);
                mAnime.Info = animeInfo;

                Anime anime = await _dataService.GetAnimeByMasterIdAsync(mAnime.Id);
                _logger.LogInformation($"Parsing anime {mAnime.Title} - {mAnime.Id}");

                if (anime != null)
                {
                    Anime newAnime = _converterService.CreateAnime(mAnime);

                    anime.Status = newAnime.Status;
                    anime.FinishedAiringDate = newAnime.FinishedAiringDate;
                    anime.StartedAiringDate = newAnime.FinishedAiringDate;
                    anime.Score = newAnime.Score;
                    anime.Type = newAnime.Type;
                    anime.YoutubeTrailerId = newAnime.YoutubeTrailerId;
                    anime.Description = newAnime.Description;

                    // change the above to if statements

                    if (anime.CoverId == null && GetCoverId(mAnime) != null)
                        anime.CoverId = GenerateCoverId(mAnime);

                    if (anime.WallpaperId == null && GetWallpaperId(mAnime) != null)
                        anime.WallpaperId = GenerateWallpaperId(mAnime);

                    var epList = await _dataService
                                        .GetEpisodeRangeAsync(mAnime.Info.Episodes
                                        .Select(c => c.Info.Id)
                                        .ToList()).ToListAsync();

                    List<MEpisode> existingEpisodes = mAnime.Info.Episodes
                        .Where(e => epList
                        .Any(a => a.MasterId == e.Info.Id))
                        .ToList();

                    mAnime.Info.Episodes
                        .RemoveAll(c => epList
                        .Any(b => b.MasterId == c.Info.Id));

                    existingEpisodes = existingEpisodes.Where(e => (e.Info.Aired ?? DateTime.Now) > DateTime.Now.AddDays(-7)).ToList();
                    foreach (MEpisode mEpisode in existingEpisodes)
                    {
                        _logger.LogInformation($"Parsing episode {mEpisode.Info.Title} - {mEpisode.Info.Id}");
                        Episode episode = await _dataService.GetEpisodeByMasterIdAsync(mEpisode.Info.Id);

                        episode.Title = mEpisode.Info.Title;
                        episode.Description = mEpisode.Info.Description;
                        // we want these all to be if statements.

                        if (episode.ThumbnailId == null && GetThumbnailId(mEpisode) != null)
                            episode.ThumbnailId = GenerateThumbnailId(mEpisode);

                        IEnumerable<Mirror> mirrors = await _masterApiService.GetEpisodeMirrorsAsync(mAnime, mEpisode);
                        List<Mirror> newMirrors = mirrors
                            .Where(m => !episode.Blobs.Any(b => b.MasterId == m.Id))
                            .ToList();

                        foreach (Mirror mirror in newMirrors)
                            episode.Blobs.Add(_converterService.CreateBlob(mirror));

                        newMirrors.ForEach(a => _discordService.DeployMirror());
                        _dataService.UpdateEpisode(episode);
                       // _discordService.DeployEpisode();
                    }

                    foreach (MEpisode mEpisode in mAnime.Info.Episodes)
                    {
                        _logger.LogInformation($"Parsing episode {mEpisode.Info.Title} - {mEpisode.Info.Id}");
                        IEnumerable<Mirror> mirrors = await _masterApiService.GetEpisodeMirrorsAsync(mAnime, mEpisode);
                        mEpisode.GeneratedThumbnailId = GenerateThumbnailId(mEpisode);
                        mEpisode.Mirrors = mirrors.ToList();
                        Episode newEpisode = _converterService.CreateEpisode(mEpisode, mAnime);

                        _discordService.DeployEpisode();
                        anime.Episodes.Add(newEpisode);
                        await _discordService.DeployNewEpisodeAsync(anime.Title, anime.Id, newEpisode.Title, newEpisode.EpisodeNumber, anime.CoverId);
                    }

                    _discordService.DeploySeries(anime.Title, anime.Id);
                    _dataService.UpdateAnime(anime);
                    await _dataService.SaveChangesAsync();
                }
                else
                {
                    foreach (MEpisode mEpisode in mAnime.Info.Episodes)
                    {
                       // _logger.LogInformation($"Parsing episode {mEpisode.Info.Title} - {mEpisode.Info.Id}");
                        IEnumerable<Mirror> mirrors = await _masterApiService.GetEpisodeMirrorsAsync(mAnime, mEpisode);
                        mEpisode.GeneratedThumbnailId = GenerateThumbnailId(mEpisode);
                        mEpisode.Mirrors = mirrors.ToList();
                    }

                    mAnime.GeneratedCoverId = GenerateCoverId(mAnime);
                    mAnime.GeneratedWallaperId = GenerateWallpaperId(mAnime);

                    await UploadAnime(mAnime, true);
                }
            }
        }

        private async Task MassBeginAsync()
        {
            DateTime Start = DateTime.Now;
            _logger.LogInformation("Starting mass anime download!");
            IEnumerable<MAnime> animeList = await _masterApiService.GetAllAnimeAsync();

            foreach (var mAnime in animeList)
            {
                Anime anime = await _dataService.GetAnimeByMasterIdAsync(mAnime.Id);

                if (anime != null)
                {
                    continue;
                }

                _logger.LogInformation($"Parsing anime {mAnime.Title} - {mAnime.Id}");
                AnimeInfo animeInfo = await _masterApiService.GetAnimeInfoAsync(mAnime);

                foreach (var mEpisode in animeInfo.Episodes)
                {
                    _logger.LogInformation($"Parsing episode {mEpisode.Info.Title} - {mEpisode.Info.Id}");
                    IEnumerable<Mirror> mirrors = await _masterApiService.GetEpisodeMirrorsAsync(mAnime, mEpisode);
                    mEpisode.GeneratedThumbnailId = GenerateThumbnailId(mEpisode);
                    mEpisode.Mirrors = mirrors.ToList();
                }

                mAnime.Info = animeInfo;
                mAnime.GeneratedCoverId = GenerateCoverId(mAnime);
                mAnime.GeneratedWallaperId = GenerateWallpaperId(mAnime);

                //     if (anime != null)
                //          await ModifyAnime(mAnime);
                //      else
                await UploadAnime(mAnime);
            }

            TimeSpan End = DateTime.Now - Start;
            _logger.LogInformation("Downloaded all anime in " + End.Hours + " hours and " + End.Minutes + " minutes.");

            _logger.LogInformation("Starting mass anime upload!");
            // await InternalUploadMassBeginAsync(animeList);
        }


        private async Task UploadNormalAnime(MAnime mAnime)
        {
            Anime anime = _converterService.CreateAnime(mAnime);
            _discordService.DeploySeries(anime.Title, anime.Id);

            foreach (var mEpisode in mAnime.Info.Episodes)
            {
                Episode episode = _converterService.CreateEpisode(mEpisode, mAnime);
                _discordService.DeployEpisode();

                foreach (var mirror in mEpisode.Mirrors)
                {
                    Blob blob = _converterService.CreateBlob(mirror);
                    episode.Blobs.Add(blob);
                    _discordService.DeployMirror();

                    _logger.LogInformation($"Uploaded blob {blob.Name} - {blob.Id} - {blob.BlobId}");
                }

                anime.Episodes.Add(episode);
                _logger.LogInformation($"Uploaded episode {episode.Title} - {episode.Id}");
            }

            anime = await _genreService.FillGenresAsync(anime, mAnime);

            await _dataService.AddAnimeAsync(anime);
            await _dataService.SaveChangesAsync();

            _logger.LogInformation($"Uploaded anime {anime.Title} - {anime.Id}");

        }

        private async Task UploadAnime(MAnime mAnime, bool isFromNormal = false)
        {
            Anime anime = _converterService.CreateAnime(mAnime);
            _discordService.DeploySeries(anime.Title, anime.Id);

            foreach (var mEpisode in mAnime.Info.Episodes)
            {
                Episode episode = _converterService.CreateEpisode(mEpisode, mAnime);
                _discordService.DeployEpisode();

                if(isFromNormal)
                    await _discordService.DeployNewEpisodeAsync(anime.Title, anime.Id, episode.Title, episode.EpisodeNumber, anime.CoverId);

                foreach (var mirror in mEpisode.Mirrors)
                {
                    Blob blob = _converterService.CreateBlob(mirror);
                    episode.Blobs.Add(blob);
                    _discordService.DeployMirror();

                    _logger.LogInformation($"Uploaded blob {blob.Name} - {blob.Id} - {blob.BlobId}");
                }

                anime.Episodes.Add(episode);
                _logger.LogInformation($"Uploaded episode {episode.Title} - {episode.Id}");
            }

            anime = await _genreService.FillGenresAsync(anime, mAnime);

            await _dataService.AddAnimeAsync(anime);
            await _dataService.SaveChangesAsync();

            _logger.LogInformation($"Uploaded anime {anime.Title} - {anime.Id}");

        }

        private async Task UploadMassBeginAsync(IEnumerable<MAnime> animeList)
        {
            DateTime Start = DateTime.Now;

            foreach (var mAnime in animeList)
            {
                Anime anime = _converterService.CreateAnime(mAnime);
                _discordService.DeploySeries(anime.Title, anime.Id);

                foreach (var mEpisode in mAnime.Info.Episodes)
                {
                    Episode episode = _converterService.CreateEpisode(mEpisode, mAnime);
                    _discordService.DeployEpisode();

                    foreach (var mirror in mEpisode.Mirrors)
                    {
                        Blob blob = _converterService.CreateBlob(mirror);
                        episode.Blobs.Add(blob);
                        _discordService.DeployMirror();

                        _logger.LogInformation($"Uploaded blob {blob.Name} - {blob.Id} - {blob.BlobId}");
                    }

                    anime.Episodes.Add(episode);
                    _logger.LogInformation($"Uploaded episode {episode.Title} - {episode.Id}");
                }

                anime = await _genreService.FillGenresAsync(anime, mAnime);

                await _dataService.AddAnimeAsync(anime);
                await _dataService.SaveChangesAsync();

                _logger.LogInformation($"Uploaded anime {anime.Title} - {anime.Id}");
            }

            TimeSpan End = DateTime.Now - Start;
            _logger.LogInformation("Uploaded all anime in " + End.Hours + " hours and " + End.Minutes + " minutes.");
        }

        private async Task VerifyGenreCreationAsync()
        {
            string[] genres = Global.Config.GetSection("Genres").Get<string[]>();

            Stopwatch timer = new Stopwatch();
            timer.Start();

            foreach (string genre in genres)
            {
                Stopwatch stopwatch = new Stopwatch();
                _logger.LogInformation($"Verifying integrity for genre '{genre}'");
                stopwatch.Start();

                await _genreService.GetOrCreateAsync(genre);

                stopwatch.Stop();
                _logger.LogInformation($"Verified integrity for genre '{genre}' in {stopwatch.ElapsedMilliseconds}ms");
            }

            timer.Stop();
            _logger.LogInformation($"Verified integrity of genres in {timer.ElapsedMilliseconds}ms");
        }

        private string GenerateThumbnailId(MEpisode episode)
        {
            string oldThumbnailId = episode.Thumbnail;
            string newThumbnailId = string.IsNullOrWhiteSpace(oldThumbnailId) ? null : _cdnService.GenerateCdnId(oldThumbnailId);

            if (newThumbnailId == null)
            {
                _logger.LogWarning("No thumbnail available!");
                return null;
            }

            CdnUpload upload = _cdnService.CreateThumbnailUpload(oldThumbnailId, newThumbnailId);
            _cdnService.QueueCdnUpload(upload);

            return upload.NewCdnId;
        }

        private string GetThumbnailId(MEpisode episode)
        {
            return episode.Thumbnail;
        }

        private string GetWallpaperId(MAnime anime)
        {
            int count = anime.Info.Wallpapers?.Count ?? 0;
            string oldWallpaperId = null;

            if (count > 0)
                oldWallpaperId = anime.Info.Wallpapers?.RandomElement()?.File;

            return oldWallpaperId;
        }

        private string GetCoverId(MAnime anime)
        {
            return anime?.Info?.Poster;
        }

        private string GenerateWallpaperId(MAnime anime)
        {
            string oldWallpaperId = GetWallpaperId(anime);
            string newWallpaperId = string.IsNullOrWhiteSpace(oldWallpaperId) ? null : _cdnService.GenerateCdnId(oldWallpaperId);

            if (newWallpaperId == null)
            {
                _logger.LogWarning("No wallpaper available!");
                return null;
            }

            CdnUpload upload = _cdnService.CreateWallpaperUpload(oldWallpaperId, newWallpaperId);
            _cdnService.QueueCdnUpload(upload);

            return upload.NewCdnId;
        }

        private string GenerateCoverId(MAnime anime)
        {
            string oldCoverId = GetCoverId(anime);
            string newCoverId = string.IsNullOrWhiteSpace(oldCoverId) ? null : _cdnService.GenerateCdnId(oldCoverId);

            if (newCoverId == null)
            {
                _logger.LogWarning("No cover available!");
                return null;
            }

            CdnUpload upload = _cdnService.CreateCoverUpload(oldCoverId, newCoverId);
            _cdnService.QueueCdnUpload(upload);

            return upload.NewCdnId;
        }
    }
}