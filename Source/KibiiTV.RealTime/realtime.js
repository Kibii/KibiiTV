﻿'use strict';

const logger = require('./logger.js');
const connectedUsers = new Set();
const kibiiServers = new Set();
const config = null;

function verifyAuth(ws, data) {
    try {
        let json = JSON.parse(data);
        let key = data.key;

        if (key === config.rootKey) {
            kibiiServers.add(ws);

            ws.on('close', (code, reason) => {
                kibiiServers.delete(ws);
            });
        }
    } catch{
        // Log invalid JSON
    }
}

function verifyPushNotification(ws, data) {
    if (!kibiiServers.has(ws))
        return false;

    try {

    } catch{
        // Log invalid JSON
    }
}

function handleClosure(ws, code, reason) {
    if (connectedUsers.has(ws)) {
        connectedUsers.delete(ws);
    }
}

function handleUserError(ws, error) {
    // Log user error
}

function Intitialize(_config) {
    config = _config;
}

module.exports = {
    Intitialize: Intitialize,
    handleUserError: handleUserError,
    handleClosure: handleClosure,
    verifyPushNotification: verifyPushNotification,
    verifyAuth: verifyAuth,
    kibiiServers: kibiiServers,
    connectedUsers: connectedUsers,
}