'use strict';

const WebSocket = require('ws');
const https = require('https');
const logger = require('./logger.js');
const realtimeHandler = require('./realtime.js')
const loadConfig = require('./config.js');

const config = loadConfig();
const server = https.createServer();
const wss = new WebSocket.Server({ server });

wss.on('connection', function (ws) {
    ws.isAlive = true;
    ws.on('pong', heartbeat);
    ws.on('message', data => realtimeHandler.parsePushNotification(ws, data));
    ws.on('message', data => realtimeHandler.verifyAuth(ws, data));
    ws.on('error', error => realtimeHandler.handleUserError(ws, error));
    ws.on('close', (code, reason) => realtimeHandler.handleClosure(ws, code, reason));
});

wss.on('error', function (error) {
    logger.logCritical('Error! ' + error, 'Gateway');
});

wss.on('listening', function () {
    logger.logDebug('Kibii RealTime WebSocket is now listening on port \'' + config.port + '\' and host \'' + config.host + '\'', 'Gateway');
});

const interval = setInterval(function ping() {
    wss.clients.forEach(function each(ws) {
        if (ws.isAlive === false) return ws.terminate();

        ws.isAlive = false;
        ws.ping(noop);
    });
}, 30000);

function noop() {
    logger.log({
        noFile: true,
        level: 'Debug',
        content: 'Server sent a heartbeat!', // Implement more detailed heartbeat information
        source: 'Gateway'
    });

    // General empty function for the sake of pinging users.
}

function heartbeat() {
    logger.log({
        noFile: true,
        level: 'Debug',
        content: 'Client responded with a heartbeat!', // Implement more detailed heartbeat information
        source: 'Gateway'
    });

    this.isAlive = true;
}

realtimeHandler.Intitialize(config); // Do some logging here
server.listen(config.port, config.host);

// Add the HTML pages and ect.
