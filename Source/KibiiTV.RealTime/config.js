﻿'use strict';

function loadConfig() {
    let host = process.env.KIBII_HOST;
    let port = process.env.KIBII_PORT;
    let rootKey = process.env.KIBII_ROOT_KEY;

    return {
        port,
        host,
        rootKey,
    };
}

module.exports = loadConfig();