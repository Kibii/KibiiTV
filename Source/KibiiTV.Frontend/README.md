# KibiiTV.Frontend

This is the Frontend app of the KibiiTV project. It's the main website.

## Contributing

### Requirements

You're gonna need to download and install a couple things before you start. You'll need:

* [Node.js](https://nodejs.org/en/)
* [.NET Core SDK](https://www.microsoft.com/net/download/thank-you/dotnet-sdk-2.1.301-windows-x64-installer)
* [Git](https://git-scm.com/)
* An IDE (I use [VS Code](https://code.visualstudio.com/)).

You'll also need experience in Vue and JavaScript. If that wasn't obvious enough.

### Making changes

### Step 1: Cloning the project

Assuming you're viewing this in your browser window, I'll show you how to clone the project.

![Clone the Project](https://apap.took-my.net/HMOgjoz.png)

Click on that clipboard icon, then open file explorer and go to the location where you want to clone the project. Once you've got the location you want to clone the project to, press and hold `Shift` and `Right Click`. Then, select "Open in PowerShell/command window". Type `git clone https://gitlab.com/Kibii/KibiiTV.git` and press Enter. You should see a folder named "KibiiTV" wherever you cloned the project. I usually clone it on my Documents folder. And that's it for step 1.

### Step 2: Editing

Open up Visual Studio Code and go to File > Open Folder. ![Navigating](https://apap.took-my.net/m4tsjzf.png) Go into KibiiTV and navigate to the `Source` folder and open the `KibiiTV.Frontend` folder. Then, press `Ctrl+~`. A command terminal should open. ![Terminal](https://apap.took-my.net/H2FQItg.png) Type `npm i`. This will install all the packages required to run the project. That should be it, make your changes in the `ClientApp` folder. Type `dotnet run` to check out your changes.

### Step 3: Commiting your changes

On the sidebar, there's an icon that looks like a branch, it's right below the search icon. ![git](https://apap.took-my.net/aGxqbXY.png) Write your commit message and click the check mark. Then, at the button of the page, you should see this: ![pushhh](https://apap.took-my.net/h9YChDA.png) Click on that circular looking sync icon squished right in between the text "develop" and that warning and error indicator. That'll push your changes to the repository.

#### Fin.

That's it! If you can, you may improve on this page. Anyways, happy hacking!
