import Home from '../Components/Home'
import AnimeList from '../Components/Video/AnimeList'
import Watch from '../Components/Video/Watch'
import Anime from '../Components/Video/Anime'
import Discover from '../Components/Video/Discover'

import News from '../Components/Information/News'
import NewsList from '../Components/Information/NewsList'
import Privacy from '../Components/Information/Privacy'
import Terms from '../Components/Information/Terms'
import Affiliates from '../Components/Information/Affiliates'
import Partners from '../Components/Information/Partners'
import Staff from '../Components/Information/Staff'

import Contact from '../Components/Input/Contact'
import Report from '../Components/Input/Report'
import Apply from '../Components/Input/Apply'
import ApplyStaff from '../Components/Input/ApplyStaff'
import ApplyAffiliate from '../Components/Input/ApplyAffiliate'

export const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/discover',
        name: 'Discover',
        component: Discover
    },
    {
        path: '/anime/:animeId/watch/:episodeId',
        name: 'Watch',
        component: Watch
    }, // Rename episodeId to episodeNumber
    {
        path: '/anime/:animeId/watch/:episodeId/:episodeSlug',
        name: 'Watch',
        component: Watch
    }, ,
    {
        path: '/anime/:animeId/:animeSlug/watch/:episodeId',
        name: 'Watch',
        component: Watch
    },
    {
        path: '/anime/:animeId/:animeSlug/watch/:episodeId/:episodeSlug',
        name: 'Watch',
        component: Watch
    },
    {
        path: '/anime',
        name: 'AnimeList',
        component: AnimeList
    },
    {
        path: '/anime/:id',
        name: 'Anime',
        component: Anime
    },
    {
        path: '/anime/:id/:slug',
        name: 'Anime',
        component: Anime
    },
    {
        path: '/news',
        name: 'NewsList',
        component: NewsList
    }, {
        path: '/news/:id',
        name: 'News',
        component: News
    },
    {
        path: '/partners',
        name: 'Partners',
        component: Partners
    },
    {
        path: '/privacy',
        name: 'Privacy',
        component: Privacy
    },
    {
        path: '/terms',
        name: 'Terms',
        component: Terms
    },
    {
        path: '/affiliates',
        name: 'Affiliates',
        component: Affiliates
    },
    {
        path: '/staff',
        name: 'Staff',
        component: Staff
    }, {
        path: '../Contact',
        name: 'Contact',
        component: Contact
    },
    {
        path: '/report',
        name: 'Report',
        component: Report
    },
    {
        path: '/affiliates/apply',
        name: 'ApplyAffiliate',
        component: ApplyAffiliate
    },
    {
        path: '/apply',
        name: 'Apply',
        component: Apply
    },
    {
        path: '/staff/apply',
        name: 'ApplyStaff',
        component: ApplyStaff
    }
];
