//=====================================
// Copyright (c) KibiiTV 2018-present
// Created by:
// Purpose:
//=====================================

import Vue from 'vue'
import VueResource from 'vue-resource'
import App from './App'
import Router from './Router/router'
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'

Vue.use(VueResource);
Vue.use(Buefy, {
    defaultIconPack: 'fa'
});

Vue.config.productionTip = false;

Vue.prototype.API_URL = (getScheme() + getApiUrl());
Vue.prototype.BLOB_URL = (getScheme() + getBlobUrl());
Vue.prototype.REALTIME_URL = 'wss://' + getWebsocketUrl();
Vue.prototype.CDN_URL = 'https://deru.kibii.tv/';

Vue.http.headers.common['X-Session-Key'] = guid();

/* Vue.prototype.$http = Axios.create({
    headers: { 'X-Session-Key': guid() } // Please switch to Axios in the future!
}); */

// TODO, load configuration from remote, make constants, and move helper functions to one file.

window.alias_open = window.open;

window.open = function (url, name, specs, replace) {
    // This doesn't actually work, so the workaround is
    // to use a pop-up blocker like UBlock Origin and
    // Adblock. Until we actually make this work ;)
    console.log('Blocked an attempt to open a pop-up!');
    return;
}

Vue.prototype.$getSession = function () {
    return guid();
};

Vue.prototype.$slug = function (text) {
    if (text === undefined || text === null) {
        return undefined;
    }

    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
};

Vue.prototype.$shortenDescription = function (desc, maxLength) {
    let errMsg = 'An internal error has occurred and the description cannot be displayed.';
    if (desc == '' || desc == undefined) {
        return errMsg; // Check if just whitespace as well.
    }

    try {
        const sentences = desc.match(/.*?(?:\.\s|$)/g).slice(0, -1)
        let result = sentences.shift()
        while (sentences.length > 0 && result.length + sentences[0].length < maxLength) {
            result += sentences.shift()
        }

        return result.trim() // + '..' //debating whether to keep this or not.
    }
    catch (error) {
        return errMsg;
    }
}

function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4()
};

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1)
};

function getScheme() {
    let proto = window.location.protocol;
    let seperator = '//';

    return proto + seperator;
}

function getApiUrl() {
    if (window.location.host == 'dev.kibii.tv') {
        return 'dev.kibii.tv/api';
    }

    if (window.location.host == 'kibii.tv') {
        return 'kibii.tv/api';
    }

    if (window.location.host == 'beta.kibii.tv') {
        return 'beta.kibii.tv/api';
    }

    if (location.hostname === "localhost" || location.hostname === "127.0.0.1")
        return 'dev.kibii.tv/api';

    return 'blob.kibii.tv/api';
}

function getBlobUrl() {
    if (window.location.host == 'dev.kibii.tv') {
        return 'dev.blob.kibii.tv/api/blobs/';
    }

    if (window.location.host == 'kibii.tv') {
        return 'blob.kibii.tv/api/blobs/';
    }

    if (window.location.host == 'beta.kibii.tv') {
        return 'blob.kibii.tv/api/blobs/';
    }

    if (location.hostname === "localhost" || location.hostname === "127.0.0.1")
        return 'dev.blob.kibii.tv/api/blobs/';

    return 'blob.kibii.tv/api/blobs/'
}

function getWebsocketUrl() {
    if (window.location.host == 'dev.kibii.tv') {
        return 'dev.realtime.kibii.tv';
    }

    if (window.location.host == 'kibii.tv') {
        return 'realtime.kibii.tv';
    }

    if (window.location.host == 'beta.kibii.tv') {
        return 'realtime.kibii.tv';
    }

    if (location.hostname === "localhost" || location.hostname === "127.0.0.1")
        return 'dev.realtime.kibii.tv';

    return 'realtime.kibii.tv';
}

/* eslint-disable no-new */
const KibiiTV = new Vue({
    el: '#app',
    router: Router,
    components: {
        App
    },
    template: '<App/>'
});
