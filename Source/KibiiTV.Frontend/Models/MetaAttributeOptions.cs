namespace KibiiTV.Frontend.Models
{
    public class MetaAttributeOptions
    {
        public bool EnableMetaQueries { get; set; }
    }
}
