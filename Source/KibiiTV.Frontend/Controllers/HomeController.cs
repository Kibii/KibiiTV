using KibiiTV.Data;
using KibiiTV.Data.Media;
using KibiiTV.Frontend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Threading.Tasks;

namespace KibiiTV.Frontend.Controllers
{
    public class HomeController : Controller
    {
        private readonly KibiiContext _context;
        private readonly MetaAttributeOptions _options;

        public HomeController(KibiiContext context, IOptions<MetaAttributeOptions> options)
        {
            _context = context;
            _options = options.Value;
        }

        public IActionResult Index()
        {
            return View();
        }

        [Route("/anime/{animeId}")]
        [Route("/anime/{animeId}/{animeSlug}")]
        public async Task<IActionResult> Index(string animeId)
        {
            if (!_options.EnableMetaQueries)
                return View();

            var anime = await _context.Anime
                .Select(a => new
                {
                    a.Id,
                    a.Title,
                    a.CoverId,
                    a.Description
                })
                .FirstOrDefaultAsync(x => x.Id == animeId);

            if (anime != null)
            {
                TempData["MetaCardType"] = "summary";
                TempData["MetaTitle"] = anime.Title ?? "Kibii TV Anime " + anime.Id;
                TempData["MetaUrl"] = "https://beta.kibii.tv/anime/" + anime.Id;
                TempData["MetaImageUrl"] = "https://deru.kibii.tv/" + anime.CoverId ?? "aldnoah.zero/default.png";
                TempData["MetaDescription"] = anime.Description ?? "There is no description available for this anime.";
            }

            return View();
        }

        [Route("/anime/{animeId}/watch/{episodeNumber}")]
        [Route("/anime/{animeId}/{animeSlug}/watch/{episodeNumber}")]
        [Route("/anime/{animeId}/watch/{episodeNumber}/{episodeSlug}")]
        [Route("/anime/{animeId}/{animeSlug}/watch/{episodeNumber}/{episodeSlug}")]
        public async Task<IActionResult> Index(string animeId, int episodeNumber)
        {
            if (!_options.EnableMetaQueries)
                return View();

            var episode = await _context.Episodes
                .Select(c => new
                {
                    c.Id,
                    c.Title,
                    c.EpisodeNumber,
                    c.ThumbnailId,
                    c.Description,
                    c.AnimeId,
                    Anime = new
                    {
                        c.Anime.Title,
                        c.Anime.Description,
                        c.Anime.Id,
                        c.Anime.WallpaperId,
                    }
                })
                .FirstOrDefaultAsync(x => x.AnimeId == animeId && x.EpisodeNumber == episodeNumber);

            if (episode != null)
            {

                TempData["MetaCardType"] = "summary_large_image";
                TempData["MetaTitle"] = episode.Title ?? $"Episode {episodeNumber} - {episode.Anime.Title}" ?? "Kibii TV Anime " + episode.Anime.Id;
                TempData["MetaUrl"] = "https://beta.kibii.tv/anime/" + animeId + "/watch/" + episodeNumber;
                TempData["MetaImageUrl"] = "https://deru.kibii.tv/" + episode.ThumbnailId ?? episode.Anime.WallpaperId ?? "https://deru.kibii.tv/aldnoah.zero/default.png";
                TempData["MetaDescription"] = episode.Description ?? episode.Anime.Description ?? "There is no description available for this episode.";

                Blob blob = null; // await _context.Blobs.FirstOrDefaultAsync(b => b.Episode.Id == episode.Id);

                if (blob != null)
                {
                    if (blob.Name.ToLower() == "aika" || blob.Name.ToLower() == "kibii cloud")
                    {
                        TempData["MetaPlayerIsCustom"] = "YES";
                    }

                    TempData["MetaPlayerUrl"] = blob.BlobPrefix + blob.BlobId + blob.BlobSuffix;
                }
            }

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
