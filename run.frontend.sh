ROOT_PATH="/opt/KibiiTV/Source/KibiiTV.Frontend"
BUILD_PATH="/opt/KibiiTV/Source/KibiiTV.Frontend/Build"
EXE_PATH="/opt/KibiiTV/Source/KibiiTV.Frontend/Build/KibiiTV.Frontend.dll"

cd $ROOT_PATH
npm install
npm run install # This does some webpack stuff
npm run build

dotnet build -c Release -o $BUILD_PATH
dotnet $EXE_PATH