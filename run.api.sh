ROOT_PATH="/opt/KibiiTV/Source/KibiiTV.Api"
BUILD_PATH="/opt/KibiiTV/Source/KibiiTV.Api/Build"
EXE_PATH="/opt/KibiiTV/Source/KibiiTV.Api/Build/Kibii.dll"

cd $ROOT_PATH
dotnet build -c Release -o $BUILD_PATH
dotnet $EXE_PATH