ROOT_PATH="/opt/KibiiTV/Source/KibiiTV.Data.Generator"
BUILD_PATH="/opt/KibiiTV/Source/KibiiTV.Data.Generator/Build"
EXE_PATH="/opt/KibiiTV/Source/KibiiTV.Data.Generator/Build/Kibii.Data.Generator.dll"

cd $ROOT_PATH
dotnet build -c Release -o $BUILD_PATH
dotnet $EXE_PATH