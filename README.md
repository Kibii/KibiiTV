# Kibii TV

Kibii is the platform for streaming Japanese animation all around the globe! Our missions is to continue supporting and spreading the most amazing content ever, for building ideals off of, getting a laugh, or spending some time with friends.

## Framework

Kibii TV is an agile-forward developed application. It's seperated into multiple layers and services under a strict OOP (Object Oriented Programming) design. Everything is a microservice in Kibii.

You can find a list of frameworks, libraries, and/or systems Kibii TV uses [here.](#Frameworks)

## Development with Vue.js and Visual Studio

If you are using `Vue.js` alongside Visual Studio, please install the extention for working with `Vue.js` in `/Tools/VisualStudio/vuejs-vs2017-v1-8-8.vsix` and restart Visual Studio to fully and easily develop our frontend.

## Building

To build Kibii TV using the command line tool:

```bash
cd REPO_HOME

# Build all the projects
sh Build.[ENVIRONMENT].sh
# Configure all the projects
sh Config.[PROJECT].[ENVIRONMENT].sh
# Run the desired project
sh Run.[PROJECT].[ENVIRONMENT].sh
```

To build Kibii TV manually:

```bash
cd REPO_HOME
$BUILD_PATH = REPO_HOME/Build

# Restoring and installing packages
dotnet restore Source/AspNetCoreRateLimit/AspNetCoreRateLimit.csproj
dotnet restore Source/Kibii.Data/Kibii.Data.csproj
dotnet restore Source/Kibii.Scraper/Scraper.csproj
dotnet restore Source/Kibii.Api/Kibii.csproj
npm install Source/Kibii.RealTime/package.json

# Build all the projects

dotnet build Source/AspNetCoreRateLimit/AspNetCoreRateLimit.csproj --configuration Release --output $BUILD_PATH/RateLimit
dotnet build Source/Kibii.Data/Kibii.Data.csproj --configuration Release --output $BUILD_PATH/Data
dotnet build Source/Kibii.Scraper/Scraper.csproj --configuration Release --output $BUILD_PATH/Scraper
dotnet build Source/Kibii.Api/Kibii.csproj --configuration Release --output $BUILD_PATH/Api
sh BuildRealtime.sh --output $BUILD_PATH/RealTime

# Run whichever you'd like

# Example 1: API Server
export KIBII_ENVIRONMENT = "PRODUCTION"
export KIBII_URLS = "http://*:80"
export CUSTOMCONNSTR_MAIN = "SERVER = DB.KIBII.TV; PORT = 3306; USERNAME = ROOT; DATABASE = KIBIIMAIN; PASSWORD = ROOT;"
export CUSTOMCONNSTR_LOGS = "SERVER = DB.KIBII.TV; PORT = 3306; USERNAME = ROOT; DATABASE = LOGS; PASSWORD = ROOT;"
export DOTNET_CLI_TELEMETRY_OPTOUT=1
dotnet run $BUILD_PATH/Api/Kibii.dll

# Example 2: Scraper
export KIBII_ENVIRONMENT = "PRODUCTION"
export CUSTOMCONNSTR_MAIN = "SERVER = DB.KIBII.TV; PORT = 3306; USERNAME = ROOT; DATABASE = KIBIIMAIN; PASSWORD = ROOT;"
export DOTNET_CLI_TELEMETRY_OPTOUT=1
dotnet run $BUILD_PATH/Scraper/Scraper.dll # This might not work

# Example 3: Frontend
export KIBII_ENVIRONMENT = "PRODUCTION"
export KIBII_URLS = "http://*:80"
export CUSTOMCONNSTR_MAIN = "SERVER = DB.KIBII.TV; PORT = 3306; USERNAME = ROOT; DATABASE = KIBIIMAIN; PASSWORD = ROOT;"
export CUSTOMCONNSTR_LOGS = "SERVER = DB.KIBII.TV; PORT = 3306; USERNAME = ROOT; DATABASE = LOGS; PASSWORD = ROOT;"
dotnet run $BUILD_PATH/Kibii.Frontend/Kibii.Frontend.dll

# Example 4: RealTime
export KIBII_HOST = "realtime.kibii.tv"
export KIBII_PORT = "80"
export KIBII_ROOT_KEY = "ROOT_KEY"

node $BUILD_PATH/RealTime/app.js
```

## Frameworks

Kibii uses the following frameworks and libraries for its entire application:

- .NET Core 2.0
  - Used for the core runtime, webserver, and background runners.
- ASP.NET Core 2.0
  - Used for both the frontend and backend webservers, and all HTTP services and endpoints.
- EntityFramework Core
  - ORM used for communicating with our databases, and to prevent SQL injection.
- ASP.NET MVC
  - Model-View-Controller standard methods of delivering and serializing data over services as fast as possible, in a well structured pattern.
- VueJS
  - Frontend UI framework for SPA design.
- Dapper
  - Used by the ORM to generate, execute, and map SQL queries as fast as possible.
- JSON.NET
  - Fast serialization to/from JSON.
- AspNetCoreRateLimit
  - A bit modified for our usage, used for protection against spam attacks on our webserver
- Docker
  - Used by CI to check automatic builds and verify they are correct.
- WS
  - Fast websocket server for node.js, used by our RealTime API
- WebSocket4Net
  - Used by the webservers to connect to the RealTime API
- Bulma
  - CSS Framework that same of our UI components are built on
- Bootstrap
  - CSS Framework that our administrative panel is build on.
- CloudFlare
  - DDoS migitation and global CDN for Kibii.
- HtmlAgilityPack
  - Used for finding elements when doing certain HTML-based work
- DigitalOcean
  - Kibii TV is hosted on DigitalOcean
- Amazon S3
  - Global CDN storage for Kibii
- Buefy
  - UI Framework for VueJS. Used for modals, toasts, and other alerts.
- MySQL
  - Our main database server, this stores all of our data. Pretty self-explanatory.
- Electron
  - Framework used for the client application, avaliable for Windows, MacOS, and Linux.
- DiscordJS/RPC
  - NodeJS library for communicating with Discord's RPC.
- Android SDK 19
  - Android app for Kibii TV
