ROOT_PATH="Source/KibiiTV.Administration"
BUILD_PATH="Build"
EXE_PATH="Build/KibiiTV.Administration.dll"

cd $ROOT_PATH
dotnet build -c Release -o $BUILD_PATH
dotnet $EXE_PATH