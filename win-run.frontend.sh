ROOT_PATH="Source/KibiiTV.Frontend"
BUILD_PATH="Build"
EXE_PATH="Build/KibiiTV.Frontend.dll"

cd $ROOT_PATH
npm run install # This does some webpack stuff
npm run build

dotnet build -c Release -o $BUILD_PATH
dotnet $EXE_PATH