cd Source

cd KibiiTV.Api
docker build -t registry.gitlab.com/daave/kibiitv/api:dev . && docker push registry.gitlab.com/daave/kibiitv/api:dev
cd ..

cd KibiiTV.Frontend
docker build -t registry.gitlab.com/daave/kibiitv/frontend:dev . && docker push registry.gitlab.com/daave/kibiitv/frontend:dev
cd ..