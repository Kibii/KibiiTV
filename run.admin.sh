ROOT_PATH="/opt/KibiiTV/Source/KibiiTV.Administration"
BUILD_PATH="/opt/KibiiTV/Source/KibiiTV.Administration/Build"
EXE_PATH="/opt/KibiiTV/Source/KibiiTV.Administration/Build/Kibii.Administration.dll"

cd $ROOT_PATH
dotnet build -c Release -o $BUILD_PATH
dotnet $EXE_PATH