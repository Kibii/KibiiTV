ROOT_PATH="Source/KibiiTV.Dispatch"
BUILD_PATH="Build"
EXE_PATH="Build/KibiiTV.Dispatch.dll"

cd $ROOT_PATH
dotnet build -c Release -o $BUILD_PATH
dotnet $EXE_PATH