ROOT_PATH="/opt/KibiiTV/Source/KibiiTV.Dispatch"
BUILD_PATH="/opt/KibiiTV/Source/KibiiTV.Dispatch/Build"
EXE_PATH="/opt/KibiiTV/Source/KibiiTV.Dispatch/Build/KibiiTV.Dispatch.dll"

cd $ROOT_PATH
dotnet build -c Release -o $BUILD_PATH
dotnet $EXE_PATH