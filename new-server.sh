# New server deployment script for Kibii TV.
# Copyright (c) Kibii Inc.
# Author: Sarmad Wahab (bin senpai) [me@bin.moe]
# Unauthorized reproduction and/or distribution of this code is STRICTLY prohibited.

GIT_URL="https://gitlab.com/Kibii/KibiiTV"
PACKAGE_FOLDER="/opt/KibiiTV"

install_git () {

}

update_packages () {

}

install_netcore_packages () {

}

install_node () {

}

install_netcore () {

}

clone_repo () {

}

verify_repo_cloned () {

}

